<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document;

/**
 * Class Performance_Info
 * @package Lukaspotthast\DSV\Document
 */
class Performance_Info
{

    /** @var Document */
    private $document_ref;

    /** @var float */
    private $read_time;

    /** @var float */
    private $parse_time;

    /** @var float */
    private $create_time;

    /**
     * Performance_Info constructor.
     * @param Document $document_ref
     */
    public function __construct(Document $document_ref)
    {
        $this->document_ref = $document_ref;
        $this->read_time    = -1;
        $this->parse_time   = -1;
        $this->create_time  = -1;
    }

    /**
     * @return int
     */
    public function get_memory_footprint(): int
    {
        $serialized = serialize($this->document_ref);
        $before     = memory_get_usage();
        /** @noinspection PhpUnusedLocalVariableInspection */
        $unserialized = unserialize($serialized);
        $after        = memory_get_usage();

        return ($after - $before);
    }

    /**
     * @param float $read_time
     */
    public function set_read_time(float $read_time): void
    {
        $this->read_time = $read_time;
    }

    /**
     * @return float
     */
    public function get_read_time(): float
    {
        return $this->read_time;
    }

    /**
     * @param float $parse_time
     */
    public function set_parse_time(float $parse_time): void
    {
        $this->parse_time = $parse_time;
    }

    /**
     * @return float
     */
    public function get_parse_time(): float
    {
        return $this->parse_time;
    }

    /**
     * @param float $create_time
     */
    public function set_create_time(float $create_time): void
    {
        $this->create_time = $create_time;
    }

    /**
     * @return float
     */
    public function get_create_time(): float
    {
        return $this->create_time;
    }

}