<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichen;
use Lukaspotthast\DSV\Data\Zeit;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class PNReaktion
 * @package Lukaspotthast\DSV\Document\Element
 */
class PNReaktion extends Document_Element
{

    const PERSONENNUMMER_ERROR  = '"Personennummer" was either not set or does not reference an existing "Person".';
    const WETTKAMPFNUMMER_ERROR = '"Wettkampfnummer" was either not set or does not reference an existing "Wettkampf".';
    const WETTKAMPFART_ERROR    = '"Wettkampfart" was not one of: [%s]';
    const ART_ERROR             = '"Art" was neither blank nor one of: [%s].';
    const REAKTIONSZEIT_ERROR   = '"Reaktionszeit" was not set.';

    const WETTKAMPFART_OPTIONS = [
        'V', // Vorlauf
        'Z', // Zwischenlauf
        'F', // Finale
        'E', // Entscheidung
        'A', // Ausschwimmen
        'N', // Nachschwimmen
    ];

    const ART_OPTIONS = [
        '+', // positive Zeit (Start nach dem Startsignal)
        '-', // negative Zeit (Start vor dem Startsignal)
    ];

    /**
     * @var Zahl
     *      Eindeutige numerische Kennung für den Schwimmer
     *      innerhalb dieser Veranstaltung definiert in PERSON.
     *
     *      - REQUIRED -
     */
    private $personennummer;

    /**
     * @var Zahl
     *      Nummer des Wettkampfes.
     *
     *      - REQUIRED -
     */
    private $wettkampfnummer;

    /**
     * @var Zeichen
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      - V = Vorlauf,
     *      - Z = Zwischenlauf,
     *      - F = Finale,
     *      - E = Entscheidung,
     *      - A = Ausschwimmen,
     *      - N = Nachschwimmen
     *
     *      - REQUIRED -
     */
    private $wettkampfart;

    /**
     * @var Zeichen
     *      Zulässige Zeichen: + und –
     *      + = positive Zeit (Start nach dem Startsignal)
     *      - = negative Zeit (Start vor dem Startsignal)
     *      Unterlassungswert ist +
     *
     *      - REQUIRED -
     */
    private $art;

    /**
     * @var Zeit
     *      Reaktionszeit.
     *
     *      - REQUIRED -
     */
    private $reaktionszeit;

    /**
     * Ausschreibungimnetz constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->personennummer  = new Zahl();
        $this->wettkampfnummer = new Zahl();
        $this->wettkampfart    = new Zeichen();
        $this->art             = new Zeichen('+');
        $this->reaktionszeit   = new Zeit();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 5;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3, 5];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $personennummer
     * @throws Runtime_Exception
     */
    public function check_personennummer(Zahl $personennummer): void
    {
        if ( !($personennummer->is_set() and
               $this->get_parent()->search_person($personennummer) !== null) )
        {
            throw new Runtime_Exception(self::PERSONENNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $personennummer
     * @throws Runtime_Exception
     */
    public function set_personennummer(Zahl $personennummer): void
    {
        $this->check_personennummer($personennummer);
        $this->personennummer = $personennummer;
    }

    /**
     * @return Zahl
     */
    public function get_personennummer(): Zahl
    {
        return $this->personennummer;
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function check_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        if ( !($wettkampfnummer->is_set() and
               $this->get_parent()->search_wettkampf($wettkampfnummer) !== null) )
        {
            throw new Runtime_Exception(self::WETTKAMPFNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function set_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        $this->check_wettkampfnummer($wettkampfnummer);
        $this->wettkampfnummer = $wettkampfnummer;
    }

    /**
     * @return Zahl
     */
    public function get_wettkampfnummer(): Zahl
    {
        return $this->wettkampfnummer;
    }

    /**
     * @param Zeichen $wettkampfart
     * @throws Runtime_Exception
     */
    public function check_wettkampfart(Zeichen $wettkampfart): void
    {
        if ( !in_array($wettkampfart->get_formatted(), self::WETTKAMPFART_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::WETTKAMPFART_ERROR, implode(', ', self::WETTKAMPFART_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $wettkampfart
     * @throws Runtime_Exception
     */
    public function set_wettkampfart(Zeichen $wettkampfart): void
    {
        $this->check_wettkampfart($wettkampfart);
        $this->wettkampfart = $wettkampfart;
    }

    /**
     * @return Zeichen
     */
    public function get_wettkampfart(): Zeichen
    {
        return $this->wettkampfart;
    }

    /**
     * @param Zeichen $art
     * @throws Runtime_Exception
     */
    public function check_art(Zeichen $art): void
    {
        if ( !($art->get_formatted() === '' or
               in_array($art->get_formatted(), self::ART_OPTIONS)) )
        {
            throw new Runtime_Exception(
                sprintf(self::ART_ERROR, implode(', ', self::ART_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $art
     * @throws Runtime_Exception
     */
    public function set_art(Zeichen $art): void
    {
        $this->check_art($art);
        $this->art = $art;
    }

    /**
     * @return Zeichen
     */
    public function get_art(): Zeichen
    {
        return $this->art;
    }

    /**
     * @param Zeit $reaktionszeit
     * @throws Runtime_Exception
     */
    public function check_reaktionszeit(Zeit $reaktionszeit): void
    {
        if ( !$reaktionszeit->is_set() )
        {
            throw new Runtime_Exception(self::REAKTIONSZEIT_ERROR);
        }
    }

    /**
     * @param Zeit $reaktionszeit
     * @throws Runtime_Exception
     */
    public function set_reaktionszeit(Zeit $reaktionszeit): void
    {
        $this->check_reaktionszeit($reaktionszeit);
        $this->reaktionszeit = $reaktionszeit;
    }

    /**
     * @return Zeit
     */
    public function get_reaktionszeit(): Zeit
    {
        return $this->reaktionszeit;
    }

}