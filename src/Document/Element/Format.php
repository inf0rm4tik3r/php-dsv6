<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Document\Structure\Ast;
use Lukaspotthast\DSV\Document\Type\Vereinsergebnisliste;
use Lukaspotthast\DSV\Document\Type\Vereinsmeldeliste;
use Lukaspotthast\DSV\Document\Type\Wettkampfdefinitionsliste;
use Lukaspotthast\DSV\Document\Type\Wettkampfergebnisliste;
use Lukaspotthast\DSV\Exception\Document_Creation_Exception;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Format
 * @package Lukaspotthast\DSV\Document\Element
 */
class Format extends Document_Element
{
    const DOCUMENT_CLASS_REFERENCES = [
        'Wettkampfdefinitionsliste' => Wettkampfdefinitionsliste::class,
        'Vereinsmeldeliste'         => Vereinsmeldeliste::class,
        'Wettkampfergebnisliste'    => Wettkampfergebnisliste::class,
        'Vereinsergebnisliste'      => Vereinsergebnisliste::class,
    ];

    const LISTART_ERROR = 'The FORMAT statement specified an invalid "Listart" value: %s.';
    const VERSION_ERROR = 'The FORMAT statement specified an unsupported DSV version: $d';

    const LISTART_OPTIONS = [
        'Wettkampfdefinitionsliste',
        'Vereinsmeldeliste',
        'Wettkampfergebnisliste',
        'Vereinsergebnisliste',
    ];

    const VERSION_OPTIONS = [
        6,
    ];

    /**
     * @var Zeichenkette
     *      Art des Dokumenttyps. Kann folgende Werte annehmen:
     *      - Wettkampfdefinitionsliste,
     *      - Vereinsmeldeliste,
     *      - Wettkampfergebnisliste,
     *      - Vereinsergebnisliste
     *
     *      - REQUIRED -
     */
    private $listart;

    /**
     * @var Zahl
     *      Versionsnummer des DSV-Standards, derzeit 6.
     *      - REQUIRED -
     */
    private $version;

    /**
     * Format constructor.
     * @param Document|null  $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(?Document $parent, ?Statement $stmt = null)
    {
        $this->listart = new Zeichenkette();
        $this->version = new Zahl();

        parent::__construct($parent, $stmt);
    }

    /**
     * @param Ast $ast
     * @return string
     * @throws Document_Creation_Exception
     */
    public function get_document_class(Ast $ast): string
    {
        $listart = $this->get_listart()->get_formatted();

        if ( array_key_exists($listart, self::DOCUMENT_CLASS_REFERENCES) )
        {
            return self::DOCUMENT_CLASS_REFERENCES[$listart];
        }

        $msg = 'No document creation process available for type: "' . $listart . '"';
        throw new Document_Creation_Exception($msg, $ast);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 2;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zeichenkette $listart
     * @throws Runtime_Exception
     */
    public function check_listart(Zeichenkette $listart): void
    {
        if ( !in_array($listart->get_formatted(), self::LISTART_OPTIONS) )
        {
            throw new Runtime_Exception(sprintf(self::LISTART_ERROR, $listart));
        }
    }

    /**
     * @param Zeichenkette $listart
     * @throws Runtime_Exception
     */
    public function set_listart(Zeichenkette $listart): void
    {
        $this->check_listart($listart);
        $this->listart = $listart;
    }

    /**
     * @return Zeichenkette
     */
    public function get_listart(): Zeichenkette
    {
        return $this->listart;
    }

    /**
     * @param Zahl $version
     * @throws Runtime_Exception
     */
    public function check_version(Zahl $version): void
    {
        if ( !in_array($version->get_formatted(), self::VERSION_OPTIONS) )
        {
            throw new Runtime_Exception(sprintf(self::VERSION_ERROR, $version));
        }
    }

    /**
     * @param Zahl $version
     * @throws Runtime_Exception
     */
    public function set_version(Zahl $version): void
    {
        $this->check_version($version);
        $this->version = $version;
    }

    /**
     * @return Zahl
     */
    public function get_version(): Zahl
    {
        return $this->version;
    }

}