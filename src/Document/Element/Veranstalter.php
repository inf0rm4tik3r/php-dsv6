<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Veranstalter
 * @package Lukaspotthast\DSV\Document\Element
 */
class Veranstalter extends Document_Element
{

    const NAME_ERROR = '"Name" must be set.';

    /**
     * @var Zeichenkette
     *      Name des Veranstalters.
     *
     *      - REQUIRED -
     */
    private $name;

    /**
     * Veranstalter constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->name = new Zeichenkette();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 1;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zeichenkette $name
     * @throws Runtime_Exception
     */
    public function check_name(Zeichenkette $name): void
    {
        if ( strlen($name->get_zeichenkette()) === 0 )
        {
            throw new Runtime_Exception(self::NAME_ERROR);
        }
    }

    /**
     * @param Zeichenkette $name
     * @throws Runtime_Exception
     */
    public function set_name(Zeichenkette $name): void
    {
        $this->check_name($name);
        $this->name = $name;
    }

    /**
     * @return Zeichenkette
     */
    public function get_name(): Zeichenkette
    {
        return $this->name;
    }

}