<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Kampfgericht
 * @package Lukaspotthast\DSV\Document\Element
 */
class Kampfgericht extends Document_Element
{

    const ABSCHNITTSNUMMER_ERROR    = '"Abschnittsnummer" was not set or did no specify an existing "Abschnitt".';
    const POSITION_ERROR            = '"Position" was not one of: [%s]';
    const KAMPFGERICHT_NAME_ERROR   = '"Kampfgericht Name" was not specified.';
    const KAMPFGERICHT_VEREIN_ERROR = '"Kampfgericht Verein" was not specified.';

    const POSITION_OPTIONS = [
        'SCH', // Schiedsrichter
        'STA', // Starter
        'ZRO', // Zielrichterobmann
        'ZR',  // Zielrichter
        'ZNO', // Zeitnehmerobmann
        'ZN',  // Zeitnehmer
        'RZN', // Reservezeitnehmer
        'SR',  // Schwimmrichter
        'WRO', // Wenderichterobmann
        'WR',  // Wenderichter
        'AUS', // Auswerter
        'SP',  // Sprecher
        'PKF', // Protokollführer
        'ZBV', // Sonstige Kampfrichter
    ];

    /**
     * @var Zahl
     *      Nummer des Abschnitts. Muss existierenden Abschnitt referenzieren.
     *
     *      - REQUIRED -
     */
    private $abschnittsnummer;

    /**
     * @var Zeichenkette
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      - SCH = Schiedsrichter,
     *      - STA = Starter,
     *      - ZRO = Zielrichterobmann,
     *      - ZR = Zielrichter,
     *      - ZNO = Zeitnehmerobmann,
     *      - ZN = Zeitnehmer,
     *      - RZN = Reservezeitnehmer,
     *      - SR = Schwimmrichter,
     *      - WRO = Wenderichterobmann,
     *      - WR = Wenderichter,
     *      - AUS = Auswerter,
     *      - SP = Sprecher,
     *      - PKF = Protokollführer,
     *      - ZBV = Sonstige Kampfrichter
     *
     *      - REQUIRED -
     */
    private $position;

    /**
     * @var Zeichenkette
     *      Name, Vorname des Kampfrichters.
     *
     *      - REQUIRED -
     */
    private $kampfrichter_name;

    /**
     * @var Zeichenkette
     *      Name des Vereins des Kampfrichters
     *      (Verein, der den Kampfrichter gestellt hat).
     *
     *      - REQUIRED -
     */
    private $kampfrichter_verein;

    /**
     * Ausschreibungimnetz constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->abschnittsnummer    = new Zahl();
        $this->position            = new Zeichenkette();
        $this->kampfrichter_name   = new Zeichenkette();
        $this->kampfrichter_verein = new Zeichenkette();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 4;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3, 4];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $abschnittsnummer
     * @throws Runtime_Exception
     */
    public function check_abschnittsnummer(Zahl $abschnittsnummer): void
    {
        $correct = false;
        if ( $abschnittsnummer->is_set() )
        {
            $search_result = $this->get_parent()->search_abschnitt($abschnittsnummer);
            $correct       = $search_result !== null;
        }
        if ( !$correct )
        {
            throw new Runtime_Exception(self::ABSCHNITTSNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $abschnittsnummer
     * @throws Runtime_Exception
     */
    public function set_abschnittsnummer(Zahl $abschnittsnummer): void
    {
        $this->check_abschnittsnummer($abschnittsnummer);
        $this->abschnittsnummer = $abschnittsnummer;
    }

    /**
     * @return Zahl
     */
    public function get_abschnittsnummer(): Zahl
    {
        return $this->abschnittsnummer;
    }

    /**
     * @param Zeichenkette $position
     * @throws Runtime_Exception
     */
    public function check_position(Zeichenkette $position): void
    {
        if ( !in_array($position->get_formatted(), self::POSITION_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::POSITION_ERROR, implode(', ', self::POSITION_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichenkette $position
     * @throws Runtime_Exception
     */
    public function set_position(Zeichenkette $position): void
    {
        $this->check_position($position);
        $this->position = $position;
    }

    /**
     * @return Zeichenkette
     */
    public function get_position(): Zeichenkette
    {
        return $this->position;
    }

    /**
     * @param Zeichenkette $kampfrichter_name
     * @throws Runtime_Exception
     */
    public function check_kampfrichter_name(Zeichenkette $kampfrichter_name): void
    {
        if ( $kampfrichter_name->get_formatted() === 0 )
        {
            throw new Runtime_Exception(self::KAMPFGERICHT_NAME_ERROR);
        }
    }

    /**
     * @param Zeichenkette $kampfrichter_name
     * @throws Runtime_Exception
     */
    public function set_kampfrichter_name(Zeichenkette $kampfrichter_name): void
    {
        $this->check_kampfrichter_name($kampfrichter_name);
        $this->kampfrichter_name = $kampfrichter_name;
    }

    /**
     * @return Zeichenkette
     */
    public function get_kampfrichter_name(): Zeichenkette
    {
        return $this->kampfrichter_name;
    }

    /**
     * @param Zeichenkette $kampfrichter_verein
     * @throws Runtime_Exception
     */
    public function check_kampfrichter_verein(Zeichenkette $kampfrichter_verein): void
    {
        if ( $kampfrichter_verein->get_formatted() === 0 )
        {
            throw new Runtime_Exception(self::KAMPFGERICHT_VEREIN_ERROR);
        }
    }

    /**
     * @param Zeichenkette $kampfrichter_verein
     * @throws Runtime_Exception
     */
    public function set_kampfrichter_verein(Zeichenkette $kampfrichter_verein): void
    {
        $this->check_kampfrichter_verein($kampfrichter_verein);
        $this->kampfrichter_verein = $kampfrichter_verein;
    }

    /**
     * @return Zeichenkette
     */
    public function get_kampfrichter_verein(): Zeichenkette
    {
        return $this->kampfrichter_verein;
    }

}