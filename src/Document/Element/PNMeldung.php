<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichen;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/*
 * INFO: Nearly identical with "Person"!
 */

/**
 * Class PNMeldung
 * @package Lukaspotthast\DSV\Document\Element
 */
class PNMeldung extends Document_Element
{

    const NAME_ERROR           = '"Name" was not set.';
    const DSV_ID_ERROR         = '"DSV ID" was neither empty nor a number of 6 digits.';
    const PERSONENNUMMER_ERROR = '"Personennummer" was not set or was not unique.';
    const GESCHLECHT_ERROR     = '"Geschlecht" was not one of: [%s]';
    const JAHRGANG_ERROR       = '"Jahrgang" was not a number of 4 digits.';
    const TRAINER_ERROR        = '"Trainernummer" was neither blank nor did it specify an existing "Trainer".';

    const GESCHLECHT_OPTIONS = ['M', 'W'];

    /**
     * @var Zeichenkette
     *      Nachname, Vorname des Teilnehmers.
     *
     *      - REQUIRED -
     */
    private $name;

    /**
     * @var Zahl
     *      6-stellige durch den DSV für jeden Schwimmer festgelegte
     *      Zahl. Ist keine DSV-ID des Schwimmers bekannt, muss 0
     *      angegeben werden.
     *
     *      - REQUIRED -
     */
    private $dsv_id;

    /**
     * @var Zahl
     *      Eindeutige numerische Kennung für den Schwimmer
     *      innerhalb dieser Veranstaltung zum Zwecke der Zuordnung
     *      der Wettkämpfe.
     *
     *      - REQUIRED -
     */
    private $personennummer;

    /**
     * @var Zeichen
     *      Zulässige Werte:
     *      M = männlich
     *      W = weiblich
     *
     *      - REQUIRED -
     */
    private $geschlecht;

    /**
     * @var Zahl
     *      Der Jahrgang ist als 4-stellige Zahl anzugeben, z.B. 1990.
     *
     *      - REQUIRED -
     */
    private $jahrgang;

    /**
     * @var Zahl
     */
    private $altersklasse;

    /**
     * @var Zahl
     *      Eindeutige Trainer-Nummer aus dem Element TRAINER, um
     *      einen Trainer einem Schwimmer zuzuordnen.
     *
     *      - NEEDS CHECK -
     */
    private $trainernummer;

    /**
     * Ausschreibungimnetz constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->name           = new Zeichenkette();
        $this->dsv_id         = new Zahl();
        $this->personennummer = new Zahl();
        $this->geschlecht     = new Zeichen();
        $this->jahrgang       = new Zahl();
        $this->altersklasse   = new Zahl();
        $this->trainernummer  = new Zahl();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 7;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3, 4, 5];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zeichenkette $name
     * @throws Runtime_Exception
     */
    public function check_name(Zeichenkette $name): void
    {
        if ( $name->get_formatted() === '' )
        {
            throw new Runtime_Exception(self::NAME_ERROR);
        }
    }

    /**
     * @param Zeichenkette $name
     * @throws Runtime_Exception
     */
    public function set_name(Zeichenkette $name): void
    {
        $this->check_name($name);
        $this->name = $name;
    }

    /**
     * @return Zeichenkette
     */
    public function get_name(): Zeichenkette
    {
        return $this->name;
    }

    /**
     * @param Zahl $dsv_id
     * @throws Runtime_Exception
     */
    public function check_dsv_id(Zahl $dsv_id): void
    {
        if ( $dsv_id->get_formatted() !== '' and
             strlen($dsv_id->get_formatted()) !== 6 )
        {
            throw new Runtime_Exception(self::DSV_ID_ERROR);
        }
    }

    /**
     * @param Zahl $dsv_id
     * @throws Runtime_Exception
     */
    public function set_dsv_id(Zahl $dsv_id): void
    {
        $this->check_dsv_id($dsv_id);
        $this->dsv_id = $dsv_id;
    }

    /**
     * @return Zahl
     */
    public function get_dsv_id(): Zahl
    {
        return $this->dsv_id;
    }

    /**
     * @param Zahl $personennummer
     * @throws Runtime_Exception
     */
    public function check_personennummer(Zahl $personennummer): void
    {
        $correct = false;
        if ( $personennummer->is_set() )
        {
            $search_result = $this->get_parent()->search_pnmeldung($personennummer, false);
            $correct       = ($search_result === null or $search_result === $this);
        }
        if ( !$correct )
        {
            throw new Runtime_Exception(self::PERSONENNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $personennummer
     * @throws Runtime_Exception
     */
    public function set_personennummer(Zahl $personennummer): void
    {
        $this->check_personennummer($personennummer);
        $this->personennummer = $personennummer;
    }

    /**
     * @return Zahl
     */
    public function get_personennummer(): Zahl
    {
        return $this->personennummer;
    }

    /**
     * @param Zeichen $geschlecht
     * @throws Runtime_Exception
     */
    public function check_geschlecht(Zeichen $geschlecht): void
    {
        if ( !in_array($geschlecht->get_formatted(), self::GESCHLECHT_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::GESCHLECHT_ERROR, implode(', ', self::GESCHLECHT_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $geschlecht
     * @throws Runtime_Exception
     */
    public function set_geschlecht(Zeichen $geschlecht): void
    {
        $this->check_geschlecht($geschlecht);
        $this->geschlecht = $geschlecht;
    }

    /**
     * @return Zeichen
     */
    public function get_geschlecht(): Zeichen
    {
        return $this->geschlecht;
    }

    /**
     * @param Zahl $jahrgang
     * @throws Runtime_Exception
     */
    public function check_jahrgang(Zahl $jahrgang): void
    {
        if ( strlen($jahrgang->get_formatted()) !== 4 )
        {
            throw new Runtime_Exception(self::JAHRGANG_ERROR);
        }
    }

    /**
     * @param Zahl $jahrgang
     * @throws Runtime_Exception
     */
    public function set_jahrgang(Zahl $jahrgang): void
    {
        $this->check_jahrgang($jahrgang);
        $this->jahrgang = $jahrgang;
    }

    /**
     * @return Zahl
     */
    public function get_jahrgang(): Zahl
    {
        return $this->jahrgang;
    }

    /**
     * @param Zahl $altersklasse
     */
    public function set_altersklasse(Zahl $altersklasse): void
    {
        $this->altersklasse = $altersklasse;
    }

    /**
     * @return Zahl
     */
    public function get_altersklasse(): Zahl
    {
        return $this->altersklasse;
    }

    /**
     * @param Zahl $trainernummer
     * @throws Runtime_Exception
     */
    public function check_trainernummer(Zahl $trainernummer): void
    {
        if ( $trainernummer->is_set() and
             $this->get_parent()->search_trainer($trainernummer, false) === null )
        {
            throw new Runtime_Exception(self::TRAINER_ERROR);
        }
    }

    /**
     * @param Zahl $trainernummer
     * @throws Runtime_Exception
     */
    public function set_trainernummer(Zahl $trainernummer): void
    {
        $this->check_trainernummer($trainernummer);
        $this->trainernummer = $trainernummer;
    }

    /**
     * @return Zahl
     */
    public function get_trainernummer(): Zahl
    {
        return $this->trainernummer;
    }

}