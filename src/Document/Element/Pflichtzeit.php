<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\JGAK;
use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichen;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Data\Zeit;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Pflichtzeit
 * @package Lukaspotthast\DSV\Document\Element
 */
class Pflichtzeit extends Document_Element
{

    const WETTKAMPFNUMMER_ERROR = '"Wettkampfnummer" must be set and specify an existing "Wettkampf".';
    const WETTKAMPFART_ERROR    = '"Wettkampfart" was not one of: [%s]';
    const WERTUNGSKLASSE_ERROR  = '"Wertungsklasse" was not one of: [%s]';
    const MINIMALER_JGAK_ERROR  = '"Minimaler Jg/Ak" must be set.';
    const PFLICHTZEIT_ERROR     = '"Pflichtzeit" must be set.';
    const GESCHLECHT_ERROR      = '"Geschlecht" was neither blank nor one of: [%s]';

    const WETTKAMPFART_OPTIONS = [
        'V', // Vorlauf
        'Z', // Zwischenlauf
        'F', // Finale
        'E', // Entscheidung
    ];

    const WERTUNGSKLASSE_OPTIONS = [
        'JG', // Jahrgang
        'AK', // Altersklasse
    ];

    const GESCHLECHT_OPTIONS = [
        'M', // männlich
        'W', // weiblich
    ];

    /**
     * @var Zahl
     *      Nummer des Wettkampfes, für den die Pflichtzeit definiert wird.
     *      Muss existieren.
     *
     *      - REQUIRED -
     */
    private $wettkampfnummer;

    /**
     * @var Zeichenkette
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      V = Vorlauf,
     *      Z = Zwischenlauf,
     *      F = Finale,
     *      E = Entscheidung
     *
     *      - REQUIRED -
     */
    private $wettkampfart;

    /**
     * @var Zeichenkette
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      JG = Jahrgang,
     *      AK = Altersklasse
     *
     *      - REQUIRED -
     */
    private $wertungsklasse;

    /**
     * @var JGAK
     *      Kleinster Jahrgang / Größte Altersklasse (für die offene
     *      Klasse wird hier 0 angegeben).
     *
     *      - REQUIRED -
     */
    private $minimaler_jgak;

    /**
     * @var JGAK
     *      Unterlassungswert = Mindest-Jahrgang/Altersklasse
     *      Ansonsten größter Jahrgang/kleinste Altersklasse (0 für „und jünger“).
     */
    private $maximaler_jgak;

    /**
     * @var Zeit
     *
     *      - REQUIRED -
     */
    private $pflichtzeit;

    /**
     * @var Zeichen
     *      Zulässige Werte:
     *      M = männlich,
     *      W = weiblich,
     *      X = mixed
     *      Wenn nicht angegeben, wird das Geschlecht aus der
     *      Wettkampffolge verwendet.
     */
    private $geschlecht;

    /**
     * Pflichtzeit constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->wettkampfnummer = new Zahl();
        $this->wettkampfart    = new Zeichenkette();
        $this->wertungsklasse  = new Zeichenkette();
        $this->minimaler_jgak  = new JGAK();
        $this->maximaler_jgak  = new JGAK();
        $this->pflichtzeit     = new Zeit();
        $this->geschlecht      = new Zeichen();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 7;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3, 4, 6];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function check_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        $correct = false;
        if ( $wettkampfnummer->is_set() )
        {
            $search_result = $this->get_parent()->search_wettkampf($wettkampfnummer, false);
            $correct       = $search_result !== null;
        }
        if ( !$correct )
        {
            throw new Runtime_Exception(self::WETTKAMPFNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function set_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        $this->check_wettkampfnummer($wettkampfnummer);
        $this->wettkampfnummer = $wettkampfnummer;
    }

    /**
     * @return Zahl
     */
    public function get_wettkampfnummer(): Zahl
    {
        return $this->wettkampfnummer;
    }

    /**
     * @param Zeichenkette $wettkampfart
     * @throws Runtime_Exception
     */
    public function check_wettkampfart(Zeichenkette $wettkampfart): void
    {
        if ( !in_array($wettkampfart->get_formatted(), self::WETTKAMPFART_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::WETTKAMPFART_ERROR, implode(', ', self::WETTKAMPFART_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichenkette $wettkampfart
     * @throws Runtime_Exception
     */
    public function set_wettkampfart(Zeichenkette $wettkampfart): void
    {
        $this->check_wettkampfart($wettkampfart);
        $this->wettkampfart = $wettkampfart;
    }

    /**
     * @return Zeichenkette
     */
    public function get_wettkampfart(): Zeichenkette
    {
        return $this->wettkampfart;
    }

    /**
     * @param Zeichenkette $wertungsklasse
     * @throws Runtime_Exception
     */
    public function check_wertungsklasse(Zeichenkette $wertungsklasse): void
    {
        if ( !in_array($wertungsklasse->get_formatted(), self::WERTUNGSKLASSE_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::WERTUNGSKLASSE_ERROR, implode(', ', self::WERTUNGSKLASSE_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichenkette $wertungsklasse
     * @throws Runtime_Exception
     */
    public function set_wertungsklasse(Zeichenkette $wertungsklasse): void
    {
        $this->check_wertungsklasse($wertungsklasse);
        $this->wertungsklasse = $wertungsklasse;
    }

    /**
     * @return Zeichenkette
     */
    public function get_wertungsklasse(): Zeichenkette
    {
        return $this->wertungsklasse;
    }

    /**
     * @param JGAK $minimaler_jgak
     * @throws Runtime_Exception
     */
    public function check_minimaler_jgak(JGAK $minimaler_jgak): void
    {
        if ( !$minimaler_jgak->is_set() )
        {
            throw new Runtime_Exception(self::MINIMALER_JGAK_ERROR);
        }
    }

    /**
     * @param JGAK $minimaler_jgak
     * @throws Runtime_Exception
     */
    public function set_minimaler_jgak(JGAK $minimaler_jgak): void
    {
        $this->check_minimaler_jgak($minimaler_jgak);
        $this->minimaler_jgak = $minimaler_jgak;

        // If $maximaler_jgak was not jet set, set it to be equal to $minimaler_jgak.
        if ( !$this->maximaler_jgak->is_set() )
        {
            $this->maximaler_jgak->set_from_string($minimaler_jgak->get_formatted());
        }
    }

    /**
     * @return JGAK
     */
    public function get_minimaler_jgak(): JGAK
    {
        return $this->minimaler_jgak;
    }

    /**
     * @param JGAK $maximaler_jgak
     */
    public function set_maximaler_jgak(JGAK $maximaler_jgak): void
    {
        $this->maximaler_jgak = $maximaler_jgak;
    }

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * @return JGAK
     */
    public function get_maximaler_jgak(): JGAK
    {
        if ( !$this->maximaler_jgak->is_set() )
        {
            /** @noinspection PhpUnhandledExceptionInspection */
            $this->maximaler_jgak->set_from_string($this->minimaler_jgak->get_formatted());
        }
        return $this->maximaler_jgak;
    }

    /**
     * @param Zeit $pflichtzeit
     * @throws Runtime_Exception
     */
    public function check_pflichtzeit(Zeit $pflichtzeit): void
    {
        if ( !$pflichtzeit->is_set() )
        {
            throw new Runtime_Exception(self::PFLICHTZEIT_ERROR);
        }
    }

    /**
     * @param Zeit $pflichtzeit
     * @throws Runtime_Exception
     */
    public function set_pflichtzeit(Zeit $pflichtzeit): void
    {
        $this->check_pflichtzeit($pflichtzeit);
        $this->pflichtzeit = $pflichtzeit;
    }

    /**
     * @return Zeit
     */
    public function get_pflichtzeit(): Zeit
    {
        return $this->pflichtzeit;
    }

    /**
     * @param Zeichen $geschlecht
     * @throws Runtime_Exception
     */
    public function check_geschlecht(Zeichen $geschlecht): void
    {
        if ( $geschlecht->get_formatted() !== '' and
             !in_array($geschlecht->get_formatted(), self::GESCHLECHT_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::GESCHLECHT_ERROR, implode(', ', self::GESCHLECHT_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $geschlecht
     * @throws Runtime_Exception
     */
    public function set_geschlecht(Zeichen $geschlecht): void
    {
        $this->check_geschlecht($geschlecht);
        $this->geschlecht = $geschlecht;
    }

    /**
     * @return Zeichen
     * @throws Runtime_Exception
     */
    public function get_geschlecht(): Zeichen
    {
        if ( $this->geschlecht->get_formatted() === '' )
        {
            // Verwende Geschlecht aus Wettkampffolge, wenn Geschlecht nicht angegeben wurde.
            $this->geschlecht->set_from_string(
                $this->get_parent()->search_wettkampf($this->wettkampfnummer)->get_geschlecht()->get_formatted()
            );
        }

        return $this->geschlecht;
    }

}