<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichen;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Data\Zeit;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Staffelergebnis
 * @package Lukaspotthast\DSV\Document\Element
 */
class Staffelergebnis extends Document_Element
{

    const STAFFELNUMMER_ERROR      = '"Staffelnummer" was either not set or did no specify an existing "Staffel".';
    const WETTKAMPFNUMMER_ERROR    = '"Wettkampfnummer" was either not set or did not specify an existing "Wettkampf".';
    const WETTKAMPFART_ERROR       = '"Wettkampfart" was not one of: [%s]';
    const WERTUNGSNUMMER_ERROR     = '"Wertungsnummer" was either not set or a "Personenergebnis" with that number already existed.';
    const PLATZ_ERROR              = '"Platz" was not specified.';
    const ENDZEIT_ERROR            = '"Endzeit" was not specified. Use "00:00:00,00" as a default if necessary.';
    const NICHTWERTUNGSGRUND_ERROR = '"Nichtwertungsgrund" was not specified, but it must if "Platz" is set to 0.';
    const ENM_ERROR                = '"Erhöhtes nachträgliches Meldegeld (ENM)" was neither blank nor one of: [%s]';

    const WETTKAMPFART_OPTIONS = [
        'V', // Vorlauf
        'Z', // Zwischenlauf
        'F', // Finale
        'E', // Entscheidung
        'A', // Ausschwimmen
        'N', // Nachschwimmen
    ];

    const NICHTWERTUNGSGRUND_OPTIONS = [
        'DS', // Disqualifikation
        'NA', // nicht am Start/nicht angetreten
        'AB', // vom Wettkampf abgemeldet
        'AU', // Aufgegeben
        'ZU', // Zeitüberschreitung (nur für Langstreckenwettkämpfe)
    ];

    const ENM_OPTIONS = [
        'E', // Norm erreicht
        'F', // ENM fällig
        'N', // Norm nicht erreicht, aber nachgewiesen
    ];

    /**
     * @var Zahl
     *      Eindeutige numerische Kennung für die Staffel
     *      innerhalb dieser Veranstaltung definiert in STAFFEL.
     *
     *      - REQUIRED -
     */
    private $staffelnummer;

    /**
     * @var Zahl
     *      Nummer des Wettkampfes.
     *
     *      - REQUIRED -
     */
    private $wettkampfnummer;

    /**
     * @var Zeichen
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      - V = Vorlauf,
     *      - Z = Zwischenlauf,
     *      - F = Finale,
     *      - E = Entscheidung,
     *      - A = Ausschwimmen,
     *      - N = Nachschwimmen
     *
     *      - REQUIRED -
     */
    private $wettkampfart;

    /**
     * @var Zahl
     *      Eindeutige, in der gesamten Veranstaltung nur einmal
     *      vergebene Nummer für diese Wertung.
     *
     *      - REQUIRED -
     */
    private $wertungsnummer;

    /**
     * @var Zahl
     *      Erzielte Platzierung. Bei Füllung des Feldes "Grund
     *      der Nichtwertung" muss hier als Platz "0" angegeben
     *      werden.
     *
     *      - REQUIRED -
     */
    private $platz;

    /**
     * @var Zeit
     *      Endzeit (Ggf / Standard: 00:00:00,00)
     *
     *      - REQUIRED -
     */
    private $endzeit;

    /**
     * @var Zeichenkette
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      - DS = Disqualifikation,
     *      - NA = nicht am Start/nicht angetreten,
     *      - AB = vom Wettkampf abgemeldet,
     *      - AU = Aufgegeben,
     *      - ZU = Zeitüberschreitung (nur für Langstreckenwettkämpfe)
     *      Muss angegeben werden, wenn Platz auf 0 gesetzt ist.
     *
     *      - NEEDS CHECK -
     */
    private $nichtwertungsgrund;

    /**
     * @var Zahl
     *      Startnummer des disqualifizierten Schwimmers
     *      innerhalb der Staffel. 0 wenn es ein allgemeiner
     *      Disqualifikationsgrund war (z.B. Meldefehler).
     */
    private $startnummer_disqualifizierter_schwimmer;

    /**
     * @var Zeichenkette
     *      Bemerkungen / Disqualifikationsgrund.
     */
    private $nichtwertungsbemerkung;

    /**
     * @var Zeichen
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      - E = Norm erreicht,
     *      - F = ENM fällig,
     *      - N = Norm nicht erreicht, aber nachgewiesen
     *
     *      - NEEDS CHECK -
     */
    private $enm;

    /**
     * Ausschreibungimnetz constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->staffelnummer                           = new Zahl();
        $this->wettkampfnummer                         = new Zahl();
        $this->wettkampfart                            = new Zeichen();
        $this->wertungsnummer                          = new Zahl();
        $this->platz                                   = new Zahl();
        $this->endzeit                                 = new Zeit("00:00:00,00");
        $this->nichtwertungsgrund                      = new Zeichenkette();
        $this->startnummer_disqualifizierter_schwimmer = new Zahl();
        $this->nichtwertungsbemerkung                  = new Zeichenkette();
        $this->enm                                     = new Zeichen();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 10;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3, 4, 5, 6];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $staffelnummer
     * @throws Runtime_Exception
     */
    public function check_staffelnummer(Zahl $staffelnummer): void
    {
        if ( !($staffelnummer->is_set() and
               $this->get_parent()->search_staffel($staffelnummer, false) !== null) )
        {
            throw new Runtime_Exception(self::STAFFELNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $staffelnummer
     * @throws Runtime_Exception
     */
    public function set_staffelnummer(Zahl $staffelnummer): void
    {
        $this->check_staffelnummer($staffelnummer);
        $this->staffelnummer = $staffelnummer;
    }

    /**
     * @return Zahl
     */
    public function get_staffelnummer(): Zahl
    {
        return $this->staffelnummer;
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function check_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        if ( !($wettkampfnummer->is_set() and
               $this->get_parent()->search_wettkampf($wettkampfnummer, false) !== null) )
        {
            throw new Runtime_Exception(self::WETTKAMPFNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function set_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        $this->check_wettkampfnummer($wettkampfnummer);
        $this->wettkampfnummer = $wettkampfnummer;
    }

    /**
     * @return Zahl
     */
    public function get_wettkampfnummer(): Zahl
    {
        return $this->wettkampfnummer;
    }

    /**
     * @param Zeichen $wettkampfart
     * @throws Runtime_Exception
     */
    public function check_wettkampfart(Zeichen $wettkampfart): void
    {
        if ( !in_array($wettkampfart->get_formatted(), self::WETTKAMPFART_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::WETTKAMPFART_ERROR, implode(', ', self::WETTKAMPFART_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $wettkampfart
     * @throws Runtime_Exception
     */
    public function set_wettkampfart(Zeichen $wettkampfart): void
    {
        $this->check_wettkampfart($wettkampfart);
        $this->wettkampfart = $wettkampfart;
    }

    /**
     * @return Zeichen
     */
    public function get_wettkampfart(): Zeichen
    {
        return $this->wettkampfart;
    }

    /**
     * @param Zahl $wertungsnummer
     * @throws Runtime_Exception
     */
    public function check_wertungsnummer(Zahl $wertungsnummer): void
    {
        if ( !($wertungsnummer->is_set() and $this->get_parent()->search_wertung($wertungsnummer) !== null) )
        {
            throw new Runtime_Exception(self::WERTUNGSNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $wertungsnummer
     * @throws Runtime_Exception
     */
    public function set_wertungsnummer(Zahl $wertungsnummer): void
    {
        $this->check_wertungsnummer($wertungsnummer);
        $this->wertungsnummer = $wertungsnummer;
    }

    /**
     * @return Zahl
     */
    public function get_wertungsnummer(): Zahl
    {
        return $this->wertungsnummer;
    }

    /**
     *
     * @param Zahl $platz
     * @throws Runtime_Exception
     */
    public function check_platz(Zahl $platz): void
    {
        if ( !$platz->is_set() )
        {
            throw new Runtime_Exception(self::PLATZ_ERROR);
        }
    }

    /**
     * @param Zahl $platz
     * @throws Runtime_Exception
     */
    public function set_platz(Zahl $platz): void
    {
        $this->check_platz($platz);
        $this->platz = $platz;
    }

    /**
     * @return Zahl
     */
    public function get_platz(): Zahl
    {
        return $this->platz;
    }

    /**
     * @param Zeit $endzeit
     * @throws Runtime_Exception
     */
    public function check_endzeit(Zeit $endzeit): void
    {
        if ( !$endzeit->is_set() )
        {
            throw new Runtime_Exception(self::ENDZEIT_ERROR);
        }
    }

    /**
     * @param Zeit $endzeit
     * @throws Runtime_Exception
     */
    public function set_endzeit(Zeit $endzeit): void
    {
        $this->check_endzeit($endzeit);
        $this->endzeit = $endzeit;
    }

    /**
     * @return Zeit
     */
    public function get_endzeit(): Zeit
    {
        return $this->endzeit;
    }

    /**
     * @param Zeichenkette $nichtwertungsgrund
     * @throws Runtime_Exception
     */
    public function check_nichtwertungsgrund(Zeichenkette $nichtwertungsgrund): void
    {
        $placement_implies_disqualification = $this->platz->get_zahl() === 0;
        if ( $placement_implies_disqualification and
             !in_array($nichtwertungsgrund, self::NICHTWERTUNGSGRUND_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::NICHTWERTUNGSGRUND_ERROR, implode(', ', self::NICHTWERTUNGSGRUND_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichenkette $nichtwertungsgrund
     * @throws Runtime_Exception
     */
    public function set_nichtwertungsgrund(Zeichenkette $nichtwertungsgrund): void
    {
        $this->check_nichtwertungsgrund($nichtwertungsgrund);
        $this->nichtwertungsgrund = $nichtwertungsgrund;

        // Non-empty values lead to: Platz => 0
        if ( $nichtwertungsgrund->is_set() )
        {
            $this->platz->set_from_string('0');
        }
    }

    /**
     * @return Zeichenkette
     */
    public function get_nichtwertungsgrund(): Zeichenkette
    {
        return $this->nichtwertungsgrund;
    }

    /**
     * @param Zahl $startnummer_disqualifizierter_schwimmer
     */
    public function set_startnummer_disqualifizierter_schwimmer(Zahl $startnummer_disqualifizierter_schwimmer): void
    {
        $this->startnummer_disqualifizierter_schwimmer = $startnummer_disqualifizierter_schwimmer;
    }

    /**
     * @return Zahl
     */
    public function get_startnummer_disqualifizierter_schwimmer(): Zahl
    {
        return $this->startnummer_disqualifizierter_schwimmer;
    }

    /**
     * @param Zeichenkette $nichtwertungsbemerkung
     */
    public function set_nichtwertungsbemerkung(Zeichenkette $nichtwertungsbemerkung): void
    {
        $this->nichtwertungsbemerkung = $nichtwertungsbemerkung;
    }

    /**
     * @return Zeichenkette
     */
    public function get_nichtwertungsbemerkung(): Zeichenkette
    {
        return $this->nichtwertungsbemerkung;
    }

    /**
     * @param Zeichen $enm
     * @throws Runtime_Exception
     */
    public function check_enm(Zeichen $enm): void
    {
        if ( $enm->get_formatted() !== '' and !in_array($enm, self::ENM_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::ENM_ERROR, implode(', ', self::ENM_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $enm
     * @throws Runtime_Exception
     */
    public function set_enm(Zeichen $enm): void
    {
        $this->check_enm($enm);
        $this->enm = $enm;
    }

    /**
     * @return Zeichen
     */
    public function get_enm(): Zeichen
    {
        return $this->enm;
    }

}