<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\JGAK;
use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/*
 * Wie STMeldung, nur ohne Staffelname
 */

/**
 * Class Staffel
 * @package Lukaspotthast\DSV\Document\Element
 */
class Staffel extends Document_Element
{

    const MANNSCHAFTSNUMMER_ERROR = '"Mannschaftsnummer" was not set.';
    const STAFFELNUMMER_ERROR     = '"Staffelnummer" was either not set or did not specify an existing "STMeldung"';
    const WERTUNGSKLASSE_ERROR    = '"Wertungsklasse" was not one of: [%s]';
    const MINIMALER_JGAK_ERROR    = '"Minimaler Jg/Ak" must be set.';

    const WERTUNGSKLASSE_OPTIONS = [
        'JG', // Jahrgang
        'AK', // Altersklasse
    ];

    /**
     * @var Zahl
     *      Nummer der Mannschaft, z.B. 1 für 1. Mannschaft,2 für 2. Mannschaft.
     *
     *      - REQUIRED -
     */
    private $mannschaftsnummer;

    /**
     * @var Zahl
     *      Eindeutige numerische Kennung für die Staffel innerhalb
     *      dieser Veranstaltung zum Zwecke der Zuordnung der
     *      Wettkämpfe.
     *
     *      - REQUIRED -
     */
    private $staffelnummer;

    /**
     * @var Zeichenkette
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      - JG = Jahrgang,
     *      - AK = Altersklasse
     *
     *      - REQUIRED -
     */
    private $wertungsklasse;

    /**
     * @var JGAK
     *      Kleinster Jahrgang/größte Altersklasse
     *      Bei MastersStaffeln müssen Mindest-Altersklasse und
     *      maximale Altersklasse identisch sein! (für die offene
     *      Klasse wird hier 0 angegeben).
     *
     *      - REQUIRED -
     */
    private $minimaler_jgak;

    /**
     * @var JGAK
     *      Unterlassungswert = Mindest-Jahrgang/Altersklasse
     *      Ansonsten größter Jahrgang/kleinste Altersklasse (0 für
     *      „und jünger“).
     */
    private $maximaler_jgak;

    /**
     * Ausschreibungimnetz constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->mannschaftsnummer = new Zahl();
        $this->staffelnummer     = new Zahl();
        $this->wertungsklasse    = new Zeichenkette();
        $this->minimaler_jgak    = new JGAK();
        $this->maximaler_jgak    = new JGAK();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 5;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3, 4];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $mannschaftsnummer
     * @throws Runtime_Exception
     */
    public function check_mannschaftsnummer(Zahl $mannschaftsnummer): void
    {
        if ( !$mannschaftsnummer->is_set() )
        {
            throw new Runtime_Exception(self::MANNSCHAFTSNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $mannschaftsnummer
     * @throws Runtime_Exception
     */
    public function set_mannschaftsnummer(Zahl $mannschaftsnummer): void
    {
        $this->check_mannschaftsnummer($mannschaftsnummer);
        $this->mannschaftsnummer = $mannschaftsnummer;
    }

    /**
     * @return Zahl
     */
    public function get_mannschaftsnummer(): Zahl
    {
        return $this->mannschaftsnummer;
    }

    /**
     * @param Zahl $staffelnummer
     * @throws Runtime_Exception
     */
    public function check_staffelnummer(Zahl $staffelnummer): void
    {
        $correct = false;
        if ( $staffelnummer->is_set() )
        {
            $search_result = $this->get_parent()->search_staffel($staffelnummer, false);
            $correct       = ($search_result === null or $search_result === $this);
        }
        if ( !$correct )
        {
            throw new Runtime_Exception(self::STAFFELNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $staffelnummer
     * @throws Runtime_Exception
     */
    public function set_staffelnummer(Zahl $staffelnummer): void
    {
        $this->check_staffelnummer($staffelnummer);
        $this->staffelnummer = $staffelnummer;
    }

    /**
     * @return Zahl
     */
    public function get_staffelnummer(): Zahl
    {
        return $this->staffelnummer;
    }

    /**
     * @param Zeichenkette $wertungsklasse
     * @throws Runtime_Exception
     */
    public function check_wertungsklasse(Zeichenkette $wertungsklasse): void
    {
        if ( !in_array($wertungsklasse->get_formatted(), self::WERTUNGSKLASSE_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::WERTUNGSKLASSE_ERROR, implode(', ', self::WERTUNGSKLASSE_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichenkette $wertungsklasse
     * @throws Runtime_Exception
     */
    public function set_wertungsklasse(Zeichenkette $wertungsklasse): void
    {
        $this->check_wertungsklasse($wertungsklasse);
        $this->wertungsklasse = $wertungsklasse;
    }

    /**
     * @return Zeichenkette
     */
    public function get_wertungsklasse(): Zeichenkette
    {
        return $this->wertungsklasse;
    }

    /**
     * @param JGAK $minimaler_jgak
     * @throws Runtime_Exception
     */
    public function check_minimaler_jgak(JGAK $minimaler_jgak): void
    {
        if ( !$minimaler_jgak->is_set() )
        {
            throw new Runtime_Exception(self::MINIMALER_JGAK_ERROR);
        }
    }

    /**
     * @param JGAK $minimaler_jgak
     * @throws Runtime_Exception
     */
    public function set_minimaler_jgak(JGAK $minimaler_jgak): void
    {
        $this->check_minimaler_jgak($minimaler_jgak);
        $this->minimaler_jgak = $minimaler_jgak;

        // If $maximaler_jgak was not jet set, set it to be equal to $minimaler_jgak.
        if ( !$this->maximaler_jgak->is_set() )
        {
            $this->maximaler_jgak->set_from_string($minimaler_jgak->get_formatted());
        }
    }

    /**
     * @return JGAK
     */
    public function get_minimaler_jgak(): JGAK
    {
        return $this->minimaler_jgak;
    }

    /**
     * @param JGAK $maximaler_jgak
     */
    public function set_maximaler_jgak(JGAK $maximaler_jgak): void
    {
        $this->maximaler_jgak = $maximaler_jgak;
    }

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * @return JGAK
     */
    public function get_maximaler_jgak(): JGAK
    {
        if ( !$this->maximaler_jgak->is_set() )
        {
            /** @noinspection PhpUnhandledExceptionInspection */
            $this->maximaler_jgak->set_from_string($this->minimaler_jgak->get_formatted());
        }
        return $this->maximaler_jgak;
    }

}