<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Erzeuger
 * @package Lukaspotthast\DSV\Document\Wettkampfdefinitionsliste\Elements
 */
class Erzeuger extends Document_Element
{

    const SOFTWARE_ERROR = '"Software" must be specified.';
    const VERSION_ERROR  = '"Version" must be specified.';
    const KONTAKT_ERROR  = '"Kontakt" must be specified.';

    /**
     * @var Zeichenkette
     *      Name der Software, die die Datei erzeugt hat.
     *      - REQUIRED -
     */
    private $software;

    /**
     * @var Zeichenkette
     *      Versionskennung der Software.
     *      - REQUIRED -
     */
    private $version;

    /**
     * @var Zeichenkette
     *      E-Mail-Adresse des Software-Herstellers.
     *      - REQUIRED -
     */
    private $kontakt;

    /**
     * Erzeuger constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->software = new Zeichenkette();
        $this->version  = new Zeichenkette();
        $this->kontakt  = new Zeichenkette();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 3;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zeichenkette $software
     * @throws Runtime_Exception
     */
    public function check_software(Zeichenkette $software): void
    {
        if ( !strlen($software->get_formatted()) > 0 )
        {
            throw new Runtime_Exception(self::SOFTWARE_ERROR);
        }
    }

    /**
     * @param Zeichenkette $software
     * @throws Runtime_Exception
     */
    public function set_software(Zeichenkette $software): void
    {
        $this->check_software($software);
        $this->software = $software;
    }

    /**
     * @return Zeichenkette
     */
    public function get_software(): Zeichenkette
    {
        return $this->software;
    }

    /**
     * @param Zeichenkette $version
     * @throws Runtime_Exception
     */
    public function check_version(Zeichenkette $version): void
    {
        if ( !strlen($version->get_formatted()) > 0 )
        {
            throw new Runtime_Exception(self::VERSION_ERROR);
        }
    }

    /**
     * @param Zeichenkette $version
     * @throws Runtime_Exception
     */
    public function set_version(Zeichenkette $version): void
    {
        $this->check_version($version);
        $this->version = $version;
    }

    /**
     * @return Zeichenkette
     */
    public function get_version(): Zeichenkette
    {
        return $this->version;
    }

    /**
     * @param Zeichenkette $kontakt
     * @throws Runtime_Exception
     */
    public function check_kontakt(Zeichenkette $kontakt): void
    {
        if ( !strlen($kontakt->get_formatted()) > 0 )
        {
            throw new Runtime_Exception(self::KONTAKT_ERROR);
        }
    }

    /**
     * @param Zeichenkette $kontakt
     * @throws Runtime_Exception
     */
    public function set_kontakt(Zeichenkette $kontakt): void
    {
        $this->check_kontakt($kontakt);
        $this->kontakt = $kontakt;
    }

    /**
     * @return Zeichenkette
     */
    public function get_kontakt(): Zeichenkette
    {
        return $this->kontakt;
    }

}