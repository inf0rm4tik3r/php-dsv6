<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\IOC_Codes;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Ansprechpartner
 * @package Lukaspotthast\DSV\Document\Element
 */
class Ansprechpartner extends Document_Element
{

    const NAME_ERROR  = '"Name" must be set.';
    const LAND_ERROR  = '"Land" must either be blank or specify an IOC country code.';
    const EMAIL_ERROR = '"Email" must be set.';

    /**
     * @var Zeichenkette
     *      Nachname, Vorname der meldenden Person.
     *
     *      - REQUIRED -
     */
    private $name;

    /**
     * @var Zeichenkette
     *      Straße der meldenden Person.
     */
    private $strasse;

    /**
     * @var Zeichenkette
     *      PLZ der meldenden Person.
     */
    private $plz;

    /**
     * @var Zeichenkette
     *      Ort der meldenden Person.
     */
    private $ort;

    /**
     * @var Zeichenkette
     *      Das Land (Nation) als FINA-Kürzel, z.B. ‚GER’ für Deutschland.
     */
    private $land;

    /**
     * @var Zeichenkette
     *      Telefonnummer der meldenden Person.
     */
    private $telefon;

    /**
     * @var Zeichenkette
     *      Fax der meldenden Person.
     */
    private $fax;

    /**
     * @var Zeichenkette
     *      E-Mail Adresse der meldenden Person.
     *
     *      - REQUIRED -
     */
    private $email;

    /**
     * Ansprechpartner constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->name    = new Zeichenkette();
        $this->strasse = new Zeichenkette();
        $this->plz     = new Zeichenkette();
        $this->ort     = new Zeichenkette();
        $this->land    = new Zeichenkette();
        $this->telefon = new Zeichenkette();
        $this->fax     = new Zeichenkette();
        $this->email   = new Zeichenkette();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 8;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 8];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zeichenkette $name
     * @throws Runtime_Exception
     */
    public function check_name(Zeichenkette $name): void
    {
        if ( strlen($name->get_formatted()) === 0 )
        {
            throw new Runtime_Exception(self::NAME_ERROR);
        }
    }

    /**
     * @param Zeichenkette $name
     * @throws Runtime_Exception
     */
    public function set_name(Zeichenkette $name): void
    {
        $this->check_name($name);
        $this->name = $name;
    }

    /**
     * @return Zeichenkette
     */
    public function get_name(): Zeichenkette
    {
        return $this->name;
    }

    /**
     * @param Zeichenkette $strasse
     */
    public function set_strasse(Zeichenkette $strasse): void
    {
        $this->strasse = $strasse;
    }

    /**
     * @return Zeichenkette
     */
    public function get_strasse(): Zeichenkette
    {
        return $this->strasse;
    }

    /**
     * @param Zeichenkette $plz
     */
    public function set_plz(Zeichenkette $plz): void
    {
        $this->plz = $plz;
    }

    /**
     * @return Zeichenkette
     */
    public function get_plz(): Zeichenkette
    {
        return $this->plz;
    }

    /**
     * @param Zeichenkette $ort
     */
    public function set_ort(Zeichenkette $ort): void
    {
        $this->ort = $ort;
    }

    /**
     * @return Zeichenkette
     */
    public function get_ort(): Zeichenkette
    {
        return $this->ort;
    }

    /**
     * @param Zeichenkette $land
     * @throws Runtime_Exception
     */
    public function check_land(Zeichenkette $land): void
    {
        if ( $land->get_formatted() !== '' and
             !IOC_Codes::is_ioc_code($land->get_formatted()) )
        {
            throw new Runtime_Exception(self::LAND_ERROR);
        }
    }

    /**
     * @param Zeichenkette $land
     * @throws Runtime_Exception
     */
    public function set_land(Zeichenkette $land): void
    {
        $this->check_land($land);
        $this->land = $land;
    }

    /**
     * @return Zeichenkette
     */
    public function get_land(): Zeichenkette
    {
        return $this->land;
    }

    /**
     * @param Zeichenkette $telefon
     */
    public function set_telefon(Zeichenkette $telefon): void
    {
        $this->telefon = $telefon;
    }

    /**
     * @return Zeichenkette
     */
    public function get_telefon(): Zeichenkette
    {
        return $this->telefon;
    }

    /**
     * @param Zeichenkette $fax
     */
    public function set_fax(Zeichenkette $fax): void
    {
        $this->fax = $fax;
    }

    /**
     * @return Zeichenkette
     */
    public function get_fax(): Zeichenkette
    {
        return $this->fax;
    }

    /**
     * @param Zeichenkette $email
     * @throws Runtime_Exception
     */
    public function check_email(Zeichenkette $email): void
    {
        if ( strlen($email->get_formatted()) === 0 )
        {
            throw new Runtime_Exception(self::EMAIL_ERROR);
        }
    }

    /**
     * @param Zeichenkette $email
     * @throws Runtime_Exception
     */
    public function set_email(Zeichenkette $email): void
    {
        $this->check_email($email);
        $this->email = $email;
    }

    /**
     * @return Zeichenkette
     */
    public function get_email(): Zeichenkette
    {
        return $this->email;
    }

}