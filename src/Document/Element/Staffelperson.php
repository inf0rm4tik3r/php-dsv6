<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichen;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Staffelperson
 * @package Lukaspotthast\DSV\Document\Element
 */
class Staffelperson extends Document_Element
{

    const STAFFELNUMMER_ERROR          = '"Staffelnummer" was not set or did not specify an existing "Staffel".';
    const WETTKAMPFNUMMER_ERROR        = '"Wettkampfnummer" was not set or did not specify an existing "Wettkampf".';
    const WETTKAMPFART_ERROR           = '"Wettkampfart" was not one of: [%s].';
    const NAME_ERROR                   = '"Name" was not specified.';
    const DSV_ID_ERROR                 = '"DSV ID" was either not specified or was not of 6 digits.';
    const STARTNUMMER_IN_STAFFEL_ERROR = '"Startnummer in Staffel" was not specified.';
    const GESCHLECHT_ERROR             = '"Geschlecht" was not one of: [%s]';
    const JAHRGANG_ERROR               = '"Jahrgang" was not specified.';

    const WETTKAMPFART_OPTIONS = [
        'V', // Vorlauf
        'Z', // Zwischenlauf
        'F', // Finale
        'E', // Entscheidung
        'A', // Ausschwimmen
        'N', // Nachschwimmen
    ];

    const GESCHLECHT_OPTIONS = [
        'M', // männlich
        'W', // weiblich
    ];

    /**
     * @var Zahl
     *      Eindeutige numerische Kennung für die Staffel innerhalb
     *      dieser Veranstaltung definiert in STAFFEL.
     *
     *      - REQUIRED -
     */
    private $staffelnummer;

    /**
     * @var Zahl
     *      Nummer des Wettkampfes.
     *
     *      - REQUIRED -
     */
    private $wettkampfnummer;

    /**
     * @var Zeichen
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      - V = Vorlauf,
     *      - Z = Zwischenlauf,
     *      - F = Finale,
     *      - E = Entscheidung,
     *      - A = Ausschwimmen,
     *      - N = Nachschwimmen
     *
     *      - REQUIRED -
     */
    private $wettkampfart;

    /**
     * @var Zeichenkette
     *      Nachname, Vorname des Teilnehmers.
     *
     *      - REQUIRED -
     */
    private $name;

    /**
     * @var Zahl
     *      6-stellige durch den DSV für jeden Schwimmer festgelegte
     *      Zahl. Ist keine DSV-ID des Schwimmers bekannt, muss 0
     *      angegeben werden.
     *
     *      - REQUIRED -
     */
    private $dsv_id;

    /**
     * @var Zahl
     *      Startnummer des Schwimmers innerhalb der Staffel.
     *
     *      - REQUIRED -
     */
    private $startnummer_in_staffel;

    /**
     * @var Zeichen
     *      Zulässige Werte:
     *      - M = männlich,
     *      - W = weiblich
     *
     *      - REQUIRED -
     */
    private $geschlecht;

    /**
     * @var Zahl
     *      Der Jahrgang ist als 4-stellige Zahl anzugeben, z.B. 1990.
     *
     *      - REQUIRED -
     */
    private $jahrgang;

    /**
     * @var Zahl
     */
    private $altersklasse;

    /**
     * Ausschreibungimnetz constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->staffelnummer          = new Zahl();
        $this->wettkampfnummer        = new Zahl();
        $this->wettkampfart           = new Zeichen();
        $this->name                   = new Zeichenkette();
        $this->dsv_id                 = new Zahl();
        $this->startnummer_in_staffel = new Zahl();
        $this->geschlecht             = new Zeichen();
        $this->jahrgang               = new Zahl();
        $this->altersklasse           = new Zahl();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 9;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3, 4, 5, 6, 7, 8];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $staffelnummer
     * @throws Runtime_Exception
     */
    public function check_staffelnummer(Zahl $staffelnummer): void
    {
        if ( !($staffelnummer->is_set() and $this->get_parent()->search_staffel($staffelnummer) !== null) )
        {
            throw new Runtime_Exception(self::STAFFELNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $staffelnummer
     * @throws Runtime_Exception
     */
    public function set_staffelnummer(Zahl $staffelnummer): void
    {
        $this->check_staffelnummer($staffelnummer);
        $this->staffelnummer = $staffelnummer;
    }

    /**
     * @return Zahl
     */
    public function get_staffelnummer(): Zahl
    {
        return $this->staffelnummer;
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function check_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        if ( !($wettkampfnummer->is_set() and $this->get_parent()->search_wettkampf($wettkampfnummer) !== null) )
        {
            throw new Runtime_Exception(self::WETTKAMPFNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function set_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        $this->check_wettkampfnummer($wettkampfnummer);
        $this->wettkampfnummer = $wettkampfnummer;
    }

    /**
     * @return Zahl
     */
    public function get_wettkampfnummer(): Zahl
    {
        return $this->wettkampfnummer;
    }

    /**
     * @param Zeichen $wettkampfart
     * @throws Runtime_Exception
     */
    public function check_wettkampfart(Zeichen $wettkampfart): void
    {
        if ( !in_array($wettkampfart->get_formatted(), self::WETTKAMPFART_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::WETTKAMPFART_ERROR, implode(', ', self::WETTKAMPFART_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $wettkampfart
     * @throws Runtime_Exception
     */
    public function set_wettkampfart(Zeichen $wettkampfart): void
    {
        $this->check_wettkampfart($wettkampfart);
        $this->wettkampfart = $wettkampfart;
    }

    /**
     * @return Zeichen
     */
    public function get_wettkampfart(): Zeichen
    {
        return $this->wettkampfart;
    }

    /**
     * @param Zeichenkette $name
     * @throws Runtime_Exception
     */
    public function check_name(Zeichenkette $name): void
    {
        if ( !$name->is_set() )
        {
            throw new Runtime_Exception(self::NAME_ERROR);
        }
    }

    /**
     * @param Zeichenkette $name
     * @throws Runtime_Exception
     */
    public function set_name(Zeichenkette $name): void
    {
        $this->check_name($name);
        $this->name = $name;
    }

    /**
     * @return Zeichenkette
     */
    public function get_name(): Zeichenkette
    {
        return $this->name;
    }

    /**
     * @param Zahl $dsv_id
     * @throws Runtime_Exception
     */
    public function check_dsv_id(Zahl $dsv_id): void
    {
        if ( !strlen($dsv_id->get_formatted()) === 6 )
        {
            throw new Runtime_Exception(self::DSV_ID_ERROR);
        }
    }

    /**
     * @param Zahl $dsv_id
     * @throws Runtime_Exception
     */
    public function set_dsv_id(Zahl $dsv_id): void
    {
        $this->check_dsv_id($dsv_id);
        $this->dsv_id = $dsv_id;
    }

    /**
     * @return Zahl
     */
    public function get_dsv_id(): Zahl
    {
        return $this->dsv_id;
    }

    /**
     * @param Zahl $startnummer_in_staffel
     * @throws Runtime_Exception
     */
    public function check_startnummer_in_staffel(Zahl $startnummer_in_staffel): void
    {
        if ( !$startnummer_in_staffel->is_set() )
        {
            throw new Runtime_Exception(self::STARTNUMMER_IN_STAFFEL_ERROR);
        }
    }

    /**
     * @param Zahl $startnummer_in_staffel
     * @throws Runtime_Exception
     */
    public function set_startnummer_in_staffel(Zahl $startnummer_in_staffel): void
    {
        $this->check_startnummer_in_staffel($startnummer_in_staffel);
        $this->startnummer_in_staffel = $startnummer_in_staffel;
    }

    /**
     * @return Zahl
     */
    public function get_startnummer_in_staffel(): Zahl
    {
        return $this->startnummer_in_staffel;
    }

    /**
     * @param Zeichen $geschlecht
     * @throws Runtime_Exception
     */
    public function check_geschlecht(Zeichen $geschlecht): void
    {
        if ( !in_array($geschlecht->get_formatted(), self::GESCHLECHT_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::GESCHLECHT_ERROR, implode(', ', self::GESCHLECHT_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $geschlecht
     * @throws Runtime_Exception
     */
    public function set_geschlecht(Zeichen $geschlecht): void
    {
        $this->check_geschlecht($geschlecht);
        $this->geschlecht = $geschlecht;
    }

    /**
     * @return Zeichen
     */
    public function get_geschlecht(): Zeichen
    {
        return $this->geschlecht;
    }

    /**
     * @param Zahl $jahrgang
     * @throws Runtime_Exception
     */
    public function check_jahrgang(Zahl $jahrgang)
    {
        if ( !$jahrgang->is_set() )
        {
            throw new Runtime_Exception(self::JAHRGANG_ERROR);
        }
    }

    /**
     * @param Zahl $jahrgang
     * @throws Runtime_Exception
     */
    public function set_jahrgang(Zahl $jahrgang): void
    {
        $this->check_jahrgang($jahrgang);
        $this->jahrgang = $jahrgang;
    }

    /**
     * @return Zahl
     */
    public function get_jahrgang(): Zahl
    {
        return $this->jahrgang;
    }

    /**
     * @param Zahl $altersklasse
     */
    public function set_altersklasse(Zahl $altersklasse): void
    {
        $this->altersklasse = $altersklasse;
    }

    /**
     * @return Zahl
     */
    public function get_altersklasse(): Zahl
    {
        return $this->altersklasse;
    }

}