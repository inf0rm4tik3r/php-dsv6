<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class STPerson
 * @package Lukaspotthast\DSV\Document\Element
 */
class STPerson extends Document_Element
{

    const STAFFELNUMMER_ERROR          = '"Staffelnummer" was not set or did not specify an existing "STMeldung".';
    const WETTKAMPFNUMMER_ERROR        = '"Wettkampfnummer" was not set or did not specify an existing "Wettkampf".';
    const PERSONENNUMMER_ERROR         = '"Personennummer" was not set or did not specify an existing "PNMeldung".';
    const STARTNUMMER_IN_STAFFEL_ERROR = '"Startnummer in Staffel" was not specified.';

    /**
     * @var Zahl
     *      Eindeutige numerische Kennung für die Staffel innerhalb
     *      dieser Veranstaltung definiert in STMELDUNG.
     *
     *      - REQUIRED -
     */
    private $staffelnummer;

    /**
     * @var Zahl
     *      Nummer des Wettkampfes.
     *
     *      - REQUIRED -
     */
    private $wettkampfnummer;

    /**
     * @var Zahl
     *      Eindeutige numerische Kennung für den Schwimmer
     *      innerhalb dieser Veranstaltung definiert in PNMELDUNG.
     *
     *      - REQUIRED -
     */
    private $personennummer;

    /**
     * @var Zahl
     *      Startnummer des Schwimmers innerhalb der Staffel.
     *
     *      - REQUIRED -
     */
    private $startnummer_in_staffel;

    /**
     * Ausschreibungimnetz constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->staffelnummer          = new Zahl();
        $this->wettkampfnummer        = new Zahl();
        $this->personennummer         = new Zahl();
        $this->startnummer_in_staffel = new Zahl();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return string
     */
    public static function override_get_element_name(): string
    {
        return 'STAFFELPERSON';
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 4;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3, 4];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $staffelnummer
     * @throws Runtime_Exception
     */
    public function check_staffelnummer(Zahl $staffelnummer): void
    {
        if ( !($staffelnummer->is_set() and $this->get_parent()->search_stmeldung($staffelnummer) !== null) )
        {
            throw new Runtime_Exception(self::STAFFELNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $staffelnummer
     * @throws Runtime_Exception
     */
    public function set_staffelnummer(Zahl $staffelnummer): void
    {
        $this->check_staffelnummer($staffelnummer);
        $this->staffelnummer = $staffelnummer;
    }

    /**
     * @return Zahl
     */
    public function get_staffelnummer(): Zahl
    {
        return $this->staffelnummer;
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function check_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        if ( !($wettkampfnummer->is_set() and $this->get_parent()->search_wettkampf($wettkampfnummer) !== null) )
        {
            throw new Runtime_Exception(self::WETTKAMPFNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function set_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        $this->check_wettkampfnummer($wettkampfnummer);
        $this->wettkampfnummer = $wettkampfnummer;
    }

    /**
     * @return Zahl
     */
    public function get_wettkampfnummer(): Zahl
    {
        return $this->wettkampfnummer;
    }

    /**
     * @param Zahl $personennummer
     * @throws Runtime_Exception
     */
    public function check_personennummer(Zahl $personennummer): void
    {
        if ( !($personennummer->is_set() and $this->get_parent()->search_pnmeldung($personennummer) !== null) )
        {
            throw new Runtime_Exception(self::WETTKAMPFNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $personennummer
     * @throws Runtime_Exception
     */
    public function set_personennummer(Zahl $personennummer): void
    {
        $this->check_personennummer($personennummer);
        $this->personennummer = $personennummer;
    }

    /**
     * @return Zahl
     */
    public function get_personennummer(): Zahl
    {
        return $this->personennummer;
    }

    /**
     * @param Zahl $startnummer_in_staffel
     * @throws Runtime_Exception
     */
    public function check_startnummer_in_staffel(Zahl $startnummer_in_staffel): void
    {
        if ( !$startnummer_in_staffel->is_set() )
        {
            throw new Runtime_Exception(self::STARTNUMMER_IN_STAFFEL_ERROR);
        }
    }

    /**
     * @param Zahl $startnummer_in_staffel
     * @throws Runtime_Exception
     */
    public function set_startnummer_in_staffel(Zahl $startnummer_in_staffel): void
    {
        $this->check_startnummer_in_staffel($startnummer_in_staffel);
        $this->startnummer_in_staffel = $startnummer_in_staffel;
    }

    /**
     * @return Zahl
     */
    public function get_startnummer_in_staffel(): Zahl
    {
        return $this->startnummer_in_staffel;
    }

}