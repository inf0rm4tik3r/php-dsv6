<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichen;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Wettkampf_Definition
 * @package Lukaspotthast\DSV\Document\Element
 */
class Wettkampf_Definition extends Document_Element
{

    const WETTKAMPFNUMMER_ERROR               = '"Wettkampfnummer" must be set, be unique, and must not exceed three digits.';
    const WETTKAMPFART_ERROR                  = '"Wettkampfart" must be one of: [%s]';
    const ABSCHNITTSNUMMER_ERROR              = '"Abschnittsnummer" must be set and specify an existing "ABSCHNITT".';
    const ANZAHL_STARTER_ERROR                = '"Anzahl Starter" must be set.';
    const EINZELSTRECKE_ERROR                 = '"Einzelstrecke" \'%s\' did not represent a number between $d and $d.';
    const TECHNIK_ERROR                       = '"Technik" was not one of: [%s]';
    const AUSUEBUNG_ERROR                     = '"Ausuebung" was not one of: [%s]';
    const GESCHLECHT_ERROR                    = '"Geschlecht" was not one of: [%s]';
    const ZUORDNUNG_BESTENLISTE_ERROR         = '"Zuordnung Bestenliste" was not one of: [%s]';
    const QUALIFIKATIONSWETTKAMPFNUMMER_ERROR = '"Qualifikationswettkampfnummer" must be set when "Wettkampfart" is of ' .
                                                '"Z"(Zwischenlauf) or "F"(Finals) and must represent an existing Wettkampf.';
    const QUALIFIKATIONSWETTKAMPFART_ERROR    = '"Qualifikationswettkampfart" was not one of: [%s] or was not equal to the ' .
                                                '"Wettkampfart" of the referenced "Wettkampf".';

    const WETTKAMPFART_OPTIONS = [
        'V', // Vorlauf
        'Z', // Zwischenlauf
        'F', // Finale
        'E', // Entscheidung
    ];

    const EINZELSTRECKE_MIN = 0;
    const EINZELSTRECKE_MAX = 25000;

    const TECHNIK_OPTIONS = [
        'F', // Freistil
        'R', // Rücken
        'B', // Brust
        'S', // Schmetterling
        'L', // Lagen
        'X', // Beliebige Sonderform
    ];

    const AUSUEBUNG_OPTIONS = [
        'GL', // ganze Lage
        'BE', // Beine
        'AR', // Arme
        'ST', // Start
        'WE', // Wende
        'GB', // Gleitübung
        'X',  // Beliebige Sonderform
    ];

    const GESCHLECHT_OPTIONS = [
        'M', // männlich
        'W', // weiblich
        'X', // gemischte Wettkämpfe
    ];

    const ZUORDNUNG_BESTENLISTE_OPTIONS = [
        'SW', // Schwimmen für Jugend und offene Klasse (Standard!)
        'MS', // Schwimmen der Masters
        'FS', // Freiwasserschwimmen
        'KG', // reiner kindgerechter Wettkampf
        'XX', // Andere, wie Schule / Universität / Organisationen (z.B. Polizei)
    ];

    /**
     * @var Zahl
     *      Nummer des Wettkampfes, maximal dreistellig. Muss eindeutig sein.
     *
     *      - REQUIRED -
     */
    private $wettkampfnummer;

    /**
     * @var Zeichen
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      V = Vorlauf,
     *      Z = Zwischenlauf,
     *      F = Finale,
     *      E = Entscheidung
     *
     *      - REQUIRED -
     */
    private $wettkampfart;

    /**
     * @var Zahl
     *      Nummer des Abschnitts. Der referenzierte Abschnitt muss existieren.
     *
     *      - REQUIRED -
     */
    private $abschnittsnummer;

    /**
     * @var Zahl
     *      1 für Einzeldisziplin, ansonsten Anzahl der Staffelteilnehmer.
     */
    private $anzahl_starter;

    /**
     * @var Zahl
     *      Strecke in Metern.
     *      Wertebereich: (0) - 25000 (0 für sonstige)
     *
     *      - REQUIRED -
     */
    private $einzelstrecke;

    /**
     * @var Zeichen
     *      Zulässige Werte:
     *      F = Freistil,
     *      R = Rücken,
     *      B = Brust,
     *      S = Schmetterling,
     *      L = Lagen,
     *      X = beliebige Sonderform
     *
     *      - REQUIRED -
     */
    private $technik;

    /**
     * @var Zeichenkette
     *      Zulässige Werte:
     *      GL = ganze Lage,
     *      BE = Beine,
     *      AR = Arme,
     *      ST = Start
     *      WE = Wende
     *      GB = Gleitübung
     *       X = beliebige Sonderform
     *
     *      - REQUIRED -
     */
    private $ausuebung;

    /**
     * @var Zeichen
     *       Zulässige Werte:
     *       M = männlich,
     *       W = weiblich,
     *       X = gemischte Wettkämpfe
     *
     *      - REQUIRED -
     */
    private $geschlecht;

    /**
     * @var Zeichenkette
     *      Zuordnung für Bestenlistenauswertungen mit
     *      SW = Schwimmen für Jugend und offene Klasse (Standard!)
     *      MS = Schwimmen der Masters
     *      FS = Freiwasserschwimmen
     *      KG = reiner kindgerechter Wettkampf
     *      XX = Andere, wie Schule / Universität / Organisationen (z.B. Polizei)
     *      Falls gemischte Wettkämpfe ist SW anzugeben.
     *
     *      - REQUIRED -
     */
    private $zuordnung_bestenliste;

    /**
     * @var Zahl
     *      Bei Zwischenläufen und Finals muss hier die
     *      Wettkampf-Nr. des Zwischenlaufs bzw. des Vorlaufs
     *      angegeben werden, aus dem man sich für diesen
     *      Wettkampf qualifiziert hat.
     */
    private $qualifikationswettkampfnummer;

    /**
     * @var Zeichen
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      V = Vorlauf,
     *      Z = Zwischenlauf,
     *      F = Finale,
     *      E = Entscheidung
     */
    private $qualifikationswettkampfart;

    /**
     * Wettkampf constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->wettkampfnummer               = new Zahl();
        $this->wettkampfart                  = new Zeichen();
        $this->abschnittsnummer              = new Zahl();
        $this->anzahl_starter                = new Zahl('1');
        $this->einzelstrecke                 = new Zahl();
        $this->technik                       = new Zeichen();
        $this->ausuebung                     = new Zeichenkette();
        $this->geschlecht                    = new Zeichen();
        $this->zuordnung_bestenliste         = new Zeichenkette('SW');
        $this->qualifikationswettkampfnummer = new Zahl();
        $this->qualifikationswettkampfart    = new Zeichen();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return string
     */
    public static function override_get_element_name(): string
    {
        return 'WETTKAMPF';
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 11;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3, 5, 6, 7, 8, 9];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function check_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        $correct = false;
        if ( $wettkampfnummer->is_set() )
        {
            $length        = strlen($wettkampfnummer->get_formatted());
            $search_result = $this->get_parent()->search_wettkampf($wettkampfnummer, false);
            $correct       = (($length >= 0 and $length <= 3) and
                              ($search_result === null or $search_result === $this));
        }
        if ( !$correct )
        {
            throw new Runtime_Exception(self::WETTKAMPFNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function set_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        $this->check_wettkampfnummer($wettkampfnummer);
        $this->wettkampfnummer = $wettkampfnummer;
    }

    /**
     * @return Zahl
     */
    public function get_wettkampfnummer(): Zahl
    {
        return $this->wettkampfnummer;
    }

    /**
     * @param Zeichen $wettkampfart
     * @throws Runtime_Exception
     */
    public function check_wettkampfart(Zeichen $wettkampfart): void
    {
        if ( !in_array($wettkampfart->get_formatted(), self::WETTKAMPFART_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::WETTKAMPFART_ERROR, implode(', ', self::WETTKAMPFART_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $wettkampfart
     * @throws Runtime_Exception
     */
    public function set_wettkampfart(Zeichen $wettkampfart): void
    {
        $this->check_wettkampfart($wettkampfart);
        $this->wettkampfart = $wettkampfart;
    }

    /**
     * @return Zeichen
     */
    public function get_wettkampfart(): Zeichen
    {
        return $this->wettkampfart;
    }

    /**
     * @param Zahl $abschnittsnummer
     * @throws Runtime_Exception
     */
    public function check_abschnittsnummer(Zahl $abschnittsnummer): void
    {
        $correct = false;
        if ( $abschnittsnummer->is_set() )
        {
            $search_result = $this->get_parent()->search_abschnitt($abschnittsnummer, false);
            $correct       = $search_result !== null;
        }
        if ( !$correct )
        {
            throw new Runtime_Exception(self::ABSCHNITTSNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $abschnittsnummer
     * @throws Runtime_Exception
     */
    public function set_abschnittsnummer(Zahl $abschnittsnummer): void
    {
        $this->check_abschnittsnummer($abschnittsnummer);
        $this->abschnittsnummer = $abschnittsnummer;
    }

    /**
     * @return Zahl
     */
    public function get_abschnittsnummer(): Zahl
    {
        return $this->abschnittsnummer;
    }

    /**
     * @param Zahl $anzahl_starter
     * @throws Runtime_Exception
     */
    function check_anzahl_starter(Zahl $anzahl_starter): void
    {
        if ( !$anzahl_starter->is_set() )
        {
            throw new Runtime_Exception(self::ANZAHL_STARTER_ERROR);
        }
    }

    /**
     * @param Zahl $anzahl_starter
     * @throws Runtime_Exception
     */
    public function set_anzahl_starter(Zahl $anzahl_starter): void
    {
        $this->check_anzahl_starter($anzahl_starter);
        $this->anzahl_starter = $anzahl_starter;
    }

    /**
     * @return Zahl
     */
    public function get_anzahl_starter(): Zahl
    {
        return $this->anzahl_starter;
    }

    /**
     * @param Zahl $einzelstrecke
     * @throws Runtime_Exception
     */
    public function check_einzelstrecke(Zahl $einzelstrecke): void
    {
        if ( !($einzelstrecke->is_set() and
               $einzelstrecke->get_zahl() >= self::EINZELSTRECKE_MIN and
               $einzelstrecke->get_zahl() <= self::EINZELSTRECKE_MAX) )
        {
            throw new Runtime_Exception(
                sprintf(
                    self::EINZELSTRECKE_ERROR,
                    $einzelstrecke->get_formatted(),
                    self::EINZELSTRECKE_MIN,
                    self::EINZELSTRECKE_MAX
                )
            );
        }
    }

    /**
     * @param Zahl $einzelstrecke
     * @throws Runtime_Exception
     */
    public function set_einzelstrecke(Zahl $einzelstrecke): void
    {
        $this->check_einzelstrecke($einzelstrecke);
        $this->einzelstrecke = $einzelstrecke;
    }

    /**
     * @return Zahl
     */
    public function get_einzelstrecke(): Zahl
    {
        return $this->einzelstrecke;
    }

    /**
     * @param Zeichen $technik
     * @throws Runtime_Exception
     */
    public function check_technik(Zeichen $technik): void
    {
        if ( !in_array($technik->get_formatted(), self::TECHNIK_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::TECHNIK_ERROR, implode(', ', self::TECHNIK_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $technik
     * @throws Runtime_Exception
     */
    public function set_technik(Zeichen $technik): void
    {
        $this->check_technik($technik);
        $this->technik = $technik;
    }

    /**
     * @return Zeichen
     */
    public function get_technik(): Zeichen
    {
        return $this->technik;
    }

    /**
     * @param Zeichenkette $ausuebung
     * @throws Runtime_Exception
     */
    public function check_ausuebung(Zeichenkette $ausuebung): void
    {
        if ( !in_array($ausuebung->get_formatted(), self::AUSUEBUNG_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::AUSUEBUNG_ERROR, implode(', ', self::AUSUEBUNG_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichenkette $ausuebung
     * @throws Runtime_Exception
     */
    public function set_ausuebung(Zeichenkette $ausuebung): void
    {
        $this->check_ausuebung($ausuebung);
        $this->ausuebung = $ausuebung;
    }

    /**
     * @return Zeichenkette
     */
    public function get_ausuebung(): Zeichenkette
    {
        return $this->ausuebung;
    }

    /**
     * @param Zeichen $geschlecht
     * @throws Runtime_Exception
     */
    public function check_geschlecht(Zeichen $geschlecht): void
    {
        if ( !in_array($geschlecht->get_formatted(), self::GESCHLECHT_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::GESCHLECHT_ERROR, implode(', ', self::GESCHLECHT_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $geschlecht
     * @throws Runtime_Exception
     */
    public function set_geschlecht(Zeichen $geschlecht): void
    {
        $this->check_geschlecht($geschlecht);
        $this->geschlecht = $geschlecht;
    }

    /**
     * @return Zeichen
     */
    public function get_geschlecht(): Zeichen
    {
        return $this->geschlecht;
    }

    /**
     * @param Zeichenkette $zuordnung_bestenliste
     * @throws Runtime_Exception
     */
    public function check_zuordnung_bestenliste(Zeichenkette $zuordnung_bestenliste): void
    {
        if ( !in_array($zuordnung_bestenliste->get_formatted(), self::ZUORDNUNG_BESTENLISTE_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::ZUORDNUNG_BESTENLISTE_ERROR, implode(', ', self::ZUORDNUNG_BESTENLISTE_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichenkette $zuordnung_bestenliste
     * @throws Runtime_Exception
     */
    public function set_zuordnung_bestenliste(Zeichenkette $zuordnung_bestenliste): void
    {
        $this->check_zuordnung_bestenliste($zuordnung_bestenliste);
        $this->zuordnung_bestenliste = $zuordnung_bestenliste;
    }

    /**
     * @return Zeichenkette
     */
    public function get_zuordnung_bestenliste(): Zeichenkette
    {
        return $this->zuordnung_bestenliste;
    }

    /**
     * @param Zahl $qualifikationswettkampfnummer
     * @throws Runtime_Exception
     */
    public function check_qualifikationswettkampfnummer(Zahl $qualifikationswettkampfnummer): void
    {
        $wkart = $this->get_wettkampfart()->get_formatted();
        if ( $wkart === 'Z' or $wkart === 'F' )
        {
            $search_result = $this->get_parent()->search_wettkampf($qualifikationswettkampfnummer, false);
            if ( !($qualifikationswettkampfnummer->is_set() and $search_result !== null) )
            {
                throw new Runtime_Exception(self::QUALIFIKATIONSWETTKAMPFNUMMER_ERROR);
            }
        }
    }

    /**
     * @param Zahl $qualifikationswettkampfnummer
     * @throws Runtime_Exception
     */
    public function set_qualifikationswettkampfnummer(Zahl $qualifikationswettkampfnummer): void
    {
        $this->check_qualifikationswettkampfnummer($qualifikationswettkampfnummer);
        $this->qualifikationswettkampfnummer = $qualifikationswettkampfnummer;
    }

    /**
     * @return Zahl
     */
    public function get_qualifikationswettkampfnummer(): Zahl
    {
        return $this->qualifikationswettkampfnummer;
    }

    /**
     * @param Zeichen $qualifikationswettkampfart
     * @throws Runtime_Exception
     */
    public function check_qualifikationswettkampfart(Zeichen $qualifikationswettkampfart): void
    {
        if ( $this->get_qualifikationswettkampfnummer()->is_set() )
        {
            $search_result = $this->get_parent()->search_wettkampf($this->get_qualifikationswettkampfnummer(), false);
            $correct       = (in_array($qualifikationswettkampfart->get_formatted(), self::WETTKAMPFART_OPTIONS) and
                              $search_result !== null and
                              $qualifikationswettkampfart->get_formatted() === $search_result->get_wettkampfart()->get_formatted());
        }
        else
        {
            $correct = $qualifikationswettkampfart->get_formatted() === '';
        }
        if ( !$correct )
        {
            throw new Runtime_Exception(
                sprintf(
                    self::QUALIFIKATIONSWETTKAMPFART_ERROR,
                    implode(', ', self::WETTKAMPFART_OPTIONS)
                )
            );
        }
    }

    /**
     * @param Zeichen $qualifikationswettkampfart
     * @throws Runtime_Exception
     */
    public function set_qualifikationswettkampfart(Zeichen $qualifikationswettkampfart): void
    {
        $this->check_qualifikationswettkampfart($qualifikationswettkampfart);
        $this->qualifikationswettkampfart = $qualifikationswettkampfart;
    }

    /**
     * @return Zeichen
     */
    public function get_qualifikationswettkampfart(): Zeichen
    {
        return $this->qualifikationswettkampfart;
    }

}