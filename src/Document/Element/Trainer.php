<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Trainer
 * @package Lukaspotthast\DSV\Document\Element
 */
class Trainer extends Document_Element
{

    const TRAINERNUMMER_ERROR = '"Trainernummer" must be set and unique.';
    const NAME_ERROR          = '"Name" must not be empty.';

    /**
     * @var Zahl
     *      Eindeutige numerische Kennung des Trainers zum Zwecke
     *      der Zuordnung bei den Schwimmern.
     *
     *      - REQUIRED -
     */
    private $trainernummer;

    /**
     * @var Zeichenkette
     *      Nachname, Vorname des Trainers, z.B. zur Nennung bei
     *      Siegerehrungen.
     *
     *      - REQUIRED -
     */
    private $name;

    /**
     * Trainer constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->trainernummer = new Zahl();
        $this->name          = new Zeichenkette();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 2;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $trainernummer
     * @throws Runtime_Exception
     */
    public function check_trainernummer(Zahl $trainernummer): void
    {
        $correct = false;
        if ( $trainernummer->is_set() )
        {
            $search_result = $this->get_parent()->search_trainer($trainernummer, false);
            $correct = ($search_result === null or $search_result === $this);
        }
        if ( !$correct )
        {
            throw new Runtime_Exception(self::TRAINERNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $trainernummer
     * @throws Runtime_Exception
     */
    public function set_trainernummer(Zahl $trainernummer): void
    {
        $this->check_trainernummer($trainernummer);
        $this->trainernummer = $trainernummer;
    }

    /**
     * @return Zahl
     */
    public function get_trainernummer(): Zahl
    {
        return $this->trainernummer;
    }

    /**
     * @param Zeichenkette $name
     * @throws Runtime_Exception
     */
    public function check_name(Zeichenkette $name): void
    {
        if ( strlen($name->get_formatted()) === 0 )
        {
            throw new Runtime_Exception(self::NAME_ERROR);
        }
    }

    /**
     * @param Zeichenkette $name
     * @throws Runtime_Exception
     */
    public function set_name(Zeichenkette $name): void
    {
        $this->check_name($name);
        $this->name = $name;
    }

    /**
     * @return Zeichenkette
     */
    public function get_name(): Zeichenkette
    {
        return $this->name;
    }

}