<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichen;
use Lukaspotthast\DSV\Data\Zeit;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class PNZwischenzeit
 * @package Lukaspotthast\DSV\Document\Element
 */
class PNZwischenzeit extends Document_Element
{

    const PERSONENNUMMER_ERROR  = '"Personennummer" was either not set or does not reference an existing "Person".';
    const WETTKAMPFNUMMER_ERROR = '"Wettkampfnummer" was either not set or does not reference an existing "Wettkampf".';
    const WETTKAMPFART_ERROR    = '"Wettkampfart" was not one of: [%s]';
    const DISTANZ_ERROR         = '"Distanz" was not set.';
    const ZWISCHENZEIT_ERROR    = '"Zwischenzeit" was not set.';

    const WETTKAMPFART_OPTIONS = [
        'V', // Vorlauf
        'Z', // Zwischenlauf
        'F', // Finale
        'E', // Entscheidung
        'A', // Ausschwimmen
        'N', // Nachschwimmen
    ];

    /**
     * @var Zahl
     *      Eindeutige numerische Kennung für den Schwimmer
     *      innerhalb dieser Veranstaltung definiert in PERSON.
     *
     *      - REQUIRED -
     */
    private $personennummer;

    /**
     * @var Zahl
     *      Nummer des Wettkampfes.
     *
     *      - REQUIRED -
     */
    private $wettkampfnummer;

    /**
     * @var Zeichen
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      - V = Vorlauf,
     *      - Z = Zwischenlauf,
     *      - F = Finale,
     *      - E = Entscheidung,
     *      - A = Ausschwimmen,
     *      - N = Nachschwimmen
     *
     *      - REQUIRED -
     */
    private $wettkampfart;

    /**
     * @var Zahl
     *      Zurückgelegte Distanz in Metern.
     *
     *      - REQUIRED -
     */
    private $distanz;

    /**
     * @var Zeit
     *      Zwischenzeit.
     *
     *      - REQUIRED -
     */
    private $zwischenzeit;

    /**
     * Ausschreibungimnetz constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->personennummer  = new Zahl();
        $this->wettkampfnummer = new Zahl();
        $this->wettkampfart    = new Zeichen();
        $this->distanz         = new Zahl();
        $this->zwischenzeit    = new Zeit();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 5;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3, 4, 5];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $personennummer
     * @throws Runtime_Exception
     */
    public function check_personennummer(Zahl $personennummer): void
    {
        if ( !($personennummer->is_set() and
               $this->get_parent()->search_person($personennummer) !== null) )
        {
            throw new Runtime_Exception(self::PERSONENNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $personennummer
     * @throws Runtime_Exception
     */
    public function set_personennummer(Zahl $personennummer): void
    {
        $this->check_personennummer($personennummer);
        $this->personennummer = $personennummer;
    }

    /**
     * @return Zahl
     */
    public function get_personennummer(): Zahl
    {
        return $this->personennummer;
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function check_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        if ( !($wettkampfnummer->is_set() and
               $this->get_parent()->search_wettkampf($wettkampfnummer) !== null) )
        {
            throw new Runtime_Exception(self::WETTKAMPFNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function set_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        $this->check_wettkampfnummer($wettkampfnummer);
        $this->wettkampfnummer = $wettkampfnummer;
    }

    /**
     * @return Zahl
     */
    public function get_wettkampfnummer(): Zahl
    {
        return $this->wettkampfnummer;
    }

    /**
     * @param Zeichen $wettkampfart
     * @throws Runtime_Exception
     */
    public function check_wettkampfart(Zeichen $wettkampfart): void
    {
        if ( !in_array($wettkampfart->get_formatted(), self::WETTKAMPFART_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::WETTKAMPFART_ERROR, implode(', ', self::WETTKAMPFART_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $wettkampfart
     * @throws Runtime_Exception
     */
    public function set_wettkampfart(Zeichen $wettkampfart): void
    {
        $this->check_wettkampfart($wettkampfart);
        $this->wettkampfart = $wettkampfart;
    }

    /**
     * @return Zeichen
     */
    public function get_wettkampfart(): Zeichen
    {
        return $this->wettkampfart;
    }

    /**
     * @param Zahl $distanz
     * @throws Runtime_Exception
     */
    public function check_distanz(Zahl $distanz): void
    {
        if ( !$distanz->is_set() )
        {
            throw new Runtime_Exception(self::DISTANZ_ERROR);
        }
    }

    /**
     * @param Zahl $distanz
     * @throws Runtime_Exception
     */
    public function set_distanz(Zahl $distanz): void
    {
        $this->check_distanz($distanz);
        $this->distanz = $distanz;
    }

    /**
     * @return Zahl
     */
    public function get_distanz(): Zahl
    {
        return $this->distanz;
    }

    /**
     * @param Zeit $zwischenzeit
     * @throws Runtime_Exception
     */
    public function check_zwischenzeit(Zeit $zwischenzeit): void
    {
        if ( !$zwischenzeit->is_set() )
        {
            throw new Runtime_Exception(self::ZWISCHENZEIT_ERROR);
        }
    }

    /**
     * @param Zeit $zwischenzeit
     * @throws Runtime_Exception
     */
    public function set_zwischenzeit(Zeit $zwischenzeit): void
    {
        $this->check_zwischenzeit($zwischenzeit);
        $this->zwischenzeit = $zwischenzeit;
    }

    /**
     * @return Zeit
     */
    public function get_zwischenzeit(): Zeit
    {
        return $this->zwischenzeit;
    }

}