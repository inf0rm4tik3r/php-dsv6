<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeit;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class StartST
 * @package Lukaspotthast\DSV\Document\Element
 */
class StartST extends Document_Element
{

    const STAFFELNUMMER_ERROR   = '"Staffelnummer" was either not set or did not specify an existing "STMeldung".';
    const WETTKAMPFNUMMER_ERROR = '"Wettkampfnummer" was either not set or did not specify an existing "Wettkampf".';
    const MELDEZEIT_ERROR       = '"Meldezeit" was not set.';

    /**
     * @var Zahl
     *      Eindeutige numerische Kennung für die Staffel innerhalb
     *      dieser Veranstaltung definiert in STMELDUNG.
     *
     *      - REQUIRED -
     */
    private $staffelnummer;

    /**
     * @var Zahl
     *      Nummer des Wettkampfes.
     *
     *      - REQUIRED -
     */
    private $wettkampfnummer;

    /**
     * @var Zeit
     *      Meldezeit der Staffel. Der Unterlassungswert ist "00:00:00,00".
     *
     *      - REQUIRED -
     */
    private $meldezeit;

    /**
     * Ausschreibungimnetz constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->staffelnummer   = new Zahl();
        $this->wettkampfnummer = new Zahl();
        $this->meldezeit       = new Zeit("00:00:00,00");

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 3;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $staffelnummer
     * @throws Runtime_Exception
     */
    public function check_staffelnummer(Zahl $staffelnummer): void
    {
        if ( !($staffelnummer->is_set() and
               $this->get_parent()->search_stmeldung($staffelnummer, false) !== null) )
        {
            throw new Runtime_Exception(self::STAFFELNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $staffelnummer
     * @throws Runtime_Exception
     */
    public function set_staffelnummer(Zahl $staffelnummer): void
    {
        $this->check_staffelnummer($staffelnummer);
        $this->staffelnummer = $staffelnummer;
    }

    /**
     * @return Zahl
     */
    public function get_staffelnummer(): Zahl
    {
        return $this->staffelnummer;
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function check_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        if ( !($wettkampfnummer->is_set() and
               $this->get_parent()->search_wettkampf($wettkampfnummer, false) !== null) )
        {
            throw new Runtime_Exception(self::WETTKAMPFNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function set_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        $this->check_wettkampfnummer($wettkampfnummer);
        $this->wettkampfnummer = $wettkampfnummer;
    }

    /**
     * @return Zahl
     */
    public function get_wettkampfnummer(): Zahl
    {
        return $this->wettkampfnummer;
    }

    /**
     * @param Zeit $meldezeit
     * @throws Runtime_Exception
     */
    public function check_meldezeit(Zeit $meldezeit): void
    {
        if ( !$meldezeit->is_set() )
        {
            throw new Runtime_Exception(self::MELDEZEIT_ERROR);
        }
    }

    /**
     * @param Zeit $meldezeit
     */
    public function set_meldezeit(Zeit $meldezeit): void
    {
        if ( $meldezeit->is_set() )
        {
            $this->meldezeit = $meldezeit;
        }
    }

    /**
     * @return Zeit
     */
    public function get_meldezeit(): Zeit
    {
        return $this->meldezeit;
    }

}