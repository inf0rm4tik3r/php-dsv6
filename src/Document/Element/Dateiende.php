<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Writer\Writer;

/**
 * Class Dateiende
 * @package Lukaspotthast\DSV\Document\Element
 */
class Dateiende extends Document_Element
{

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 0;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return [];
    }

    /**
     * @param Writer $writer
     * @param array  $options
     * @return string
     */
    public function write(Writer $writer, array $options = []): string
    {
        $options['include_element_name_completion'] = false;
        $options['include_argument_list']           = false;
        return parent::write($writer, $options);
    }

}