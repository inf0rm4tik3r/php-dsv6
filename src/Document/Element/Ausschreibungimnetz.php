<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;

/**
 * Class Ausschreibungimnetz
 * @package Lukaspotthast\DSV\Document\Element
 */
class Ausschreibungimnetz extends Document_Element
{

    /**
     * @var Zeichenkette
     *      Gültige Internetadresse unter der die Ausschreibung zu finden ist.
     */
    private $internetadresse;

    /**
     * Ausschreibungimnetz constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->internetadresse = new Zeichenkette();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 1;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zeichenkette $internetadresse
     */
    public function set_internetadresse(Zeichenkette $internetadresse): void
    {
        $this->internetadresse = $internetadresse;
    }

    /**
     * @return Zeichenkette
     */
    public function get_internetadresse(): Zeichenkette
    {
        return $this->internetadresse;
    }

}