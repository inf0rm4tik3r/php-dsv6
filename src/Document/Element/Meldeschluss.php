<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Datum;
use Lukaspotthast\DSV\Data\Uhrzeit;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Meldeschluss
 * @package Lukaspotthast\DSV\Document\Element
 */
class Meldeschluss extends Document_Element
{

    const DATUM_ERROR   = '"Datum" must be set.';
    const UHRZEIT_ERROR = '"Uhrzeit" must be set.';

    /**
     * @var Datum
     *      Datum Meldeschluss.
     *
     *      - REQUIRED -
     */
    private $datum;

    /**
     * @var Uhrzeit
     *      Uhrzeit Meldeschluss.
     *
     *      - REQUIRED -
     */
    private $uhrzeit;

    /**
     * Meldeschluss constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->datum   = new Datum();
        $this->uhrzeit = new Uhrzeit();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 2;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Datum $datum
     * @throws Runtime_Exception
     */
    public function check_datum(Datum $datum): void
    {
        if ( !$datum->is_set() )
        {
            throw new Runtime_Exception(self::DATUM_ERROR);
        }
    }

    /**
     * @param Datum $datum
     * @throws Runtime_Exception
     */
    public function set_datum(Datum $datum): void
    {
        $this->check_datum($datum);
        $this->datum = $datum;
    }

    /**
     * @return Datum
     */
    public function get_datum(): Datum
    {
        return $this->datum;
    }

    /**
     * @param Uhrzeit $uhrzeit
     * @throws Runtime_Exception
     */
    public function check_uhrzeit(Uhrzeit $uhrzeit): void
    {
        if ( !$uhrzeit->is_set() )
        {
            throw new Runtime_Exception(self::UHRZEIT_ERROR);
        }
    }

    /**
     * @param Uhrzeit $uhrzeit
     * @throws Runtime_Exception
     */
    public function set_uhrzeit(Uhrzeit $uhrzeit): void
    {
        $this->check_uhrzeit($uhrzeit);
        $this->uhrzeit = $uhrzeit;
    }

    /**
     * @return Uhrzeit
     */
    public function get_uhrzeit(): Uhrzeit
    {
        return $this->uhrzeit;
    }

}