<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Besonderes
 * @package Lukaspotthast\DSV\Document\Element
 */
class Besonderes extends Document_Element
{

    const ANMERKUNGEN_ERROR = '"Anmerkungen" must be set.';

    /**
     * @var Zeichenkette
     *      Für Bestimmungen, die nicht durch die restlichen Regeln definiert
     *      wurden. Z.B.: „Achtung! Für besondere Wertungen und
     *      Meldegelder Ausschreibung im Internet beachten!“
     *
     *      - REQUIRED -
     */
    private $anmerkungen;

    /**
     * Besonderes constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->anmerkungen = new Zeichenkette();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 1;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zeichenkette $anmerkungen
     * @throws Runtime_Exception
     */
    public function check_anmerkungen(Zeichenkette $anmerkungen): void
    {
        if ( !strlen($anmerkungen->get_formatted()) > 0 )
        {
            throw new Runtime_Exception(self::ANMERKUNGEN_ERROR);
        }
    }

    /**
     * @param Zeichenkette $anmerkungen
     * @throws Runtime_Exception
     */
    public function set_anmerkungen(Zeichenkette $anmerkungen): void
    {
        $this->check_anmerkungen($anmerkungen);
        $this->anmerkungen = $anmerkungen;
    }

    /**
     * @return Zeichenkette
     */
    public function get_anmerkungen(): Zeichenkette
    {
        return $this->anmerkungen;
    }

}