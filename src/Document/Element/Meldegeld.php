<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Geldbetrag;
use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Meldegeld
 * @package Lukaspotthast\DSV\Document\Element
 */
class Meldegeld extends Document_Element
{

    const MELDEGELD_TYP_INDEX   = 0;
    const BETRAG_INDEX          = 1;
    const WETTKAMPFNUMMER_INDEX = 2;

    const MELDEGELD_TYP_ERROR   = '"Meldegeld Typ" was not one of: [%s]';
    const BETRAG_ERROR          = '"Betrag" must be set.';
    const WETTKAMPFNUMMER_ERROR = '"Wettkampfnummer" must only be specified if "Meldegeld Typ" is of "Wkmeldegeld". ' .
                                  'It was not set or did not referenced an existing "Wettkampf".';

    const MELDEGELD_TYP_OPTIONS = [
        'Meldegeldpauschale',
        'Einzelmeldegeld',
        'Staffelmeldegeld',
        'Wkmeldegeld',
        'Mannschaftmeldegeld',
    ];

    /**
     * @var Zeichenkette
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      - Meldegeldpauschale,
     *      - Einzelmeldegeld,
     *      - Staffelmeldegeld,
     *      - Wkmeldegeld,
     *      - Mannschaftmeldegeld
     *
     *      - REQUIRED -
     */
    private $meldegeld_typ;

    /**
     * @var Geldbetrag
     *      Meldegeld Betrag.
     *
     *      - REQUIRED -
     */
    private $betrag;

    /**
     * @var Zahl
     *      Nummer des Wettkampfes, Pflichtfeld bei Meldegeld Typ "Wkmeldegeld".
     */
    private $wettkampfnummer;

    /**
     * Meldegeld constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->meldegeld_typ   = new Zeichenkette();
        $this->betrag          = new Geldbetrag();
        $this->wettkampfnummer = new Zahl();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 3;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zeichenkette $meldegeld_typ
     * @throws Runtime_Exception
     */
    public function check_meldegeld_typ(Zeichenkette $meldegeld_typ): void
    {
        if ( !in_array($meldegeld_typ->get_formatted(), self::MELDEGELD_TYP_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::MELDEGELD_TYP_ERROR, implode(', ', self::MELDEGELD_TYP_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichenkette $meldegeld_typ
     * @throws Runtime_Exception
     */
    public function set_meldegeld_typ(Zeichenkette $meldegeld_typ): void
    {
        $this->check_meldegeld_typ($meldegeld_typ);
        $this->meldegeld_typ = $meldegeld_typ;
    }

    /**
     * @return Zeichenkette
     */
    public function get_meldegeld_typ(): Zeichenkette
    {
        return $this->meldegeld_typ;
    }

    /**
     * @param Geldbetrag $betrag
     * @throws Runtime_Exception
     */
    public function check_betrag(Geldbetrag $betrag): void
    {
        if ( !$betrag->is_set() )
        {
            throw new Runtime_Exception(self::BETRAG_ERROR);
        }
    }

    /**
     * @param Geldbetrag $betrag
     * @throws Runtime_Exception
     */
    public function set_betrag(Geldbetrag $betrag): void
    {
        $this->check_betrag($betrag);
        $this->betrag = $betrag;
    }

    /**
     * @return Geldbetrag
     */
    public function get_betrag(): Geldbetrag
    {
        return $this->betrag;
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function check_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        $correct = false;
        if ( $this->get_meldegeld_typ()->get_formatted() === 'Wkmeldegeld' )
        {
            // The "Wettkampfnummer" must be set if "Meldegeld Typ" is of "Wkmeldegeld".
            if ( $wettkampfnummer->is_set() )
            {
                $search_result = $this->get_parent()->search_wettkampf($wettkampfnummer);
                // The referenced "Wettkampf" must exist.
                $correct = $search_result !== null;
            }
        }
        else
        {
            // Must be empty if "Meldegeld Typ" is not of "Wkmeldegeld".
            $correct = $wettkampfnummer->get_formatted() === '';
        }

        if ( !$correct )
        {
            throw new Runtime_Exception(self::WETTKAMPFNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function set_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        $this->check_wettkampfnummer($wettkampfnummer);
        $this->wettkampfnummer = $wettkampfnummer;
    }

    /**
     * @return Zahl
     */
    public function get_wettkampfnummer(): Zahl
    {
        return $this->wettkampfnummer;
    }

}