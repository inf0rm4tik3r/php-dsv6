<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Karimeldung
 * @package Lukaspotthast\DSV\Document\Element
 */
class Karimeldung extends Document_Element
{

    const KAMPFRICHTERNUMMER_ERROR = '"Kampfrichter Nummer" must be set and unique.';
    const NAME_ERROR               = '"Name" must not be empty.';
    const KAMPFRICHTERGRUPPE_ERROR = '"Kampfrichtergruppe" was not one of: [%s]';

    const KAMPFRICHTERGRUPPE_OPTIONS = [
        'WKR', // Wettkampfrichter
        'AUS', // Auswerter
        'SCH', // Schiedsrichter
        'SPR', // Sprecher
    ];

    /**
     * @var Zahl
     *      Eindeutige numerische Kennung des Kampfrichters zum
     *      Zwecke der Zuordnung der Abschnitte.
     *
     *      - REQUIRED -
     */
    private $kampfrichternummer;

    /**
     * @var Zeichenkette
     *      Nachname, Vorname des Kampfrichters.
     *
     *      - REQUIRED -
     */
    private $name;

    /**
     * @var Zeichenkette
     *      Es steht folgende Auswahl zur Verfügung:
     *      WKR = Wettkampfrichter,
     *      AUS = Auswerter,
     *      SCH = Schiedsrichter,
     *      SPR = Sprecher
     *
     *      - REQUIRED -
     */
    private $kampfrichtergruppe;

    /**
     * Karimeldung constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->kampfrichternummer = new Zahl();
        $this->name               = new Zeichenkette();
        $this->kampfrichtergruppe = new Zeichenkette();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 3;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $kampfrichternummer
     * @throws Runtime_Exception
     */
    public function check_kampfrichternummer(Zahl $kampfrichternummer): void
    {
        $correct = false;
        // "Kampfrichternummer" must be set.
        if ( $kampfrichternummer->is_set() )
        {
            $search_result = $this->get_parent()->search_karimeldung($kampfrichternummer, false);
            // There must not be another "Karimeldung" with $kampfrichternummer.
            $correct = ($search_result === null or $search_result === $this);
        }
        if ( !$correct )
        {
            throw new Runtime_Exception(self::KAMPFRICHTERNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $kampfrichternummer
     * @throws Runtime_Exception
     */
    public function set_kampfrichternummer(Zahl $kampfrichternummer): void
    {
        $this->check_kampfrichternummer($kampfrichternummer);
        $this->kampfrichternummer = $kampfrichternummer;
    }

    /**
     * @return Zahl
     */
    public function get_kampfrichternummer(): Zahl
    {
        return $this->kampfrichternummer;
    }

    /**
     * @param Zeichenkette $name
     * @throws Runtime_Exception
     */
    public function check_name(Zeichenkette $name): void
    {
        if ( strlen($name->get_formatted()) === 0 )
        {
            throw new Runtime_Exception(self::NAME_ERROR);
        }
    }

    /**
     * @param Zeichenkette $name
     * @throws Runtime_Exception
     */
    public function set_name(Zeichenkette $name): void
    {
        $this->check_name($name);
        $this->name = $name;
    }

    /**
     * @return Zeichenkette
     */
    public function get_name(): Zeichenkette
    {
        return $this->name;
    }

    /**
     * @param Zeichenkette $kampfrichtergruppe
     * @throws Runtime_Exception
     */
    public function check_kampfrichtergruppe(Zeichenkette $kampfrichtergruppe): void
    {
        if ( !in_array($kampfrichtergruppe->get_formatted(), self::KAMPFRICHTERGRUPPE_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::KAMPFRICHTERGRUPPE_ERROR, implode(', ', self::KAMPFRICHTERGRUPPE_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichenkette $kampfrichtergruppe
     * @throws Runtime_Exception
     */
    public function set_kampfrichtergruppe(Zeichenkette $kampfrichtergruppe): void
    {
        $this->check_kampfrichtergruppe($kampfrichtergruppe);
        $this->kampfrichtergruppe = $kampfrichtergruppe;
    }

    /**
     * @return Zeichenkette
     */
    public function get_kampfrichtergruppe(): Zeichenkette
    {
        return $this->kampfrichtergruppe;
    }

}