<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\IOC_Codes;
use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Verein
 * @package Lukaspotthast\DSV\Document\Element
 */
class Verein extends Document_Element
{

    const VEREINSBEZEICHNUNG_ERROR   = '"Vereinsbezeichnung" must not be empty.';
    const VEREINSKENNZAHL_ERROR      = '"Vereinskennzahl" must be of four digits or 0.';
    const LANDESSCHWIMMVERBAND_ERROR = '"Landesschwimmverband" must lie between 1 and 18 or be 0 or 99.';
    const FINA_NATIONENKUERZEL_ERROR = '"Fina Nationenkuerzel" must be specified and of length 3.';

    /**
     * @var Zeichenkette
     *      Name des Vereins
     *
     *      - REQUIRED -
     */
    private $vereinsbezeichnung;

    /**
     * @var Zahl
     *      4-stellige durch den DSV für jeden Verein festgelegte
     *      Zahl. Bei nicht dem DSV angehörenden Vereinen ist 0
     *      anzugeben.
     *
     *      - REQUIRED -
     */
    private $vereinskennzahl;

    /**
     * @var Zahl
     *      Die 18 Landesschwimmverbände im DSV sind mit einer
     *      numerischen Kennung von 1 bis 18 versehen. Siehe
     *      Anhang. Bei ausländischen Vereinen ist 0 anzugeben.
     *      Auswahlmannschaften mit Schwimmern von
     *      inländischen Vereinen, z.B. Mannschaften des DSV
     *      oder der Ländergruppen, sind mit 99 anzugeben.
     *
     *      - REQUIRED -
     */
    private $landesschwimmverband;

    /**
     * @var Zeichenkette
     *      Hier müssen die im FINA-Handbook festgelegten 3-stelligen
     *      Nationenkürzel angegeben werden, z.B. GER für Deutschland.
     *
     *      - REQUIRED -
     */
    private $fina_nationenkuerzel;

    /**
     * Verein constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->vereinsbezeichnung   = new Zeichenkette();
        $this->vereinskennzahl      = new Zahl();
        $this->landesschwimmverband = new Zahl();
        $this->fina_nationenkuerzel = new Zeichenkette();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 4;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3, 4];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zeichenkette $vereinsbezeichnung
     * @throws Runtime_Exception
     */
    public function check_vereinsbezeichnung(Zeichenkette $vereinsbezeichnung): void
    {
        if ( strlen($vereinsbezeichnung->get_formatted()) === 0 )
        {
            throw new Runtime_Exception(self::VEREINSBEZEICHNUNG_ERROR);
        }
    }

    /**
     * @param Zeichenkette $vereinsbezeichnung
     * @throws Runtime_Exception
     */
    public function set_vereinsbezeichnung(Zeichenkette $vereinsbezeichnung): void
    {
        $this->check_vereinsbezeichnung($vereinsbezeichnung);
        $this->vereinsbezeichnung = $vereinsbezeichnung;
    }

    /**
     * @return Zeichenkette
     */
    public function get_vereinsbezeichnung(): Zeichenkette
    {
        return $this->vereinsbezeichnung;
    }

    /**
     * @param Zahl $vereinskennzahl
     * @throws Runtime_Exception
     */
    public function check_vereinskennzahl(Zahl $vereinskennzahl): void
    {
        $vkz = $vereinskennzahl->get_zahl();

        if ( !($vereinskennzahl->is_set() and
               $vkz === 0 or
               ($vkz > 0 and strlen(strval($vkz)) === 4)) )
        {
            throw new Runtime_Exception(self::VEREINSKENNZAHL_ERROR);
        }
    }

    /**
     * @param Zahl $vereinskennzahl
     * @throws Runtime_Exception
     */
    public function set_vereinskennzahl(Zahl $vereinskennzahl): void
    {
        $this->check_vereinskennzahl($vereinskennzahl);
        $this->vereinskennzahl = $vereinskennzahl;
    }

    /**
     * @return Zahl
     */
    public function get_vereinskennzahl(): Zahl
    {
        return $this->vereinskennzahl;
    }

    /**
     * @param Zahl $landesschwimmverband
     * @throws Runtime_Exception
     */
    public function check_landesschwimmverband(Zahl $landesschwimmverband): void
    {
        $lsv = $landesschwimmverband->get_zahl();

        if ( !($landesschwimmverband->is_set() and
               $lsv === 0 or $lsv === 99 or
               ($lsv >= 1 and $lsv <= 18)) )
        {
            throw new Runtime_Exception(self::LANDESSCHWIMMVERBAND_ERROR);
        }
    }

    /**
     * @param Zahl $landesschwimmverband
     * @throws Runtime_Exception
     */
    public function set_landesschwimmverband(Zahl $landesschwimmverband): void
    {
        $this->check_landesschwimmverband($landesschwimmverband);
        $this->landesschwimmverband = $landesschwimmverband;
    }

    /**
     * @return Zahl
     */
    public function get_landesschwimmverband(): Zahl
    {
        return $this->landesschwimmverband;
    }

    /**
     * @param Zeichenkette $fina_nationenkuerzel
     * @throws Runtime_Exception
     */
    public function check_fina_nationenkuerzel(Zeichenkette $fina_nationenkuerzel): void
    {
        if ( !IOC_Codes::is_ioc_code($fina_nationenkuerzel->get_formatted()) )
        {
            throw new Runtime_Exception(self::FINA_NATIONENKUERZEL_ERROR);
        }
    }

    /**
     * @param Zeichenkette $fina_nationenkuerzel
     * @throws Runtime_Exception
     */
    public function set_fina_nationenkuerzel(Zeichenkette $fina_nationenkuerzel): void
    {
        $this->check_fina_nationenkuerzel($fina_nationenkuerzel);
        $this->fina_nationenkuerzel = $fina_nationenkuerzel;
    }

    /**
     * @return Zeichenkette
     */
    public function get_fina_nationenkuerzel(): Zeichenkette
    {
        return $this->fina_nationenkuerzel;
    }

}