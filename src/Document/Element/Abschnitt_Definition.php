<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Datum;
use Lukaspotthast\DSV\Data\Uhrzeit;
use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Abschnitt_Definition
 * @package Lukaspotthast\DSV\Document\Element
 */
class Abschnitt_Definition extends Document_Element
{

    const ABSCHNITTSNUMMER_ERROR = '"Abschnittsnummer" was either not set, exceeded two digits, ' .
                                   'or was already used by another "Abschnitt".';
    const ABSCHNITTSDATUM_ERROR  = '"Abschnittsdatum" must be set.';
    const ANFANGSZEIT_ERROR      = '"Anfangszeit" must be set.';
    const RELATIVE_ANGABE_ERROR  = '"Relative Angabe" must be \'J\' or \'N\'.';

    const RELATIVE_ANGABE_OPTIONS = ['J', 'N'];

    /**
     * @var Zahl
     *      Nummer des Abschnitts, maximal zweistellig. Muss eindeutig sein.
     *
     *      - REQUIRED -
     */
    private $abschnittsnummer;

    /**
     * @var Datum
     *      Datum des Abschnitts.
     *
     *      - REQUIRED -
     */
    private $abschnittsdatum;

    /**
     * @var Uhrzeit
     *      Zeitpunkt Einlass.
     */
    private $einlass;

    /**
     * @var Uhrzeit
     *      Anfangszeit der Kampfrichtersitzung.
     */
    private $kampfrichtersitzung;

    /**
     * @var Uhrzeit
     *      Anfangszeit des Abschnitts.
     *
     *      - REQUIRED -
     */
    private $anfangszeit;

    /**
     * @var Zeichenkette
     *      Zulässige Zeichen: J und N.
     *      N = Einlass, Kampfrichtersitzung und Anfangszeit sind echte Uhrzeiten.
     *      J = Einlass, Kampfrichtersitzung und Anfangszeit sind relative
     *          Angaben Stunden:Minuten nach dem vorherigen Abschnitt.
     *      Unterlassungswert ist N.
     *
     *      - NEEDS CHECK -
     */
    private $relative_angabe;

    /**
     * Abschnitt constructor.
     *
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->abschnittsnummer    = new Zahl();
        $this->abschnittsdatum     = new Datum();
        $this->einlass             = new Uhrzeit();
        $this->kampfrichtersitzung = new Uhrzeit();
        $this->anfangszeit         = new Uhrzeit();
        $this->relative_angabe     = new Zeichenkette('N', false);

        parent::__construct($parent, $stmt);
    }

    /**
     * @return string
     */
    public static function override_get_element_name(): string
    {
        return 'ABSCHNITT';
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 6;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 5];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $abschnittsnummer
     * @throws Runtime_Exception
     */
    public function check_abschnittsnummer(Zahl $abschnittsnummer): void
    {
        $correct = false;
        if ( $abschnittsnummer->is_set() )
        {
            $search_result = $this->get_parent()->search_abschnitt($abschnittsnummer, false);
            $length        = strlen($abschnittsnummer->get_formatted());
            $correct       = ($length <= 2 and ($search_result === null or $search_result === $this));
        }
        if ( !$correct )
        {
            throw new Runtime_Exception(self::ABSCHNITTSNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $abschnittsnummer
     * @throws Runtime_Exception
     */
    public function set_abschnittsnummer(Zahl $abschnittsnummer): void
    {
        $this->check_abschnittsnummer($abschnittsnummer);
        $this->abschnittsnummer = $abschnittsnummer;
    }

    /**
     * @return Zahl
     */
    public function get_abschnittsnummer(): Zahl
    {
        return $this->abschnittsnummer;
    }

    /**
     * @param Datum $abschnittsdatum
     * @throws Runtime_Exception
     */
    public function check_abschnittsdatum(Datum $abschnittsdatum): void
    {
        if ( !$abschnittsdatum->is_set() )
        {
            throw new Runtime_Exception(self::ABSCHNITTSDATUM_ERROR);
        }
    }

    /**
     * @param Datum $abschnittsdatum
     * @throws Runtime_Exception
     */
    public function set_abschnittsdatum(Datum $abschnittsdatum): void
    {
        $this->check_abschnittsdatum($abschnittsdatum);
        $this->abschnittsdatum = $abschnittsdatum;
    }

    /**
     * @return Datum
     */
    public function get_abschnittsdatum(): Datum
    {
        return $this->abschnittsdatum;
    }

    /**
     * @param Uhrzeit $einlass
     */
    public function set_einlass(Uhrzeit $einlass): void
    {
        $this->einlass = $einlass;
    }

    /**
     * @return Uhrzeit
     */
    public function get_einlass(): Uhrzeit
    {
        return $this->einlass;
    }

    /**
     * @param Uhrzeit $kampfrichtersitzung
     */
    public function set_kampfrichtersitzung(Uhrzeit $kampfrichtersitzung): void
    {
        $this->kampfrichtersitzung = $kampfrichtersitzung;
    }

    /**
     * @return Uhrzeit
     */
    public function get_kampfrichtersitzung(): Uhrzeit
    {
        return $this->kampfrichtersitzung;
    }

    /**
     * @param Uhrzeit $anfangszeit
     * @throws Runtime_Exception
     */
    public function check_anfangszeit(Uhrzeit $anfangszeit): void
    {
        if ( !$anfangszeit->is_set() )
        {
            throw new Runtime_Exception(self::ANFANGSZEIT_ERROR);
        }
    }

    /**
     * @param Uhrzeit $anfangszeit
     * @throws Runtime_Exception
     */
    public function set_anfangszeit(Uhrzeit $anfangszeit): void
    {
        $this->check_anfangszeit($anfangszeit);
        $this->anfangszeit = $anfangszeit;
    }

    /**
     * @return Uhrzeit
     */
    public function get_anfangszeit(): Uhrzeit
    {
        return $this->anfangszeit;
    }

    /**
     * @param Zeichenkette $relative_angabe
     * @throws Runtime_Exception
     */
    public function check_relative_angabe(Zeichenkette $relative_angabe): void
    {
        if ( $relative_angabe->get_formatted() !== '' and
             !in_array($relative_angabe->get_formatted(), self::RELATIVE_ANGABE_OPTIONS) )
        {
            throw new Runtime_Exception(self::RELATIVE_ANGABE_ERROR);
        }
    }

    /**
     * @param Zeichenkette $relative_angabe
     * @throws Runtime_Exception
     */
    public function set_relative_angabe(Zeichenkette $relative_angabe): void
    {
        $this->check_relative_angabe($relative_angabe);
        $this->relative_angabe = $relative_angabe;
    }

    /**
     * @return Zeichenkette
     */
    public function get_relative_angabe(): Zeichenkette
    {
        return $this->relative_angabe;
    }

}