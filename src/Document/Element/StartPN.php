<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeit;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class StartPN
 * @package Lukaspotthast\DSV\Document\Element
 */
class StartPN extends Document_Element
{

    const PERSONENNUMMER_ERROR  = '"Personennummer" was either not set or did not specify an existing "PNMeldung".';
    const WETTKAMPFNUMMER_ERROR = '"Wettkampfnummer" was either not set or did not specify an existing "Wettkampf".';
    const MELDEZEIT_ERROR       = '"Meldezeit" was not set.';

    /**
     * @var Zahl
     *      Eindeutige numerische Kennung für den Schwimmer
     *      innerhalb dieser Veranstaltung definiert in PNMELDUNG.
     *
     *      - REQUIRED -
     */
    private $personennummer;

    /**
     * @var Zahl
     *      Nummer des Wettkampfes.
     *
     *      - REQUIRED -
     */
    private $wettkampfnummer;

    /**
     * @var Zeit
     *      Meldezeit des Schwimmers. Der Unterlassungswert ist "00:00:00,00".
     *
     *      - REQUIRED -
     */
    private $meldezeit;

    /**
     * Ausschreibungimnetz constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->personennummer  = new Zahl();
        $this->wettkampfnummer = new Zahl();
        $this->meldezeit       = new Zeit("00:00:00,00");

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 3;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $personennummer
     * @throws Runtime_Exception
     */
    public function check_personennummer(Zahl $personennummer): void
    {
        if ( !($personennummer->is_set() and
               $this->get_parent()->search_pnmeldung($personennummer, false) !== null) )
        {
            throw new Runtime_Exception(self::PERSONENNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $personennummer
     * @throws Runtime_Exception
     */
    public function set_personennummer(Zahl $personennummer): void
    {
        $this->check_personennummer($personennummer);
        $this->personennummer = $personennummer;
    }

    /**
     * @return Zahl
     */
    public function get_personennummer(): Zahl
    {
        return $this->personennummer;
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function check_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        if ( !($wettkampfnummer->is_set() and
               $this->get_parent()->search_wettkampf($wettkampfnummer, false) !== null) )
        {
            throw new Runtime_Exception(self::WETTKAMPFNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function set_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        $this->check_wettkampfnummer($wettkampfnummer);
        $this->wettkampfnummer = $wettkampfnummer;
    }

    /**
     * @return Zahl
     */
    public function get_wettkampfnummer(): Zahl
    {
        return $this->wettkampfnummer;
    }

    /**
     * @param Zeit $meldezeit
     * @throws Runtime_Exception
     */
    public function check_meldezeit(Zeit $meldezeit): void
    {
        if ( !$meldezeit->is_set() )
        {
            throw new Runtime_Exception(self::MELDEZEIT_ERROR);
        }
    }

    /**
     * @param Zeit $meldezeit
     */
    public function set_meldezeit(Zeit $meldezeit): void
    {
        if ( $meldezeit->is_set() )
        {
            $this->meldezeit = $meldezeit;
        }
    }

    /**
     * @return Zeit
     */
    public function get_meldezeit(): Zeit
    {
        return $this->meldezeit;
    }

}