<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichen;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Data\Zeit;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/*
 * Beinhaltet "Person" und Anfang von "Verein"
 */

/**
 * Class PNErgebnis
 * @package Lukaspotthast\DSV\Document\Element
 */
class PNErgebnis extends Document_Element
{

    const WETTKAMPFNUMMER_ERROR    = '"Wettkampfnummer" was either not set or did not specify an existing "Wettkampf".';
    const WETTKAMPFART_ERROR       = '"Wettkampfart" was not one of: [%s]';
    const WERTUNGSNUMMER_ERROR     = '"Wertungsnummer" was either not set or a "Personenergebnis" with that number already existed.';
    const PLATZ_ERROR              = '"Platz" was not specified.';
    const NICHTWERTUNGSGRUND_ERROR = '"Nichtwertungsgrund" was not specified, but it must if "Platz" is set to 0.';
    const NAME_ERROR               = '"Name" was not set.';
    const DSV_ID_ERROR             = '"DSV ID" was neither empty nor a number of 6 digits.';
    const PERSONENNUMMER_ERROR     = '"Personennummer" was not set or was not unique.';
    const GESCHLECHT_ERROR         = '"Geschlecht" was not one of: [%s]';
    const JAHRGANG_ERROR           = '"Jahrgang" was not a number of 4 digits.';
    const VEREINSBEZEICHNUNG_ERROR = '"Vereinsbezeichnung" must not be empty.';
    const VEREINSKENNZAHL_ERROR    = '"Vereinskennzahl" must be of four digits or 0.';
    const ENDZEIT_ERROR            = '"Endzeit" was not specified. Use "00:00:00,00" as a default if necessary.';
    const ENM_ERROR                = '"Erhöhtes nachträgliches Meldegeld (ENM)" was neither blank nor one of: [%s]';

    const WETTKAMPFART_OPTIONS = [
        'V', // Vorlauf
        'Z', // Zwischenlauf
        'F', // Finale
        'E', // Entscheidung
        'A', // Ausschwimmen
        'N', // Nachschwimmen
    ];

    const NICHTWERTUNGSGRUND_OPTIONS = [
        'DS', // Disqualifikation
        'NA', // nicht am Start/nicht angetreten
        'AB', // vom Wettkampf abgemeldet
        'AU', // Aufgegeben
        'ZU', // Zeitüberschreitung (nur für Langstreckenwettkämpfe)
    ];

    const GESCHLECHT_OPTIONS = [
        'M', // männlich
        'W', // weiblich
    ];

    const ENM_OPTIONS = [
        'E', // Norm erreicht
        'F', // ENM fällig
        'N', // Norm nicht erreicht, aber nachgewiesen
    ];


    /**
     * @var Zahl
     *      Nummer des Wettkampfes.
     *
     *      - REQUIRED -
     */
    private $wettkampfnummer;

    /**
     * @var Zeichen
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      - V = Vorlauf,
     *      - Z = Zwischenlauf,
     *      - F = Finale,
     *      - E = Entscheidung,
     *      - A = Ausschwimmen,
     *      - N = Nachschwimmen
     *
     *      - REQUIRED -
     */
    private $wettkampfart;

    /**
     * @var Zahl
     *      Eindeutige, in der gesamten Veranstaltung nur einmal
     *      vergebene Nummer für diese Wertung.
     *
     *      - REQUIRED -
     */
    private $wertungsnummer;

    /**
     * @var Zahl
     *      Erzielte Platzierung. Bei Füllung des Feldes "Grund
     *      der Nichtwertung" muss hier als Platz "0" angegeben
     *      werden.
     *
     *      - REQUIRED -
     */
    private $platz;

    /**
     * @var Zeichenkette
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      - DS = Disqualifikation,
     *      - NA = nicht am Start/nicht angetreten,
     *      - AB = vom Wettkampf abgemeldet,
     *      - AU = Aufgegeben,
     *      - ZU = Zeitüberschreitung (nur für Langstreckenwettkämpfe)
     *      Muss angegeben werden, wenn Platz auf 0 gesetzt ist.
     *
     *      - NEEDS CHECK -
     */
    private $nichtwertungsgrund;

    // PERSON

    /**
     * @var Zeichenkette
     *      Nachname, Vorname des Teilnehmers.
     *
     *      - REQUIRED -
     */
    private $name;

    /**
     * @var Zahl
     *      6-stellige durch den DSV für jeden Schwimmer festgelegte
     *      Zahl. Ist keine DSV-ID des Schwimmers bekannt, muss 0
     *      angegeben werden.
     *
     *      - REQUIRED -
     */
    private $dsv_id;

    /**
     * @var Zahl
     *      Eindeutige numerische Kennung für den Schwimmer
     *      innerhalb dieser Veranstaltung zum Zwecke der Zuordnung
     *      der Wettkämpfe.
     *
     *      - REQUIRED -
     */
    private $personennummer;

    /**
     * @var Zeichen
     *      Zulässige Werte:
     *      M = männlich
     *      W = weiblich
     *
     *      - REQUIRED -
     */
    private $geschlecht;

    /**
     * @var Zahl
     *      Der Jahrgang ist als 4-stellige Zahl anzugeben, z.B. 1990.
     *
     *      - REQUIRED -
     */
    private $jahrgang;

    /**
     * @var Zahl
     */
    private $altersklasse;

    // Verein

    /**
     * @var Zeichenkette
     *      Name des Vereins
     *
     *      - REQUIRED -
     */
    private $vereinsbezeichnung;

    /**
     * @var Zahl
     *      4-stellige durch den DSV für jeden Verein festgelegte
     *      Zahl. Bei nicht dem DSV angehörenden Vereinen ist 0
     *      anzugeben.
     *
     *      - REQUIRED -
     */
    private $vereinskennzahl;


    /**
     * @var Zeit
     *      Endzeit (Ggf / Standard: 00:00:00,00)
     *
     *      - REQUIRED -
     */
    private $endzeit;


    /**
     * @var Zeichenkette
     *      Bemerkungen / Disqualifikationsgrund.
     */
    private $nichtwertungsbemerkung;

    /**
     * @var Zeichen
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      - E = Norm erreicht,
     *      - F = ENM fällig,
     *      - N = Norm nicht erreicht, aber nachgewiesen
     *
     *      - NEEDS CHECK -
     */
    private $enm;

    /**
     * Ausschreibungimnetz constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->wettkampfnummer        = new Zahl();
        $this->wettkampfart           = new Zeichen();
        $this->wertungsnummer         = new Zahl();
        $this->platz                  = new Zahl();
        $this->nichtwertungsgrund     = new Zeichenkette();
        $this->name                   = new Zeichenkette();
        $this->dsv_id                 = new Zahl();
        $this->personennummer         = new Zahl();
        $this->geschlecht             = new Zeichen();
        $this->jahrgang               = new Zahl();
        $this->altersklasse           = new Zahl();
        $this->vereinsbezeichnung     = new Zeichenkette();
        $this->vereinskennzahl        = new Zahl();
        $this->endzeit                = new Zeit("00:00:00,00");
        $this->nichtwertungsbemerkung = new Zeichenkette();
        $this->enm                    = new Zeichen();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 16;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3, 4, 6, 7, 8, 9, 10, 12, 13, 14];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function check_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        if ( !($wettkampfnummer->is_set() and
               $this->get_parent()->search_wettkampf($wettkampfnummer, false) !== null) )
        {
            throw new Runtime_Exception(self::WETTKAMPFNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $wettkampfnummer
     * @throws Runtime_Exception
     */
    public function set_wettkampfnummer(Zahl $wettkampfnummer): void
    {
        $this->check_wettkampfnummer($wettkampfnummer);
        $this->wettkampfnummer = $wettkampfnummer;
    }

    /**
     * @return Zahl
     */
    public function get_wettkampfnummer(): Zahl
    {
        return $this->wettkampfnummer;
    }

    /**
     * @param Zeichen $wettkampfart
     * @throws Runtime_Exception
     */
    public function check_wettkampfart(Zeichen $wettkampfart): void
    {
        if ( !in_array($wettkampfart->get_formatted(), self::WETTKAMPFART_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::WETTKAMPFART_ERROR, implode(', ', self::WETTKAMPFART_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $wettkampfart
     * @throws Runtime_Exception
     */
    public function set_wettkampfart(Zeichen $wettkampfart): void
    {
        $this->check_wettkampfart($wettkampfart);
        $this->wettkampfart = $wettkampfart;
    }

    /**
     * @return Zeichen
     */
    public function get_wettkampfart(): Zeichen
    {
        return $this->wettkampfart;
    }

    /**
     * @param Zahl $wertungsnummer
     * @throws Runtime_Exception
     */
    public function check_wertungsnummer(Zahl $wertungsnummer): void
    {
        if ( !($wertungsnummer->is_set() and $this->get_parent()->search_wertung($wertungsnummer) !== null) )
        {
            throw new Runtime_Exception(self::WERTUNGSNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $wertungsnummer
     * @throws Runtime_Exception
     */
    public function set_wertungsnummer(Zahl $wertungsnummer): void
    {
        $this->check_wertungsnummer($wertungsnummer);
        $this->wertungsnummer = $wertungsnummer;
    }

    /**
     * @return Zahl
     */
    public function get_wertungsnummer(): Zahl
    {
        return $this->wertungsnummer;
    }

    /**
     *
     * @param Zahl $platz
     * @throws Runtime_Exception
     */
    public function check_platz(Zahl $platz): void
    {
        if ( !$platz->is_set() )
        {
            throw new Runtime_Exception(self::PLATZ_ERROR);
        }
    }

    /**
     * @param Zahl $platz
     * @throws Runtime_Exception
     */
    public function set_platz(Zahl $platz): void
    {
        $this->check_platz($platz);
        $this->platz = $platz;
    }

    /**
     * @return Zahl
     */
    public function get_platz(): Zahl
    {
        return $this->platz;
    }

    /**
     * @param Zeichenkette $nichtwertungsgrund
     * @throws Runtime_Exception
     */
    public function check_nichtwertungsgrund(Zeichenkette $nichtwertungsgrund): void
    {
        $placement_implies_disqualification = $this->platz->get_zahl() === 0;
        if ( $placement_implies_disqualification and
             !in_array($nichtwertungsgrund, self::NICHTWERTUNGSGRUND_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::NICHTWERTUNGSGRUND_ERROR, implode(', ', self::NICHTWERTUNGSGRUND_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichenkette $nichtwertungsgrund
     * @throws Runtime_Exception
     */
    public function set_nichtwertungsgrund(Zeichenkette $nichtwertungsgrund): void
    {
        $this->check_nichtwertungsgrund($nichtwertungsgrund);
        $this->nichtwertungsgrund = $nichtwertungsgrund;

        // Non-empty values lead to: Platz => 0
        if ( $nichtwertungsgrund->is_set() )
        {
            $this->platz->set_from_string('0');
        }
    }

    /**
     * @return Zeichenkette
     */
    public function get_nichtwertungsgrund(): Zeichenkette
    {
        return $this->nichtwertungsgrund;
    }

    /**
     * @param Zeichenkette $name
     * @throws Runtime_Exception
     */
    public function check_name(Zeichenkette $name): void
    {
        if ( $name->get_formatted() === '' )
        {
            throw new Runtime_Exception(self::NAME_ERROR);
        }
    }

    /**
     * @param Zeichenkette $name
     * @throws Runtime_Exception
     */
    public function set_name(Zeichenkette $name): void
    {
        $this->check_name($name);
        $this->name = $name;
    }

    /**
     * @return Zeichenkette
     */
    public function get_name(): Zeichenkette
    {
        return $this->name;
    }

    /**
     * @param Zahl $dsv_id
     * @throws Runtime_Exception
     */
    public function check_dsv_id(Zahl $dsv_id): void
    {
        if ( $dsv_id->get_formatted() !== '' and
             strlen($dsv_id->get_formatted()) !== 6 )
        {
            throw new Runtime_Exception(self::DSV_ID_ERROR);
        }
    }

    /**
     * @param Zahl $dsv_id
     * @throws Runtime_Exception
     */
    public function set_dsv_id(Zahl $dsv_id): void
    {
        $this->check_dsv_id($dsv_id);
        $this->dsv_id = $dsv_id;
    }

    /**
     * @return Zahl
     */
    public function get_dsv_id(): Zahl
    {
        return $this->dsv_id;
    }

    /**
     * @param Zahl $personennummer
     * @throws Runtime_Exception
     */
    public function check_personennummer(Zahl $personennummer): void
    {
        $correct = false;
        if ( $personennummer->is_set() )
        {
            $search_result = $this->get_parent()->search_person($personennummer, false);
            $correct       = ($search_result === null or $search_result === $this);
        }
        if ( !$correct )
        {
            throw new Runtime_Exception(self::PERSONENNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $personennummer
     * @throws Runtime_Exception
     */
    public function set_personennummer(Zahl $personennummer): void
    {
        $this->check_personennummer($personennummer);
        $this->personennummer = $personennummer;
    }

    /**
     * @return Zahl
     */
    public function get_personennummer(): Zahl
    {
        return $this->personennummer;
    }

    /**
     * @param Zeichen $geschlecht
     * @throws Runtime_Exception
     */
    public function check_geschlecht(Zeichen $geschlecht): void
    {
        if ( !in_array($geschlecht->get_formatted(), self::GESCHLECHT_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::GESCHLECHT_ERROR, implode(', ', self::GESCHLECHT_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $geschlecht
     * @throws Runtime_Exception
     */
    public function set_geschlecht(Zeichen $geschlecht): void
    {
        $this->check_geschlecht($geschlecht);
        $this->geschlecht = $geschlecht;
    }

    /**
     * @return Zeichen
     */
    public function get_geschlecht(): Zeichen
    {
        return $this->geschlecht;
    }

    /**
     * @param Zahl $jahrgang
     * @throws Runtime_Exception
     */
    public function check_jahrgang(Zahl $jahrgang): void
    {
        if ( strlen($jahrgang->get_formatted()) !== 4 )
        {
            throw new Runtime_Exception(self::JAHRGANG_ERROR);
        }
    }

    /**
     * @param Zahl $jahrgang
     * @throws Runtime_Exception
     */
    public function set_jahrgang(Zahl $jahrgang): void
    {
        $this->check_jahrgang($jahrgang);
        $this->jahrgang = $jahrgang;
    }

    /**
     * @return Zahl
     */
    public function get_jahrgang(): Zahl
    {
        return $this->jahrgang;
    }

    /**
     * @param Zahl $altersklasse
     */
    public function set_altersklasse(Zahl $altersklasse): void
    {
        $this->altersklasse = $altersklasse;
    }

    /**
     * @return Zahl
     */
    public function get_altersklasse(): Zahl
    {
        return $this->altersklasse;
    }

    /**
     * @param Zeichenkette $vereinsbezeichnung
     * @throws Runtime_Exception
     */
    public function check_vereinsbezeichnung(Zeichenkette $vereinsbezeichnung): void
    {
        if ( strlen($vereinsbezeichnung->get_formatted()) === 0 )
        {
            throw new Runtime_Exception(self::VEREINSBEZEICHNUNG_ERROR);
        }
    }

    /**
     * @param Zeichenkette $vereinsbezeichnung
     * @throws Runtime_Exception
     */
    public function set_vereinsbezeichnung(Zeichenkette $vereinsbezeichnung): void
    {
        $this->check_vereinsbezeichnung($vereinsbezeichnung);
        $this->vereinsbezeichnung = $vereinsbezeichnung;
    }

    /**
     * @return Zeichenkette
     */
    public function get_vereinsbezeichnung(): Zeichenkette
    {
        return $this->vereinsbezeichnung;
    }

    /**
     * @param Zahl $vereinskennzahl
     * @throws Runtime_Exception
     */
    public function check_vereinskennzahl(Zahl $vereinskennzahl): void
    {
        $vkz = $vereinskennzahl->get_zahl();

        if ( !($vereinskennzahl->is_set() and
               $vkz === 0 or
               ($vkz > 0 and strlen(strval($vkz)) === 4)) )
        {
            throw new Runtime_Exception(self::VEREINSKENNZAHL_ERROR);
        }
    }

    /**
     * @param Zahl $vereinskennzahl
     * @throws Runtime_Exception
     */
    public function set_vereinskennzahl(Zahl $vereinskennzahl): void
    {
        $this->check_vereinskennzahl($vereinskennzahl);
        $this->vereinskennzahl = $vereinskennzahl;
    }

    /**
     * @return Zahl
     */
    public function get_vereinskennzahl(): Zahl
    {
        return $this->vereinskennzahl;
    }

    /**
     * @param Zeit $endzeit
     * @throws Runtime_Exception
     */
    public function check_endzeit(Zeit $endzeit): void
    {
        if ( !$endzeit->is_set() )
        {
            throw new Runtime_Exception(self::ENDZEIT_ERROR);
        }
    }

    /**
     * @param Zeit $endzeit
     * @throws Runtime_Exception
     */
    public function set_endzeit(Zeit $endzeit): void
    {
        $this->check_endzeit($endzeit);
        $this->endzeit = $endzeit;
    }

    /**
     * @return Zeit
     */
    public function get_endzeit(): Zeit
    {
        return $this->endzeit;
    }

    /**
     * @param Zeichenkette $nichtwertungsbemerkung
     */
    public function set_nichtwertungsbemerkung(Zeichenkette $nichtwertungsbemerkung): void
    {
        $this->nichtwertungsbemerkung = $nichtwertungsbemerkung;
    }

    /**
     * @return Zeichenkette
     */
    public function get_nichtwertungsbemerkung(): Zeichenkette
    {
        return $this->nichtwertungsbemerkung;
    }

    /**
     * @param Zeichen $enm
     * @throws Runtime_Exception
     */
    public function check_enm(Zeichen $enm): void
    {
        if ( $enm->get_formatted() !== '' and !in_array($enm, self::ENM_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::ENM_ERROR, implode(', ', self::ENM_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichen $enm
     * @throws Runtime_Exception
     */
    public function set_enm(Zeichen $enm): void
    {
        $this->check_enm($enm);
        $this->enm = $enm;
    }

    /**
     * @return Zeichen
     */
    public function get_enm(): Zeichen
    {
        return $this->enm;
    }

}