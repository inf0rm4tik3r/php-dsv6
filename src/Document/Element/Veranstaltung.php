<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Veranstaltung
 * @package Lukaspotthast\DSV\Document\Element
 */
class Veranstaltung extends Document_Element
{

    const VERANSTALTUNGSBEZEICHNUNG_ERROR = '"Veranstaltungsbezeichnung must be set."';
    const VERANSTALTUNGSORT_ERROR         = '"Veranstaltungsort" must be set.';
    const BAHNLAENGE_ERROR                = '"Bahnlaenge" was not one of: [%s]';
    const ZEITMESSUNG_ERROR               = '"Zeitmessung" was not one of: [%s]';

    const BAHNLAENGE_OPTIONS = [
        '16',
        '20',
        '25',
        '33',
        '50',
        'X',  // Sonstige Länge
        'FW'  // Freiwasser
    ];

    const ZEITMESSUNG_OPTIONS = [
        'HANDZEIT',
        'HALBAUTOMATISCH',
        'AUTOMATISCH',
    ];

    /**
     * @var Zeichenkette
     *      Name der Veranstaltung.
     *
     *      - REQUIRED -
     */
    private $veranstaltungsbezeichnung;

    /**
     * @var Zeichenkette
     *      Veranstaltungsort.
     *
     *      - REQUIRED -
     */
    private $veranstaltungsort;

    /**
     * @var Zeichenkette
     *      Falls die Bahnlänge nicht 16 2/3, 20, 25, 33 1/3, 50 Meter oder
     *      FW (Freiwasser) beträgt, ist X anzugeben. Außerdem ist
     *      festgelegt, dass für die Länge von "16 2/3m" 16 anzugeben ist
     *      und für "33 1/3m" 33.
     *
     *      - REQUIRED -
     */
    private $bahnlaenge;

    /**
     * @var Zeichenkette
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      - HANDZEIT,
     *      - AUTOMATISCH,
     *      - HALBAUTOMATISCH
     *
     *      - REQUIRED -
     */
    private $zeitmessung;

    /**
     * Veranstaltung constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->veranstaltungsbezeichnung = new Zeichenkette();
        $this->veranstaltungsort         = new Zeichenkette();
        $this->bahnlaenge                = new Zeichenkette();
        $this->zeitmessung               = new Zeichenkette();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 4;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2, 3, 4];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zeichenkette $veranstaltungsbezeichnung
     * @throws Runtime_Exception
     */
    public function check_veranstaltungsbezeichnung(Zeichenkette $veranstaltungsbezeichnung): void
    {
        if ( strlen($veranstaltungsbezeichnung->get_formatted()) === 0 )
        {
            throw new Runtime_Exception(self::VERANSTALTUNGSBEZEICHNUNG_ERROR);
        }
    }

    /**
     * @param Zeichenkette $veranstaltungsbezeichnung
     * @throws Runtime_Exception
     */
    public function set_veranstaltungsbezeichnung(Zeichenkette $veranstaltungsbezeichnung): void
    {
        $this->check_veranstaltungsbezeichnung($veranstaltungsbezeichnung);
        $this->veranstaltungsbezeichnung = $veranstaltungsbezeichnung;
    }

    /**
     * @return Zeichenkette
     */
    public function get_veranstaltungsbezeichnung(): Zeichenkette
    {
        return $this->veranstaltungsbezeichnung;
    }

    /**
     * @param Zeichenkette $veranstaltungsort
     * @throws Runtime_Exception
     */
    public function check_veranstaltungsort(Zeichenkette $veranstaltungsort): void
    {
        if ( strlen($veranstaltungsort->get_formatted()) === 0 )
        {
            throw new Runtime_Exception(self::VERANSTALTUNGSORT_ERROR);
        }
    }

    /**
     * @param Zeichenkette $veranstaltungsort
     * @throws Runtime_Exception
     */
    public function set_veranstaltungsort(Zeichenkette $veranstaltungsort): void
    {
        $this->check_veranstaltungsort($veranstaltungsort);
        $this->veranstaltungsort = $veranstaltungsort;
    }

    /**
     * @return Zeichenkette
     */
    public function get_veranstaltungsort(): Zeichenkette
    {
        return $this->veranstaltungsort;
    }

    /**
     * @param Zeichenkette $bahnlaenge
     * @throws Runtime_Exception
     */
    public function check_bahnlaenge(Zeichenkette $bahnlaenge): void
    {
        if ( !in_array($bahnlaenge->get_formatted(), self::BAHNLAENGE_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::BAHNLAENGE_ERROR, implode(', ', self::BAHNLAENGE_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichenkette $bahnlaenge
     * @throws Runtime_Exception
     */
    public function set_bahnlaenge(Zeichenkette $bahnlaenge): void
    {
        $this->check_bahnlaenge($bahnlaenge);
        $this->bahnlaenge = $bahnlaenge;
    }

    /**
     * @return Zeichenkette
     */
    public function get_bahnlaenge(): Zeichenkette
    {
        return $this->bahnlaenge;
    }

    /**
     * @param Zeichenkette $zeitmessung
     * @throws Runtime_Exception
     */
    public function check_zeitmessung(Zeichenkette $zeitmessung): void
    {
        if ( !in_array($zeitmessung->get_formatted(), self::ZEITMESSUNG_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::ZEITMESSUNG_ERROR, implode(', ', self::ZEITMESSUNG_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichenkette $zeitmessung
     * @throws Runtime_Exception
     */
    public function set_zeitmessung(Zeichenkette $zeitmessung): void
    {
        $this->check_zeitmessung($zeitmessung);
        $this->zeitmessung = $zeitmessung;
    }

    /**
     * @return Zeichenkette
     */
    public function get_zeitmessung(): Zeichenkette
    {
        return $this->zeitmessung;
    }

}