<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Datum;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Nachweis
 * @package Lukaspotthast\DSV\Document\Element
 */
class Nachweis extends Document_Element
{

    const NACHWEIS_VON_ERROR = '"Nachweis von" must be set.';
    const BAHNLAENGE_ERROR   = '"Bahnlaenge" is not one of: [%s].';

    const BAHNLAENGE_OPTIONS = [
        '25', // Nur 25m Bahn
        '50', // Nur 50m Bahn
        'FW', // Freiwasser
        'AL'  // Alle Bahnlängen
    ];

    /**
     * @var Datum
     *      Datum, ab wann Zeiten für den Pflichtzeitennachweis
     *      berücksichtigt werden können.
     *
     *      - REQUIRED -
     */
    private $nachweis_von;

    /**
     * @var Datum
     *      Datum, bis wann Zeiten für den Pflichtzeitennachweis
     *      berücksichtigt werden können.
     */
    private $nachweis_bis;

    /**
     * @var Zeichenkette
     *      Angabe, auf welcher Bahnlänge Zeiten für den
     *      Pflichtzeitennachweis berücksichtigt werden können.
     *      Zulässige Werte:
     *      - 25 = Nur 25m Bahn,
     *      - 50 = Nur 50m Bahn,
     *      - FW = Freiwasser,
     *      - AL = alle Bahnlängen
     *
     *      - REQUIRED -
     */
    private $bahnlaenge;

    /**
     * Nachweis constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->nachweis_von = new Datum();
        $this->nachweis_bis = new Datum();
        $this->bahnlaenge   = new Zeichenkette();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 3;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 3];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Datum $nachweis_von
     * @throws Runtime_Exception
     */
    public function check_nachweis_von(Datum $nachweis_von): void
    {
        if ( !$nachweis_von->is_set() )
        {
            throw new Runtime_Exception(self::NACHWEIS_VON_ERROR);
        }
    }

    /**
     * @param Datum $nachweis_von
     * @throws Runtime_Exception
     */
    public function set_nachweis_von(Datum $nachweis_von): void
    {
        $this->check_nachweis_von($nachweis_von);
        $this->nachweis_von = $nachweis_von;
    }

    /**
     * @return Datum
     */
    public function get_nachweis_von(): Datum
    {
        return $this->nachweis_von;
    }

    /**
     * @param Datum $nachweis_bis
     */
    public function set_nachweis_bis(Datum $nachweis_bis): void
    {
        $this->nachweis_bis = $nachweis_bis;
    }

    /**
     * @return Datum
     */
    public function get_nachweis_bis(): Datum
    {
        return $this->nachweis_bis;
    }

    /**
     * @param Zeichenkette $bahnlaenge
     * @throws Runtime_Exception
     */
    public function check_bahnlaenge(Zeichenkette $bahnlaenge): void
    {
        if ( !in_array($bahnlaenge->get_formatted(), self::BAHNLAENGE_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::BAHNLAENGE_ERROR, implode(', ', self::BAHNLAENGE_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichenkette $bahnlaenge
     * @throws Runtime_Exception
     */
    public function set_bahnlaenge(Zeichenkette $bahnlaenge): void
    {
        $this->check_bahnlaenge($bahnlaenge);
        $this->bahnlaenge = $bahnlaenge;
    }

    /**
     * @return Zeichenkette
     */
    public function get_bahnlaenge(): Zeichenkette
    {
        return $this->bahnlaenge;
    }

}