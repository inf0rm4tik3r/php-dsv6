<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Bankverbindung
 * @package Lukaspotthast\DSV\Document\Element
 */
class Bankverbindung extends Document_Element
{

    const BANKNAME_ERROR = '"Bankverbindung" must be set.';
    const IBAN_ERROR     = '"IBAN" must be set.';
    const BIC_ERROR      = '"BIC" must be set.';

    /**
     * @var Zeichenkette
     *      Bankverbindung für die Überweisung der Meldegelder.
     *
     *      - REQUIRED -
     */
    private $bankname;

    /**
     * @var Zeichenkette
     *
     *      - REQUIRED -
     */
    private $iban;

    /**
     * @var Zeichenkette
     *
     *      - REQUIRED -
     */
    private $bic;

    /**
     * Bankverbindung constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->bankname = new Zeichenkette();
        $this->iban     = new Zeichenkette();
        $this->bic      = new Zeichenkette();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 3;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [2, 3];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Zeichenkette $bankname
     * @throws Runtime_Exception
     */
    public function check_bankname(Zeichenkette $bankname): void
    {
        if ( !strlen($bankname->get_formatted()) > 0 )
        {
            throw new Runtime_Exception(self::BANKNAME_ERROR);
        }
    }

    /**
     * @param Zeichenkette $bankname
     * @throws Runtime_Exception
     */
    public function set_bankname(Zeichenkette $bankname): void
    {
        $this->check_bankname($bankname);
        $this->bankname = $bankname;
    }

    /**
     * @return Zeichenkette
     */
    public function get_bankname(): Zeichenkette
    {
        return $this->bankname;
    }

    /**
     * @param Zeichenkette $iban
     * @throws Runtime_Exception
     */
    public function check_iban(Zeichenkette $iban): void
    {
        if ( !strlen($iban->get_formatted()) > 0 )
        {
            throw new Runtime_Exception(self::IBAN_ERROR);
        }
    }

    /**
     * @param Zeichenkette $iban
     * @throws Runtime_Exception
     */
    public function set_iban(Zeichenkette $iban): void
    {
        $this->check_iban($iban);
        $this->iban = $iban;
    }

    /**
     * @return Zeichenkette
     */
    public function get_iban(): Zeichenkette
    {
        return $this->iban;
    }

    /**
     * @param Zeichenkette $bic
     * @throws Runtime_Exception
     */
    public function check_bic(Zeichenkette $bic): void
    {
        if ( !strlen($bic->get_formatted()) > 0 )
        {
            throw new Runtime_Exception(self::BIC_ERROR);
        }
    }

    /**
     * @param Zeichenkette $bic
     * @throws Runtime_Exception
     */
    public function set_bic(Zeichenkette $bic): void
    {
        $this->check_bic($bic);
        $this->bic = $bic;
    }

    /**
     * @return Zeichenkette
     */
    public function get_bic(): Zeichenkette
    {
        return $this->bic;
    }

}