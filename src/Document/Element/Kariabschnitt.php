<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Element;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Document_Element;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Kariabschnitt
 * @package Lukaspotthast\DSV\Document\Element
 */
class Kariabschnitt extends Document_Element
{

    const KAMPFRICHTERNUMMER_ERROR = '"Kampfrichternummer" was not set or did not reference an existing Kampfrichter ' .
                                     'previously defined through a "Karimeldung".';
    const ABSCHNITTSNUMMER_ERROR   = '"Abschnittsnummer" was not set or did not reference an existing "Abschnitt".';
    const EINSATZWUNSCH_ERROR      = '"Kampfrichtergruppe" was not one of: [%s]';

    const EINSATZWUNSCH_OPTIONS = [
        'SCH', // Schiedsrichter
        'STA', // Starter
        'ZRO', // Zielrichterobmann
        'ZR',  // Zielrichter
        'ZNO', // Zeitnehmerobmann
        'ZN',  // Zeitnehmer
        'RZN', // Reservezeitnehmer
        'SR',  // Schwimmrichter
        'WRO', // Wenderichterobmann
        'WR',  // Wenderichter
        'AUS', // Auswerter
        'SP',  // Sprecher
        'PKF', // Protokollführer
    ];

    /**
     * @var Zahl
     *      Eindeutige numerische Kennung des Kampfrichters, definiert
     *      in der KARIMELDUNG.
     *
     *      - REQUIRED -
     */
    private $kampfrichternummer;

    /**
     * @var Zahl
     *      Hier werden die Abschnitte angegeben, für die der gemeldete
     *      Kampfrichter zur Verfügung steht. Es muss für jeden
     *      Abschnitt einen eigenen Eintrag geben. Der Abschnitt muss existieren.
     *
     *      - REQUIRED -
     */
    private $abschnittsnummer;

    /**
     * @var Zeichenkette
     *      Es stehen folgende Auswahlen zur Verfügung:
     *      SCH = Schiedsrichter
     *      STA = Starter
     *      ZRO = Zielrichterobmann
     *      ZR = Zielrichter
     *      ZNO = Zeitnehmerobmann
     *      ZN = Zeitnehmer
     *      RZN = Reservezeitnehmer
     *      SR = Schwimmrichter
     *      WRO = Wenderichterobmann
     *      WR = Wenderichter
     *      AUS = Auswerter
     *      SP = Sprecher
     *      PKF = Protokollführer
     */
    private $einsatzwunsch;

    /**
     * Kariabschnitt constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    public function __construct(Document $parent, ?Statement $stmt = null)
    {
        $this->kampfrichternummer = new Zahl();
        $this->abschnittsnummer   = new Zahl();
        $this->einsatzwunsch      = new Zeichenkette();

        parent::__construct($parent, $stmt);
    }

    /**
     * @return int
     */
    public static function get_attribute_amount(): int
    {
        return 3;
    }

    /**
     * @return array [int]
     */
    public static function get_required_attribute_indices(): array
    {
        return [1, 2];
    }

    /**
     * @return array
     */
    protected function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @throws Runtime_Exception
     */
    public function perform_additional_integrity_checks(): void
    {
        // There must not be another "Kariabschnitt" with the same "Kampfrichternummer" and "Abschnittsnummer"!
        $kariabschnitte = $this->get_parent()->search_kariabschnitte($this->get_kampfrichternummer());
        $found          = 0;
        foreach ( $kariabschnitte as &$kariabschnitt )
        {
            /** @var Kariabschnitt $kariabschnitt */
            if ( $kariabschnitt->get_abschnittsnummer()->get_zahl() === $this->get_abschnittsnummer()->get_zahl() )
            {
                $found++;
            }
        }
        // We will always find our current object in there.
        if ( $found > 1 )
        {
            throw new Runtime_Exception('Duplicate specification of a "Kariabschnitt": ' . serialize($this));
        }
    }

    /**
     * @param Zahl $kampfrichternummer
     * @throws Runtime_Exception
     */
    public function check_kampfrichternummer(Zahl $kampfrichternummer): void
    {
        $correct = false;
        // The "Kampfrichternummer" must be set.
        if ( $kampfrichternummer->is_set() )
        {
            $search_result = $this->get_parent()->search_karimeldung($kampfrichternummer, false);
            // The specified "Kampfrichternummer" must reference an existing Kampfrichter.
            $correct = $search_result !== null;
        }
        if ( !$correct )
        {
            throw new Runtime_Exception(self::KAMPFRICHTERNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $kampfrichternummer
     * @throws Runtime_Exception
     */
    public function set_kampfrichternummer(Zahl $kampfrichternummer): void
    {
        $this->check_kampfrichternummer($kampfrichternummer);
        $this->kampfrichternummer = $kampfrichternummer;
    }

    /**
     * @return Zahl
     */
    public function get_kampfrichternummer(): Zahl
    {
        return $this->kampfrichternummer;
    }

    /**
     * @param Zahl $abschnittsnummer
     * @throws Runtime_Exception
     */
    public function check_abschnittsnummer(Zahl $abschnittsnummer): void
    {
        $correct = false;
        // The "Abschnittsnummer" must be set.
        if ( $abschnittsnummer->is_set() )
        {
            $search_result = $this->get_parent()->search_abschnitt($abschnittsnummer, false);
            // The specified "Abschnittsnummer" must reference an existing "Abschnitt".
            $correct = $search_result !== null;
        }
        if ( !$correct )
        {
            throw new Runtime_Exception(self::ABSCHNITTSNUMMER_ERROR);
        }
    }

    /**
     * @param Zahl $abschnittsnummer
     * @throws Runtime_Exception
     */
    public function set_abschnittsnummer(Zahl $abschnittsnummer): void
    {
        $this->check_abschnittsnummer($abschnittsnummer);
        $this->abschnittsnummer = $abschnittsnummer;
    }

    /**
     * @return Zahl
     */
    public function get_abschnittsnummer(): Zahl
    {
        return $this->abschnittsnummer;
    }

    /**
     * @param Zeichenkette $einsatzwunsch
     * @throws Runtime_Exception
     */
    public function check_einsatzwunsch(Zeichenkette $einsatzwunsch): void
    {
        if ( $einsatzwunsch->get_formatted() !== '' and
             !in_array($einsatzwunsch->get_formatted(), self::EINSATZWUNSCH_OPTIONS) )
        {
            throw new Runtime_Exception(
                sprintf(self::EINSATZWUNSCH_ERROR, implode(', ', self::EINSATZWUNSCH_OPTIONS))
            );
        }
    }

    /**
     * @param Zeichenkette $einsatzwunsch
     * @throws Runtime_Exception
     */
    public function set_einsatzwunsch(Zeichenkette $einsatzwunsch): void
    {
        $this->check_einsatzwunsch($einsatzwunsch);
        $this->einsatzwunsch = $einsatzwunsch;
    }

    /**
     * @return Zeichenkette
     */
    public function get_einsatzwunsch(): Zeichenkette
    {
        return $this->einsatzwunsch;
    }

}