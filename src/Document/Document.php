<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document;

use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Document\Element\Abschnitt;
use Lukaspotthast\DSV\Document\Element\Abschnitt_Definition;
use Lukaspotthast\DSV\Document\Element\Ansprechpartner;
use Lukaspotthast\DSV\Document\Element\Ausrichter;
use Lukaspotthast\DSV\Document\Element\Ausschreibungimnetz;
use Lukaspotthast\DSV\Document\Element\Bankverbindung;
use Lukaspotthast\DSV\Document\Element\Besonderes;
use Lukaspotthast\DSV\Document\Element\Dateiende;
use Lukaspotthast\DSV\Document\Element\Erzeuger;
use Lukaspotthast\DSV\Document\Element\Format;
use Lukaspotthast\DSV\Document\Element\Kampfgericht;
use Lukaspotthast\DSV\Document\Element\Kariabschnitt;
use Lukaspotthast\DSV\Document\Element\Karimeldung;
use Lukaspotthast\DSV\Document\Element\Meldeadresse;
use Lukaspotthast\DSV\Document\Element\Meldegeld;
use Lukaspotthast\DSV\Document\Element\Meldeschluss;
use Lukaspotthast\DSV\Document\Element\Nachweis;
use Lukaspotthast\DSV\Document\Element\Person;
use Lukaspotthast\DSV\Document\Element\Personenergebnis;
use Lukaspotthast\DSV\Document\Element\Pflichtzeit;
use Lukaspotthast\DSV\Document\Element\PNErgebnis;
use Lukaspotthast\DSV\Document\Element\PNMeldung;
use Lukaspotthast\DSV\Document\Element\PNReaktion;
use Lukaspotthast\DSV\Document\Element\PNZwischenzeit;
use Lukaspotthast\DSV\Document\Element\STAbloese;
use Lukaspotthast\DSV\Document\Element\Staffel;
use Lukaspotthast\DSV\Document\Element\Staffelergebnis;
use Lukaspotthast\DSV\Document\Element\Staffelperson;
use Lukaspotthast\DSV\Document\Element\StartPN;
use Lukaspotthast\DSV\Document\Element\StartST;
use Lukaspotthast\DSV\Document\Element\STErgebnis;
use Lukaspotthast\DSV\Document\Element\STMeldung;
use Lukaspotthast\DSV\Document\Element\STPerson;
use Lukaspotthast\DSV\Document\Element\STZwischenzeit;
use Lukaspotthast\DSV\Document\Element\Trainer;
use Lukaspotthast\DSV\Document\Element\Veranstalter;
use Lukaspotthast\DSV\Document\Element\Veranstaltung;
use Lukaspotthast\DSV\Document\Element\Veranstaltungsort;
use Lukaspotthast\DSV\Document\Element\Verein;
use Lukaspotthast\DSV\Document\Element\Wertung;
use Lukaspotthast\DSV\Document\Element\Wettkampf;
use Lukaspotthast\DSV\Document\Element\Wettkampf_Definition;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Comment;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Document\Structure\Ast;
use Lukaspotthast\DSV\Document\Writer\Text_Writer;
use Lukaspotthast\DSV\Document\Writer\Writer;
use Lukaspotthast\DSV\DSV;
use Lukaspotthast\DSV\Exception\Document_Creation_Exception;
use Lukaspotthast\DSV\Exception\Document_Integrity_Exception;
use Lukaspotthast\DSV\Exception\Document_Write_Exception;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Element_Integrity_Exception;
use Lukaspotthast\DSV\Exception\Occurrence_Constraint_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;
use Lukaspotthast\Support\Arg;

/**
 * Class Document
 * @package Lukaspotthast\DSV\Document
 */
abstract class Document
{

    /** @var array */
    private $document_element_class_references = [];

    /** @var array */
    private $occurrence_constraints = [];

    /** @var array */
    private $occurrences = [];

    /** @var array */
    private $comments = [];

    /** @var Format|null */
    private $format = null;

    /** @var Dateiende|null */
    private $dateiende = null;

    /** @var Writer */
    private $writer = null;

    /** @var Performance_Info */
    private $performance_info = null;

    /** @var array */
    private $retained_comments = [];

    /**
     * Document constructor.
     * @throws Runtime_Exception
     */
    public function __construct()
    {
        switch ( $this->get_listart() )
        {
            case 'Wettkampfdefinitionsliste':
                $abschnitt_class     = Abschnitt_Definition::class;
                $wettkampf_class     = Wettkampf_Definition::class;
                $staffelperson_class = STPerson::class;
                break;
            case 'Vereinsmeldeliste':
                $abschnitt_class     = Abschnitt::class;
                $wettkampf_class     = Wettkampf::class;
                $staffelperson_class = STPerson::class;
                break;
            case 'Vereinsergebnisliste':
                $abschnitt_class     = Abschnitt::class;
                $wettkampf_class     = Wettkampf_Definition::class;
                $staffelperson_class = Staffelperson::class;
                break;
            case 'Wettkampfergebnisliste':
                $abschnitt_class     = Abschnitt::class;
                $wettkampf_class     = Wettkampf_Definition::class;
                $staffelperson_class = STPerson::class;
                break;
            default:
                $abschnitt_class     = Abschnitt::class;
                $wettkampf_class     = Wettkampf::class;
                $staffelperson_class = STPerson::class;
                break;
        }

        $this->document_element_class_references = [
            Abschnitt::get_element_name()           => $abschnitt_class,
            Ansprechpartner::get_element_name()     => Ansprechpartner::class,
            Ausrichter::get_element_name()          => Ausrichter::class,
            Ausschreibungimnetz::get_element_name() => Ausschreibungimnetz::class,
            Bankverbindung::get_element_name()      => Bankverbindung::class,
            Besonderes::get_element_name()          => Besonderes::class,
            Dateiende::get_element_name()           => Dateiende::class,
            Erzeuger::get_element_name()            => Erzeuger::class,
            Format::get_element_name()              => Format::class,
            Kampfgericht::get_element_name()        => Kampfgericht::class,
            Kariabschnitt::get_element_name()       => Kariabschnitt::class,
            Karimeldung::get_element_name()         => Karimeldung::class,
            Meldeadresse::get_element_name()        => Meldeadresse::class,
            Meldegeld::get_element_name()           => Meldegeld::class,
            Meldeschluss::get_element_name()        => Meldeschluss::class,
            Nachweis::get_element_name()            => Nachweis::class,
            Person::get_element_name()              => Person::class,
            Personenergebnis::get_element_name()    => Personenergebnis::class,
            Pflichtzeit::get_element_name()         => Pflichtzeit::class,
            PNErgebnis::get_element_name()          => PNErgebnis::class,
            PNMeldung::get_element_name()           => PNMeldung::class,
            PNReaktion::get_element_name()          => PNReaktion::class,
            PNZwischenzeit::get_element_name()      => PNZwischenzeit::class,
            STAbloese::get_element_name()           => STAbloese::class,
            Staffel::get_element_name()             => Staffel::class,
            Staffelergebnis::get_element_name()     => Staffelergebnis::class,
            STPerson::get_element_name()            => $staffelperson_class,
            StartPN::get_element_name()             => StartPN::class,
            StartST::get_element_name()             => StartST::class,
            STErgebnis::get_element_name()          => STErgebnis::class,
            STMeldung::get_element_name()           => STMeldung::class,
            STZwischenzeit::get_element_name()      => STZwischenzeit::class,
            Trainer::get_element_name()             => Trainer::class,
            Veranstalter::get_element_name()        => Veranstalter::class,
            Veranstaltung::get_element_name()       => Veranstaltung::class,
            Veranstaltungsort::get_element_name()   => Veranstaltungsort::class,
            Verein::get_element_name()              => Verein::class,
            Wertung::get_element_name()             => Wertung::class,
            Wettkampf::get_element_name()           => $wettkampf_class,
        ];

        // Define default occurrences: All 0
        $this->occurrences = [];
        foreach ( array_keys($this->document_element_class_references) as &$element_name )
        {
            $this->occurrences[$element_name] = 0;
        }

        // Set the occurrence constraints by asking the child class.
        $this->set_occurrence_constraints($this->create_occurrence_constraints());

        // Create the document format by asking the child class for its type.
        $this->set_format($this->create_format());

        // Use the Text_Writer as the default writer.
        $this->writer = new Text_Writer();

        // Create an empty performance info.
        $this->performance_info = new Performance_Info($this);
    }

    /**
     * @return array
     */
    public abstract function create_occurrence_constraints(): array;

    /**
     * @return string
     */
    public abstract function get_listart(): string;

    /**
     * @return array
     */
    public abstract function get_attributes(): array;

    /**
     * Creates the document from the Ast.
     * @param Ast $ast
     * @throws Document_Creation_Exception
     * @throws Document_Integrity_Exception
     */
    public function create(Ast $ast): void
    {
        $this->retained_comments = [];

        foreach ( $ast as $node )
        {
            if ( $node instanceof Statement )
            {
                try
                {
                    $this->process_stmt($this->get_class_from_stmt($node), $node);
                }
                catch ( Runtime_Exception $e )
                {
                    throw new Document_Creation_Exception(
                        'Statement could not be processed.',
                        $ast, 0, $e
                    );
                }
            }
            else if ( $node instanceof Comment )
            {
                array_push($this->retained_comments, $node);
            }
            else
            {
                throw new Document_Creation_Exception(
                    'Unexpected element in the abstract syntax tree.', $ast
                );
            }
        }

        // Validate the integrity of the document before leaving!
        $this->check_integrity();
    }

    /**
     * @return Format
     * @throws Element_Creation_Exception
     * @throws Runtime_Exception
     */
    protected function create_format(): Format
    {
        $format = new Format($this);
        $format->get_listart()->set_from_string($this->get_listart());
        $format->get_version()->set_from_string('6');
        return $format;
    }

    /**
     * @return Erzeuger
     * @throws Element_Creation_Exception
     */
    protected function create_erzeuger(): Erzeuger
    {
        $e = new Erzeuger($this);
        $e->get_software()->set_from_string(DSV::ERZEUGER_SOFTWARE);
        $e->get_version()->set_from_string(DSV::ERZEUGER_VERSION);
        $e->get_kontakt()->set_from_string(DSV::ERZEUGER_KONTAKT);
        return $e;
    }

    /**
     * @param string    $class
     * @param Statement $stmt
     * @throws Runtime_Exception
     */
    protected function process_stmt(string $class, Statement $stmt)
    {
        // Create an object of the specified Document_Element.
        /** @var Document_Element $obj */
        $obj = new $class($this, $stmt);

        // Give it the comments which were read right before this statement was encountered.
        $obj->set_comments($this->retained_comments);
        $this->retained_comments = [];

        // Calculate whether the object must be set_ or add_-ed to this object.
        /** @noinspection PhpUndefinedMethodInspection */
        $co_result = $this->check_occurrences($class::get_element_name(), true);
        switch ( $co_result )
        {
            case 1:
                $function_prefix = 'set_';
                break;
            case 2:
                $function_prefix = 'add_';
                break;
            default:
                throw new Runtime_Exception(
                    __CLASS__ . ': check_occurrences() returned an unexpected value: ' . $co_result
                );
        }

        // set_ or add_ the object.
        $this->{$function_prefix . strtolower($stmt->get_name())}($obj);

        // Keep track that an object of $class was added.
        /** @noinspection PhpUndefinedMethodInspection */
        $this->add_occurrence($class::get_element_name());
    }

    /**
     * @param Statement $stmt
     * @return string
     * @throws Runtime_Exception
     */
    public final function get_class_from_stmt(Statement $stmt): string
    {
        $key = $stmt->get_name();

        if ( array_key_exists($key, $this->document_element_class_references) )
        {
            return $this->document_element_class_references[$key];
        }

        throw new Runtime_Exception(
            'Unable to find class reference for "' . $key . '".'
        );
    }

    /**
     * @throws Document_Integrity_Exception
     */
    public final function check_integrity(): void
    {
        try
        {
            $this->perform_integrity_check();
        }
        catch ( Element_Integrity_Exception $e )
        {
            throw new Document_Integrity_Exception('Document integrity is violated.', 0, $e);
        }
        catch ( Occurrence_Constraint_Exception $e )
        {
            throw new Document_Integrity_Exception('Document integrity is violated.', 0, $e);
        }
    }

    /**
     * @throws Occurrence_Constraint_Exception
     * @throws Element_Integrity_Exception
     */
    protected function perform_integrity_check(): void
    {
        $this->check_occurrence_constraints();
        $this->perform_member_integrity_checks();
    }

    /**
     * @throws Element_Integrity_Exception
     */
    protected function perform_member_integrity_checks(): void
    {
        foreach ( $this->get_attributes() as $attribute_name => $attribute )
        {
            $attribs = is_array($attribute) ? $attribute : [$attribute];
            foreach ( $attribs as $key => $var )
            {
                if ( $var !== null and is_object($var) )
                {
                    if ( $var instanceof Document_Element )
                    {
                        $var->check_integrity();
                    }
                }
            }
        }
    }

    /**
     * @return string
     * @throws Document_Write_Exception
     */
    public final function write(): string
    {
        // The integrity of the document must hold.
        try
        {
            $this->check_integrity();
        }
        catch ( Document_Integrity_Exception $e )
        {
            throw new Document_Write_Exception(
                'Could not write document.',
                $this, 0, $e
            );
        }

        $writer = $this->get_writer();

        $s = '';
        $s .= $writer->start_document($this);
        foreach ( $this->comments as &$comment )
        {
            $s .= $writer->start_comment();
            $s .= $writer->write_comment($comment);
            $s .= $writer->end_comment();
        }

        $s .= $this->get_format()->write($writer);
        try
        {
            $s .= $this->create_erzeuger()->write($writer);
        }
        catch ( Element_Creation_Exception $e )
        {
            throw new Document_Write_Exception(
                'Could not write document.',
                $this, 0, $e
            );
        }

        // Write all object attributes.
        foreach ( $this->get_attributes() as $name => &$attribute )
        {
            $attribs = is_array($attribute) ? $attribute : [$attribute];
            foreach ( $attribs as &$attrib )
            {
                // Do not render to original "Erzeuger".
                if ( $attrib instanceof Document_Element and !$attrib instanceof Erzeuger )
                {
                    $s .= $attrib->write($writer);
                }
            }
        }

        $s .= $this->get_dateiende()->write($writer);

        $s .= $writer->end_document($this);
        return $s;
    }

    /**
     * Checks if all specified occurrence constraints are fulfilled in the current state.
     * @throws Occurrence_Constraint_Exception
     */
    public function check_occurrence_constraints(): void
    {
        foreach ( array_keys($this->get_occurrence_constraints()) as &$element_name )
        {
            $fulfilled = $this->occurrence_constraint_fulfilled($element_name);

            if ( !$fulfilled )
            {
                throw new Occurrence_Constraint_Exception(
                    'Occurrence constraint ' . json_encode($this->occurrence_constraints[$element_name]) .
                    ' could not be fulfilled. There are ' . $this->occurrences[$element_name] . ' occurrences of: ' .
                    '"' . $element_name . '".'
                );
            }
        }
    }

    /**
     * Checks if the occurrence constraint of a specific element is fulfilled.
     * @param string $element_name
     * @return bool
     * @throws Occurrence_Constraint_Exception
     */
    public function occurrence_constraint_fulfilled(string $element_name): bool
    {
        $constraint = $this->get_occurrence_constraint($element_name);
        $cc         = count($constraint);

        $equal_x = ($cc === 1) && is_int($constraint[0]);
        $from_to = ($cc === 2) && is_int($constraint[0]) && is_int($constraint[1]) && ($constraint[0] < $constraint[1]);
        $from    = ($cc === 2) && is_int($constraint[0]) && is_string($constraint[1]) && ($constraint[1] === '*');

        if ( !($equal_x || $from_to || $from) )
        {
            throw new Occurrence_Constraint_Exception(
                'Unprocessable occurrence constraint specified: ' . implode(', ', $constraint)
            );
        }

        if ( $equal_x )
        {
            $min_occurrences = $constraint[0];
            $max_occurrences = $constraint[0];
            return
                $this->occurrences[$element_name] >= $min_occurrences &&
                $this->occurrences[$element_name] <= $max_occurrences;
        }
        else if ( $from_to )
        {
            $min_occurrences = $constraint[0];
            $max_occurrences = $constraint[1];
            return
                $this->occurrences[$element_name] >= $min_occurrences &&
                $this->occurrences[$element_name] <= $max_occurrences;
        }
        else if ( $from )
        {
            $min_occurrences = $constraint[0];
            return
                $this->occurrences[$element_name] >= $min_occurrences;
        }

        // Unreachable.
        return false;
    }

    /**
     * @param string $element_name
     * @param bool   $throw_on_error
     * @return int
     *       <pre>
     *       -1: if max occurrences are reached or on failure
     *       1: if max occurrences are not reached -> element can be set
     *       2: if max occurrences are not reached -> element can be added
     *       </pre>
     * @throws Occurrence_Constraint_Exception
     */
    public function check_occurrences(string $element_name, bool $throw_on_error = true): int
    {
        $constraint = $this->get_occurrence_constraint($element_name);
        $cc         = count($constraint);

        $equal_x = ($cc === 1) && is_int($constraint[0]);
        $from_to = ($cc === 2) && is_int($constraint[0]) && is_int($constraint[1]) && ($constraint[0] < $constraint[1]);
        $from    = ($cc === 2) && is_int($constraint[0]) && is_string($constraint[1]) && ($constraint[1] === '*');

        if ( !($equal_x || $from_to || $from) )
        {
            if ( $throw_on_error )
            {
                throw new Occurrence_Constraint_Exception(
                    'Unprocessable occurrence constraint specified: ' . implode(', ', $constraint)
                );
            }
            // Error.
            return -1;
        }

        $max_occurrences = null;
        if ( $equal_x )
        {
            $max_occurrences = $constraint[0];
            if ( $this->occurrences[$element_name] < $max_occurrences )
            {
                // Max occurrences not reached. (set)
                return 1;
            }
            else
            {
                // Max occurrences reached.
                return -1;
            }
        }
        else if ( $from_to )
        {
            $max_occurrences = $constraint[1];
            if ( $this->occurrences[$element_name] < $max_occurrences )
            {
                if ( $constraint[0] === 0 and $constraint[1] === 1 )
                {
                    // Max occurrences not reached. (set)
                    return 1;
                }
                // Max occurrences not reached. (set)
                return 2;
            }
            else
            {
                // Max occurrences reached.
                return -1;
            }
        }
        else if ( $from )
        {
            // Unlimited...
            // Max occurrences obviously not reached.
            return 2;
        }

        if ( $throw_on_error )
        {
            throw new Occurrence_Constraint_Exception(
                '"' . $element_name . '" is only allowed to be added ' . Arg::stringify($max_occurrences) . 'x.'
            );
        }
        return -1;
    }

    /**
     * @param Zahl $trainernummer
     * @param bool $throw_on_error
     * @return Trainer|null
     * @throws Runtime_Exception
     */
    public function search_trainer(Zahl $trainernummer, $throw_on_error = true): ?Trainer
    {
        if ( !method_exists($this, 'get_trainer') )
        {
            die(__CLASS__ . ': ' . __FUNCTION__ . '() ' . get_class($this) . ' should have a "get_trainer" method!');
        }

        if ( !$trainernummer->is_set() )
        {
            throw new Runtime_Exception(__CLASS__ . ': ' . __FUNCTION__ . '() Given $trainernummer was not set.');
        }

        foreach ( $this->get_trainer() as &$trainer )
        {
            /** @var Trainer $trainer */
            if ( $trainer->get_trainernummer()->is_set() )
            {
                if ( $trainer->get_trainernummer()->get_zahl() === $trainernummer->get_zahl() )
                {
                    return $trainer;
                }
            }
        }

        if ( $throw_on_error )
        {
            throw new Runtime_Exception(
                __CLASS__ . ': ' . __FUNCTION__ . '() ' .
                'Unable to find "Trainer" with $trainernummer: ' . $trainernummer->get_zahl()
            );
        }

        // Otherwise return null. Object could not be found.
        return null;
    }

    /**
     * @param Zahl $abschnittsnummer
     * @param bool $throw_on_error
     * @return Abschnitt_Definition|Abschnitt|null
     * @throws Runtime_Exception
     */
    public function search_abschnitt(Zahl $abschnittsnummer, $throw_on_error = true)
    {
        if ( !method_exists($this, 'get_abschnitte') )
        {
            die(__CLASS__ . ': ' . __FUNCTION__ . '() ' . get_class($this) . ' should have a "get_abschnitte" method!');
        }

        if ( !$abschnittsnummer->is_set() )
        {
            throw new Runtime_Exception(__CLASS__ . ': ' . __FUNCTION__ . '() Given $abschnittsnummer was not set.');
        }

        foreach ( $this->get_abschnitte() as &$abschnitt )
        {
            /** @var Abschnitt $abschnitt */
            if ( $abschnitt->get_abschnittsnummer()->is_set() )
            {
                if ( $abschnitt->get_abschnittsnummer()->get_zahl() === $abschnittsnummer->get_zahl() )
                {
                    return $abschnitt;
                }
            }
        }

        if ( $throw_on_error )
        {
            throw new Runtime_Exception(
                __CLASS__ . ': ' . __FUNCTION__ . '() ' .
                'Unable to find "Abschnitt" with $abschnittsnummer: ' . $abschnittsnummer->get_zahl()
            );
        }

        // Otherwise return null. Object could not be found.
        return null;
    }

    /**
     * @param Zahl $wettkampfnummer
     * @param bool $throw_on_error
     * @return Wettkampf_Definition|Wettkampf|null
     * @throws Runtime_Exception
     */
    public function search_wettkampf(Zahl $wettkampfnummer, $throw_on_error = true)
    {
        if ( !method_exists($this, 'get_wettkaempfe') )
        {
            die(__CLASS__ . ': ' . __FUNCTION__ . '() ' . get_class($this) . ' should have a "get_wettkaempfe" method!');
        }

        if ( !$wettkampfnummer->is_set() )
        {
            throw new Runtime_Exception(__CLASS__ . ': ' . __FUNCTION__ . '() Given $wettkampfnummer was not set.');
        }

        foreach ( $this->get_wettkaempfe() as &$wettkampf )
        {
            /** @var Wettkampf $wettkampf */
            if ( $wettkampf->get_wettkampfnummer()->is_set() )
            {
                if ( $wettkampf->get_wettkampfnummer()->get_zahl() === $wettkampfnummer->get_zahl() )
                {
                    return $wettkampf;
                }
            }
        }

        if ( $throw_on_error )
        {
            throw new Runtime_Exception(
                __CLASS__ . ': ' . __FUNCTION__ . '() ' .
                'Unable to find "Wettkampf" with $wettkampfnummer: ' . $wettkampfnummer->get_zahl()
            );
        }

        // Otherwise return null. Object could not be found.
        return null;
    }

    /**
     * @param Zahl $wertungsnummer
     * @param bool $throw_on_error
     * @return Wertung|null
     * @throws Runtime_Exception
     */
    public function search_wertung(Zahl $wertungsnummer, $throw_on_error = true): ?Wertung
    {
        if ( !method_exists($this, 'get_wertungen') )
        {
            die(__CLASS__ . ': ' . __FUNCTION__ . '() ' . get_class($this) . ' should have a "get_wertungen" method!');
        }

        if ( !$wertungsnummer->is_set() )
        {
            throw new Runtime_Exception(__CLASS__ . ': ' . __FUNCTION__ . '() Given $wertungsnummer was not set.');
        }

        foreach ( $this->get_wertungen() as &$wertung )
        {
            /** @var Wertung $wertung */
            if ( $wertung->get_wertungsnummer()->is_set() )
            {
                if ( $wertung->get_wertungsnummer()->get_zahl() === $wertungsnummer->get_zahl() )
                {
                    return $wertung;
                }
            }
        }

        if ( $throw_on_error )
        {
            throw new Runtime_Exception(
                __CLASS__ . ': ' . __FUNCTION__ . '() ' .
                'Unable to find "Wertung" with $wertungsnummer: ' . $wertungsnummer->get_zahl()
            );
        }

        // Otherwise return null. Object could not be found.
        return null;
    }

    /**
     * @param Zahl $kampfrichternummer
     * @param bool $throw_on_error
     * @return Trainer|null
     * @throws Runtime_Exception
     */
    public function search_karimeldung(Zahl $kampfrichternummer, $throw_on_error = true): ?Karimeldung
    {
        if ( !method_exists($this, 'get_karimeldungen') )
        {
            die(__CLASS__ . ': ' . __FUNCTION__ . '() ' . get_class($this) . ' should have a "get_karimeldungen" method!');
        }

        if ( !$kampfrichternummer->is_set() )
        {
            throw new Runtime_Exception(__CLASS__ . ': ' . __FUNCTION__ . '() Given $kampfrichternummer was not set.');
        }

        foreach ( $this->get_karimeldungen() as &$kampfrichter )
        {
            /** @var Karimeldung $kampfrichter */
            if ( $kampfrichter->get_kampfrichternummer()->is_set() )
            {
                if ( $kampfrichter->get_kampfrichternummer()->get_zahl() === $kampfrichternummer->get_zahl() )
                {
                    return $kampfrichter;
                }
            }
        }

        if ( $throw_on_error )
        {
            throw new Runtime_Exception(
                __CLASS__ . ': ' . __FUNCTION__ . '() ' .
                'Unable to find "Karimeldung" with $kampfrichternummer: ' . $kampfrichternummer->get_zahl()
            );
        }

        // Otherwise return null. Object could not be found.
        return null;
    }

    /**
     * @param Zahl $kampfrichternummer
     * @return array
     * @throws Runtime_Exception
     */
    public function search_kariabschnitte(Zahl $kampfrichternummer): array
    {
        if ( !method_exists($this, 'get_kariabschnitte') )
        {
            die(__CLASS__ . ': ' . __FUNCTION__ . '() ' . get_class($this) . ' should have a "get_kariabschnitte" method!');
        }

        if ( !$kampfrichternummer->is_set() )
        {
            throw new Runtime_Exception(__CLASS__ . ': ' . __FUNCTION__ . '() Given $kampfrichternummer was not set.');
        }

        $found = [];

        foreach ( $this->get_kariabschnitte() as &$kariabschnitt )
        {
            /** @var Kariabschnitt $kariabschnitt */
            if ( $kariabschnitt->get_kampfrichternummer()->is_set() )
            {
                if ( $kariabschnitt->get_kampfrichternummer()->get_zahl() === $kampfrichternummer->get_zahl() )
                {
                    array_push($found, $kariabschnitt);
                }
            }
        }

        return $found;
    }

    /**
     * @param Zahl $personennummer
     * @param bool $throw_on_error
     * @return PNMeldung|null
     * @throws Runtime_Exception
     */
    public function search_pnmeldung(Zahl $personennummer, $throw_on_error = true): ?PNMeldung
    {
        if ( !method_exists($this, 'get_pnmeldungen') )
        {
            throw new Runtime_Exception(
                __CLASS__ . ': ' . __FUNCTION__ . '() ' . get_class($this) . ' should have a "get_pnmeldungen" method!'
            );
        }

        if ( !$personennummer->is_set() )
        {
            throw new Runtime_Exception(__CLASS__ . ': ' . __FUNCTION__ . '() Given $personennummer was not set.');
        }

        foreach ( $this->get_pnmeldungen() as &$pnmeldung )
        {
            /** @var PNMeldung $pnmeldung */
            if ( $pnmeldung->get_personennummer()->is_set() )
            {
                if ( $pnmeldung->get_personennummer()->get_zahl() === $personennummer->get_zahl() )
                {
                    return $pnmeldung;
                }
            }
        }

        if ( $throw_on_error )
        {
            throw new Runtime_Exception(
                __CLASS__ . ': ' . __FUNCTION__ . '() ' .
                'Unable to find "PNMeldung" with $personennummer: ' . $personennummer->get_zahl()
            );
        }

        // Otherwise return null. Object could not be found.
        return null;
    }

    /**
     * @param Zahl $staffelnummer
     * @param bool $throw_on_error
     * @return STMeldung|null
     * @throws Runtime_Exception
     */
    public function search_stmeldung(Zahl $staffelnummer, $throw_on_error = true): ?STMeldung
    {
        if ( !method_exists($this, 'get_stmeldungen') )
        {
            throw new Runtime_Exception(
                __CLASS__ . ': ' . __FUNCTION__ . '() ' . get_class($this) . ' should have a "get_stmeldungen" method!'
            );
        }

        if ( !$staffelnummer->is_set() )
        {
            throw new Runtime_Exception(__CLASS__ . ': ' . __FUNCTION__ . '() Given $staffelnummer was not set.');
        }

        foreach ( $this->get_stmeldungen() as &$stmeldung )
        {
            /** @var STMeldung $stmeldung */
            if ( $stmeldung->get_staffelnummer()->is_set() )
            {
                if ( $stmeldung->get_staffelnummer()->get_zahl() === $staffelnummer->get_zahl() )
                {
                    return $stmeldung;
                }
            }
        }

        if ( $throw_on_error )
        {
            throw new Runtime_Exception(
                __CLASS__ . ': ' . __FUNCTION__ . '() ' .
                'Unable to find "STMeldung" with $staffelnummer: ' . $staffelnummer->get_zahl()
            );
        }

        // Otherwise return null. Object could not be found.
        return null;
    }

    /**
     * @param Zahl $staffelnummer
     * @param bool $throw_on_error
     * @return Staffel|null
     * @throws Runtime_Exception
     */
    public function search_staffel(Zahl $staffelnummer, $throw_on_error = true): ?Staffel
    {
        if ( !method_exists($this, 'get_staffeln') )
        {
            throw new Runtime_Exception(
                __CLASS__ . ': ' . __FUNCTION__ . '() ' . get_class($this) . ' should have a "get_staffeln" method!'
            );
        }

        if ( !$staffelnummer->is_set() )
        {
            throw new Runtime_Exception(__CLASS__ . ': ' . __FUNCTION__ . '() Given $staffelnummer was not set.');
        }

        foreach ( $this->get_staffeln() as &$staffel )
        {
            /** @var Staffel $stmeldung */
            if ( $staffel->get_staffelnummer()->is_set() )
            {
                if ( $staffel->get_staffelnummer()->get_zahl() === $staffelnummer->get_zahl() )
                {
                    return $staffel;
                }
            }
        }

        if ( $throw_on_error )
        {
            throw new Runtime_Exception(
                __CLASS__ . ': ' . __FUNCTION__ . '() ' .
                'Unable to find "Staffel" with $staffelnummer: ' . $staffelnummer->get_zahl()
            );
        }

        // Otherwise return null. Object could not be found.
        return null;
    }

    /**
     * @param Zahl $personennummer
     * @param bool $throw_on_error
     * @return Person|null
     * @throws Runtime_Exception
     */
    public function search_person(Zahl $personennummer, $throw_on_error = true): ?Person
    {
        if ( !method_exists($this, 'get_personen') )
        {
            throw new Runtime_Exception(
                __CLASS__ . ': ' . __FUNCTION__ . '() ' . get_class($this) . ' should have a "get_personen" method!'
            );
        }

        if ( !$personennummer->is_set() )
        {
            throw new Runtime_Exception(__CLASS__ . ': ' . __FUNCTION__ . '() Given $personennummer was not set.');
        }

        foreach ( $this->get_personen() as &$person )
        {
            /** @var Person $person */
            if ( $person->get_personennummer()->is_set() )
            {
                if ( $person->get_personennummer()->get_zahl() === $personennummer->get_zahl() )
                {
                    return $person;
                }
            }
        }

        if ( $throw_on_error )
        {
            throw new Runtime_Exception(
                __CLASS__ . ': ' . __FUNCTION__ . '() ' .
                'Unable to find "Person" with $personennummer: ' . $personennummer->get_zahl()
            );
        }

        // Otherwise return null. Object could not be found.
        return null;
    }

    /**
     * @param string $key
     * @throws Runtime_Exception
     */
    public function check_occurrences_key_exists(string $key): void
    {
        if ( !array_key_exists($key, $this->occurrences) )
        {
            throw new Runtime_Exception('Invalid index ($key) specified: "' . $key . '"');
        }
    }

    /**
     * @param string $element_name
     * @param int    $count
     * @throws Runtime_Exception
     */
    public function add_occurrence(string $element_name, int $count = 1): void
    {
        $this->check_occurrences_key_exists($element_name);
        $this->occurrences[$element_name] += $count;
    }

    /**
     * @param string $element_name
     * @param int    $count
     * @throws Runtime_Exception
     */
    public function remove_occurrence(string $element_name, int $count = 1): void
    {
        $this->add_occurrence($element_name, -$count);
    }

    /* SETTER / GETTER */

    /**
     * @param array $occurrence_constraints
     */
    public function set_occurrence_constraints(array $occurrence_constraints): void
    {
        $this->occurrence_constraints = $occurrence_constraints;
    }

    /**
     * @return array
     */
    public function get_occurrence_constraints(): array
    {
        return $this->occurrence_constraints;
    }

    /**
     * @param string $key
     * @return array
     * @throws Occurrence_Constraint_Exception
     */
    public function get_occurrence_constraint(string $key): array
    {
        if ( array_key_exists($key, $this->occurrence_constraints) )
        {
            return $this->occurrence_constraints[$key];
        }
        throw new Occurrence_Constraint_Exception('No occurrence constraint specified for: ' . $key);
    }

    /**
     * @param array $comments
     */
    public function set_comments(array $comments): void
    {
        $this->comments = $comments;
    }

    /**
     * @return array
     */
    public function get_comments(): array
    {
        return $this->comments;
    }

    /**
     * @param Comment $comment
     */
    public function add_comment(Comment $comment): void
    {
        array_push($this->comments, $comment);
    }

    public function clear_comments(): void
    {
        $this->comments = [];
    }

    /**
     * @param Format|null $format
     */
    public function set_format(?Format $format): void
    {
        $this->format = $format;
    }

    /**
     * @return Format|null
     */
    public function get_format(): ?Format
    {
        return $this->format;
    }

    /**
     * @param Dateiende $dateiende
     */
    public function set_dateiende(Dateiende $dateiende): void
    {
        $this->dateiende = $dateiende;
    }

    /**
     * @return Dateiende
     */
    public function get_dateiende(): Dateiende
    {
        return $this->dateiende;
    }

    /**
     * @param Writer $writer
     */
    public function set_writer(Writer $writer): void
    {
        $this->writer = $writer;
    }

    /**
     * @return Writer
     */
    public function get_writer(): Writer
    {
        return $this->writer;
    }

    /**
     * @return Performance_Info
     */
    public function get_performance_info(): Performance_Info
    {
        return $this->performance_info;
    }

}