<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Type;

use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Element\Abschnitt_Definition;
use Lukaspotthast\DSV\Document\Element\Ausrichter;
use Lukaspotthast\DSV\Document\Element\Ausschreibungimnetz;
use Lukaspotthast\DSV\Document\Element\Bankverbindung;
use Lukaspotthast\DSV\Document\Element\Besonderes;
use Lukaspotthast\DSV\Document\Element\Erzeuger;
use Lukaspotthast\DSV\Document\Element\Meldeadresse;
use Lukaspotthast\DSV\Document\Element\Meldegeld;
use Lukaspotthast\DSV\Document\Element\Meldeschluss;
use Lukaspotthast\DSV\Document\Element\Nachweis;
use Lukaspotthast\DSV\Document\Element\Pflichtzeit;
use Lukaspotthast\DSV\Document\Element\Veranstalter;
use Lukaspotthast\DSV\Document\Element\Veranstaltung;
use Lukaspotthast\DSV\Document\Element\Veranstaltungsort;
use Lukaspotthast\DSV\Document\Element\Wertung;
use Lukaspotthast\DSV\Document\Element\Wettkampf_Definition;
use Lukaspotthast\Support\Arg;
use Lukaspotthast\Support\Error\Exceptions\Typecheck_Exception;

/**
 * Class Wettkampfdefinitionsliste
 * @package Lukaspotthast\DSV\Document\Wettkampfdefinitionsliste
 */
class Wettkampfdefinitionsliste extends Document
{

    /** @var Erzeuger|null */
    private $erzeuger = null;

    /** @var Veranstaltung|null */
    private $veranstaltung = null;

    /** @var Veranstaltungsort|null */
    private $veranstaltungsort = null;

    /** @var Ausschreibungimnetz|null */
    private $ausschreibungimnetz = null;

    /** @var Veranstalter|null */
    private $veranstalter = null;

    /** @var Ausrichter|null */
    private $ausrichter = null;

    /** @var Meldeadresse|null */
    private $meldeadresse = null;

    /** @var Meldeschluss|null */
    private $meldeschluss = null;

    /** @var Bankverbindung|null */
    private $bankverbindung = null;

    /** @var Besonderes|null */
    private $besonderes = null;

    /** @var Nachweis|null */
    private $nachweis = null;

    /** @var array [Abschnitt] */
    private $abschnitte = [];

    /** @var array [Wettkampf] */
    private $wettkaempfe = [];

    /** @var array [Wertung] */
    private $wertungen = [];

    /** @var array [Pflichtzeit] */
    private $pflichtzeiten = [];

    /** @var array [Meldegeld] */
    private $meldegeld_typen = [];

    /**
     * @return array
     */
    public function create_occurrence_constraints(): array
    {
        return [
            Erzeuger::get_element_name()             => [1],
            Veranstaltung::get_element_name()        => [1],
            Veranstaltungsort::get_element_name()    => [1],
            Ausschreibungimnetz::get_element_name()  => [1],
            Veranstalter::get_element_name()         => [1],
            Ausrichter::get_element_name()           => [1],
            Meldeadresse::get_element_name()         => [1],
            Meldeschluss::get_element_name()         => [1],
            Bankverbindung::get_element_name()       => [0, 1],
            Besonderes::get_element_name()           => [0, 1],
            Nachweis::get_element_name()             => [0, 1],
            Abschnitt_Definition::get_element_name() => [1, '*'],
            Wettkampf_Definition::get_element_name() => [1, '*'],
            Wertung::get_element_name()              => [1, '*'],
            Pflichtzeit::get_element_name()          => [0, '*'],
            Meldegeld::get_element_name()            => [1, '*'],
        ];
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function get_listart(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @return array
     */
    public function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Erzeuger|null $erzeuger
     */
    public function set_erzeuger(?Erzeuger $erzeuger): void
    {
        $this->erzeuger = $erzeuger;
    }

    /**
     * @return Erzeuger|null
     */
    public function get_erzeuger(): ?Erzeuger
    {
        return $this->erzeuger;
    }

    /**
     * @param Veranstaltung|null $veranstaltung
     */
    public function set_veranstaltung(?Veranstaltung $veranstaltung): void
    {
        $this->veranstaltung = $veranstaltung;
    }

    /**
     * @return Veranstaltung|null
     */
    public function get_veranstaltung(): ?Veranstaltung
    {
        return $this->veranstaltung;
    }

    /**
     * @param Veranstaltungsort|null $veranstaltungsort
     */
    public function set_veranstaltungsort(?Veranstaltungsort $veranstaltungsort): void
    {
        $this->veranstaltungsort = $veranstaltungsort;
    }

    /**
     * @return Veranstaltungsort|null
     */
    public function get_veranstaltungsort(): ?Veranstaltungsort
    {
        return $this->veranstaltungsort;
    }

    /**
     * @param Ausschreibungimnetz|null $ausschreibungimnetz
     */
    public function set_ausschreibungimnetz(?Ausschreibungimnetz $ausschreibungimnetz): void
    {
        $this->ausschreibungimnetz = $ausschreibungimnetz;
    }

    /**
     * @return Ausschreibungimnetz|null
     */
    public function get_ausschreibungimnetz(): ?Ausschreibungimnetz
    {
        return $this->ausschreibungimnetz;
    }

    /**
     * @param Veranstalter|null $veranstalter
     */
    public function set_veranstalter(?Veranstalter $veranstalter): void
    {
        $this->veranstalter = $veranstalter;
    }

    /**
     * @return Veranstalter|null
     */
    public function get_veranstalter(): ?Veranstalter
    {
        return $this->veranstalter;
    }

    /**
     * @param Ausrichter|null $ausrichter
     */
    public function set_ausrichter(?Ausrichter $ausrichter): void
    {
        $this->ausrichter = $ausrichter;
    }

    /**
     * @return Ausrichter|null
     */
    public function get_ausrichter(): ?Ausrichter
    {
        return $this->ausrichter;
    }

    /**
     * @param Meldeadresse|null $meldeadresse
     */
    public function set_meldeadresse(?Meldeadresse $meldeadresse): void
    {
        $this->meldeadresse = $meldeadresse;
    }

    /**
     * @return Meldeadresse|null
     */
    public function get_meldeadresse(): ?Meldeadresse
    {
        return $this->meldeadresse;
    }

    /**
     * @param Meldeschluss|null $meldeschluss
     */
    public function set_meldeschluss(?Meldeschluss $meldeschluss): void
    {
        $this->meldeschluss = $meldeschluss;
    }

    /**
     * @return Meldeschluss|null
     */
    public function get_meldeschluss(): ?Meldeschluss
    {
        return $this->meldeschluss;
    }

    /**
     * @param Bankverbindung|null $bankverbindung
     */
    public function set_bankverbindung(?Bankverbindung $bankverbindung): void
    {
        $this->bankverbindung = $bankverbindung;
    }

    /**
     * @return Bankverbindung|null
     */
    public function get_bankverbindung(): ?Bankverbindung
    {
        return $this->bankverbindung;
    }

    /**
     * @param Besonderes|null $besonderes
     */
    public function set_besonderes(?Besonderes $besonderes): void
    {
        $this->besonderes = $besonderes;
    }

    /**
     * @return Besonderes|null
     */
    public function get_besonderes(): ?Besonderes
    {
        return $this->besonderes;
    }

    /**
     * @param Nachweis|null $nachweis
     */
    public function set_nachweis(?Nachweis $nachweis): void
    {
        $this->nachweis = $nachweis;
    }

    /**
     * @return Nachweis|null
     */
    public function get_nachweis(): ?Nachweis
    {
        return $this->nachweis;
    }

    /**
     * @param array $abschnitte
     * @throws Typecheck_Exception
     */
    public function set_abschnitte(array $abschnitte): void
    {
        Arg::typecheck($abschnitte, '[' . Abschnitt_Definition::class . ']');
        $this->abschnitte = $abschnitte;
    }

    /**
     * @return array
     */
    public function get_abschnitte(): array
    {
        return $this->abschnitte;
    }

    /**
     * @param Abschnitt_Definition $abschnitt
     */
    public function add_abschnitt(Abschnitt_Definition $abschnitt): void
    {
        array_push($this->abschnitte, $abschnitt);
    }

    /**
     *
     */
    public function clear_abschnitte(): void
    {
        $this->abschnitte = [];
    }

    /**
     * @param array $wettkaempfe
     * @throws Typecheck_Exception
     */
    public function set_wettkaempfe(array $wettkaempfe): void
    {
        Arg::typecheck($wettkaempfe, '[' . Wettkampf_Definition::class . ']');
        $this->wettkaempfe = $wettkaempfe;
    }

    /**
     * @return array
     */
    public function get_wettkaempfe(): array
    {
        return $this->wettkaempfe;
    }

    /**
     * @param Wettkampf_Definition $wettkampf
     */
    public function add_wettkampf(Wettkampf_Definition $wettkampf): void
    {
        array_push($this->wettkaempfe, $wettkampf);
    }

    /**
     *
     */
    public function clear_wettkaempfe(): void
    {
        $this->wettkaempfe = [];
    }

    /**
     * @param array $wertungen
     * @throws Typecheck_Exception
     */
    public function set_wertungen(array $wertungen): void
    {
        Arg::typecheck($wertungen, '[' . Wertung::class . ']');
        $this->wertungen = $wertungen;
    }

    /**
     * @return array
     */
    public function get_wertungen(): array
    {
        return $this->wertungen;
    }

    /**
     * @param Wertung $wertung
     */
    public function add_wertung(Wertung $wertung): void
    {
        array_push($this->wertungen, $wertung);
    }

    /**
     *
     */
    public function clear_wertungen(): void
    {
        $this->wertungen = [];
    }

    /**
     * @param array $pflichtzeiten
     * @throws Typecheck_Exception
     */
    public function set_pflichtzeiten(array $pflichtzeiten): void
    {
        Arg::typecheck($pflichtzeiten, '[' . Pflichtzeit::class . ']');
        $this->pflichtzeiten = $pflichtzeiten;
    }

    /**
     * @return array
     */
    public function get_pflichtzeiten(): array
    {
        return $this->pflichtzeiten;
    }

    /**
     * @param Pflichtzeit $pflichtzeit
     */
    public function add_pflichtzeit(Pflichtzeit $pflichtzeit): void
    {
        array_push($this->pflichtzeiten, $pflichtzeit);
    }

    /**
     *
     */
    public function clear_pflichtzeiten(): void
    {
        $this->pflichtzeiten = [];
    }

    /**
     * @param array $meldegeld_typen
     * @throws Typecheck_Exception
     */
    public function set_meldegeld_typen(array $meldegeld_typen): void
    {
        Arg::typecheck($meldegeld_typen, '[' . Meldegeld::class . ']');
        $this->meldegeld_typen = $meldegeld_typen;
    }

    /**
     * @return array
     */
    public function get_meldegeld_typen(): array
    {
        return $this->meldegeld_typen;
    }

    /**
     * OPTIONAL/ADDITIONAL WAY OF CALLING {@link add_meldegeld_typ}.
     * @param Meldegeld $meldegeld_typ
     */
    public function add_meldegeld(Meldegeld $meldegeld_typ): void
    {
        $this->add_meldegeld_typ($meldegeld_typ);
    }

    /**
     * @param Meldegeld $meldegeld_typ
     */
    public function add_meldegeld_typ(Meldegeld $meldegeld_typ): void
    {
        array_push($this->meldegeld_typen, $meldegeld_typ);
    }

    /**
     *
     */
    public function clear_meldegeld_typen(): void
    {
        $this->meldegeld_typen = [];
    }

}