<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Type;

use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Element\Abschnitt;
use Lukaspotthast\DSV\Document\Element\Ausrichter;
use Lukaspotthast\DSV\Document\Element\Erzeuger;
use Lukaspotthast\DSV\Document\Element\Kampfgericht;
use Lukaspotthast\DSV\Document\Element\PNErgebnis;
use Lukaspotthast\DSV\Document\Element\PNReaktion;
use Lukaspotthast\DSV\Document\Element\PNZwischenzeit;
use Lukaspotthast\DSV\Document\Element\STAbloese;
use Lukaspotthast\DSV\Document\Element\Staffelperson;
use Lukaspotthast\DSV\Document\Element\STErgebnis;
use Lukaspotthast\DSV\Document\Element\STZwischenzeit;
use Lukaspotthast\DSV\Document\Element\Veranstalter;
use Lukaspotthast\DSV\Document\Element\Veranstaltung;
use Lukaspotthast\DSV\Document\Element\Verein;
use Lukaspotthast\DSV\Document\Element\Wertung;
use Lukaspotthast\DSV\Document\Element\Wettkampf_Definition;
use Lukaspotthast\Support\Arg;
use Lukaspotthast\Support\Error\Exceptions\Typecheck_Exception;

/**
 * Class Wettkampfergebnisliste
 * @package Lukaspotthast\DSV\Document\Type
 */
class Wettkampfergebnisliste extends Document
{

    /** @var Erzeuger|null */
    private $erzeuger = null;

    /** @var Veranstaltung|null */
    private $veranstaltung = null;

    /** @var Veranstalter|null */
    private $veranstalter = null;

    /** @var Ausrichter|null */
    private $ausrichter = null;

    /** @var array [Abschnitt] */
    private $abschnitte = [];

    /** @var array [Kampfgericht] */
    private $kampfgericht = [];

    /** @var array [Wettkampf_Definition] */
    private $wettkaempfe = [];

    /** @var array [Wertung] */
    private $wertungen = [];

    /** @var array [Verein] */
    private $vereine = [];

    /** @var array [PNErgebnis] */
    private $pnergebnisse = [];

    /** @var array [PNZwischenzeit] */
    private $pnzwischenzeiten = [];

    /** @var array [PNReaktion] */
    private $pnreaktionen = [];

    /** @var array [STErgebnis] */
    private $stergebnisse = [];

    /** @var array [Staffelperson] */
    private $staffelpersonen = [];

    /** @var array [STZwischenzeit] */
    private $stzwischenzeiten = [];

    /** @var array [Stabloese] */
    private $stabloesen = [];

    /**
     * @return array
     */
    public function create_occurrence_constraints(): array
    {
        return [
            Erzeuger::get_element_name()             => [1],
            Veranstaltung::get_element_name()        => [1],
            Veranstalter::get_element_name()         => [1],
            Ausrichter::get_element_name()           => [1],
            Abschnitt::get_element_name()            => [1, '*'],
            Kampfgericht::get_element_name()         => [0, '*'],
            Wettkampf_Definition::get_element_name() => [1, '*'],
            Wertung::get_element_name()              => [1, '*'],
            Verein::get_element_name()               => [1, '*'],
            PNErgebnis::get_element_name()           => [0, '*'],
            PNZwischenzeit::get_element_name()       => [0, '*'],
            PNReaktion::get_element_name()           => [0, '*'],
            STErgebnis::get_element_name()           => [0, '*'],
            Staffelperson::get_element_name()        => [0, '*'],
            STZwischenzeit::get_element_name()       => [0, '*'],
            STAbloese::get_element_name()            => [0, '*'],
        ];
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function get_listart(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @return array
     */
    public function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Erzeuger|null $erzeuger
     */
    public function set_erzeuger(?Erzeuger $erzeuger): void
    {
        $this->erzeuger = $erzeuger;
    }

    /**
     * @return Erzeuger|null
     */
    public function get_erzeuger(): ?Erzeuger
    {
        return $this->erzeuger;
    }

    /**
     * @param Veranstaltung|null $veranstaltung
     */
    public function set_veranstaltung(?Veranstaltung $veranstaltung): void
    {
        $this->veranstaltung = $veranstaltung;
    }

    /**
     * @return Veranstaltung|null
     */
    public function get_veranstaltung(): ?Veranstaltung
    {
        return $this->veranstaltung;
    }

    /**
     * @param Veranstalter|null $veranstalter
     */
    public function set_veranstalter(?Veranstalter $veranstalter): void
    {
        $this->veranstalter = $veranstalter;
    }

    /**
     * @return Veranstalter|null
     */
    public function get_veranstalter(): ?Veranstalter
    {
        return $this->veranstalter;
    }

    /**
     * @param Ausrichter|null $ausrichter
     */
    public function set_ausrichter(?Ausrichter $ausrichter): void
    {
        $this->ausrichter = $ausrichter;
    }

    /**
     * @return Ausrichter|null
     */
    public function get_ausrichter(): ?Ausrichter
    {
        return $this->ausrichter;
    }

    /**
     * @param array $abschnitte
     */
    public function set_abschnitte(array $abschnitte): void
    {
        $this->abschnitte = $abschnitte;
    }

    /**
     * @return array
     */
    public function get_abschnitte(): array
    {
        return $this->abschnitte;
    }

    /**
     * @param Abschnitt $abschnitt
     */
    public function add_abschnitt(Abschnitt $abschnitt): void
    {
        array_push($this->abschnitte, $abschnitt);
    }

    public function clear_abschnitte(): void
    {
        $this->abschnitte = [];
    }

    /**
     * @param array $kampfgericht
     */
    public function set_kampfgericht(array $kampfgericht): void
    {
        Arg::typecheck($kampfgericht, '[' . Kampfgericht::class . ']');
        $this->kampfgericht = $kampfgericht;
    }

    /**
     * @return array
     */
    public function get_kampfgericht(): array
    {
        return $this->kampfgericht;
    }

    /**
     * @param Kampfgericht $kampfgericht
     */
    public function add_kampfgericht(Kampfgericht $kampfgericht): void
    {
        array_push($this->kampfgericht, $kampfgericht);
    }

    public function clear_kampfgericht(): void
    {
        $this->kampfgericht = [];
    }

    /**
     * @param array $wettkaempfe
     */
    public function set_wettkaempfe(array $wettkaempfe): void
    {
        Arg::typecheck($wettkaempfe, '[' . Wettkampf_Definition::class . ']');
        $this->wettkaempfe = $wettkaempfe;
    }

    /**
     * @return array
     */
    public function get_wettkaempfe(): array
    {
        return $this->wettkaempfe;
    }

    /**
     * @param Wettkampf_Definition $wettkampf
     */
    public function add_wettkampf(Wettkampf_Definition $wettkampf): void
    {
        array_push($this->wettkaempfe, $wettkampf);
    }

    public function clear_wettkaempfe(): void
    {
        $this->wettkaempfe = [];
    }

    /**
     * @param array $wertungen
     * @throws Typecheck_Exception
     */
    public function set_wertungen(array $wertungen): void
    {
        Arg::typecheck($wertungen, '[' . Wertung::class . ']');
        $this->wertungen = $wertungen;
    }

    /**
     * @return array
     */
    public function get_wertungen(): array
    {
        return $this->wertungen;
    }

    /**
     * @param Wertung $wertung
     */
    public function add_wertung(Wertung $wertung): void
    {
        array_push($this->wertungen, $wertung);
    }

    public function clear_wertungen(): void
    {
        $this->wertungen = [];
    }

    /**
     * @param array $vereine
     */
    public function set_vereine(array $vereine): void
    {
        $this->vereine = $vereine;
    }

    /**
     * @return array
     */
    public function get_vereine(): array
    {
        return $this->vereine;
    }

    /**
     * @param Verein $verein
     */
    public function add_verein(Verein $verein): void
    {
        array_push($this->vereine, $verein);
    }

    public function clear_vereine(): void
    {
        $this->vereine = [];
    }

    /**
     * @param array $pnergebnisse
     */
    public function set_pnergebnisse(array $pnergebnisse): void
    {
        $this->pnergebnisse = $pnergebnisse;
    }

    /**
     * @return array
     */
    public function get_pnergebnisse(): array
    {
        return $this->pnergebnisse;
    }

    /**
     * @param PNErgebnis $pnergebnis
     */
    public function add_pnergebnis(PNErgebnis $pnergebnis): void
    {
        array_push($this->pnergebnisse, $pnergebnis);
    }

    public function clear_pnergebnisse(): void
    {
        $this->pnergebnisse = [];
    }

    /**
     * @param array $pnzwischenzeiten
     */
    public function set_pnzwischenzeiten(array $pnzwischenzeiten): void
    {
        Arg::typecheck($pnzwischenzeiten, '[' . PNZwischenzeit::class . ']');
        $this->pnzwischenzeiten = $pnzwischenzeiten;
    }

    /**
     * @return array
     */
    public function get_pnzwischenzeiten(): array
    {
        return $this->pnzwischenzeiten;
    }

    /**
     * @param PNZwischenzeit $pnzwischenzeit
     */
    public function add_pnzwischenzeit(PNZwischenzeit $pnzwischenzeit): void
    {
        array_push($this->pnzwischenzeiten, $pnzwischenzeit);
    }

    public function clear_pnzwischenzeiten(): void
    {
        $this->pnzwischenzeiten = [];
    }

    /**
     * @param array $pnreaktionen
     */
    public function set_pnreaktionen(array $pnreaktionen): void
    {
        Arg::typecheck($pnreaktionen, '[' . PNReaktion::class . ']');
        $this->pnreaktionen = $pnreaktionen;
    }

    /**
     * @return array
     */
    public function get_pnreaktionen(): array
    {
        return $this->pnreaktionen;
    }

    /**
     * @param PNReaktion $pnreaktion
     */
    public function add_pnreaktion(PNReaktion $pnreaktion): void
    {
        array_push($this->pnreaktionen, $pnreaktion);
    }

    public function clear_pnreaktionen(): void
    {
        $this->pnreaktionen = [];
    }

    /**
     * @param array $stergebnisse
     */
    public function set_stergebnisse(array $stergebnisse): void
    {
        Arg::typecheck($stergebnisse, '[' . STErgebnis::class . ']');
        $this->stergebnisse = $stergebnisse;
    }

    /**
     * @return array
     */
    public function get_stergebnisse(): array
    {
        return $this->stergebnisse;
    }

    /**
     * @param STErgebnis $stergebnis
     */
    public function add_stergebnis(STErgebnis $stergebnis): void
    {
        array_push($this->stergebnisse, $stergebnis);
    }

    public function clear_stergebnisse(): void
    {
        $this->stergebnisse = [];
    }

    /**
     * @param array $staffelpersonen
     */
    public function set_staffelpersonen(array $staffelpersonen): void
    {
        Arg::typecheck($staffelpersonen, '[' . Staffelperson::class . ']');
        $this->staffelpersonen = $staffelpersonen;
    }

    /**
     * @return array
     */
    public function get_staffelpersonen(): array
    {
        return $this->staffelpersonen;
    }

    /**
     * @param Staffelperson $staffelperson
     */
    public function add_staffelperson(Staffelperson $staffelperson): void
    {
        array_push($this->staffelpersonen, $staffelperson);
    }

    public function clear_staffelpersonen(): void
    {
        $this->staffelpersonen = [];
    }

    /**
     * @param array $stzwischenzeiten
     */
    public function set_stzwischenzeiten(array $stzwischenzeiten): void
    {
        Arg::typecheck($stzwischenzeiten, '[' . STZwischenzeit::class . ']');
        $this->stzwischenzeiten = $stzwischenzeiten;
    }

    /**
     * @return array
     */
    public function get_stzwischenzeiten(): array
    {
        return $this->stzwischenzeiten;
    }

    /**
     * @param STZwischenzeit $stzwischenzeit
     */
    public function add_stzwischenzeit(STZwischenzeit $stzwischenzeit): void
    {
        array_push($this->stzwischenzeiten, $stzwischenzeit);
    }

    public function clear_stzwischenzeiten(): void
    {
        $this->stzwischenzeiten = [];
    }

    /**
     * @param array $stabloesen
     */
    public function set_stabloesen(array $stabloesen): void
    {
        Arg::typecheck($stabloesen, '[' . STAbloese::class . ']');
        $this->stabloesen = $stabloesen;
    }

    /**
     * @return array
     */
    public function get_stabloesen(): array
    {
        return $this->stabloesen;
    }

    /**
     * @param STAbloese $stabloese
     */
    public function add_stabloese(STAbloese $stabloese): void
    {
        array_push($this->stabloesen, $stabloese);
    }

    public function clear_stabloesen(): void
    {
        $this->stabloesen = [];
    }

}