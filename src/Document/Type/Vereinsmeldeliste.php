<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Type;

use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Element\Abschnitt;
use Lukaspotthast\DSV\Document\Element\Ansprechpartner;
use Lukaspotthast\DSV\Document\Element\Erzeuger;
use Lukaspotthast\DSV\Document\Element\Kariabschnitt;
use Lukaspotthast\DSV\Document\Element\Karimeldung;
use Lukaspotthast\DSV\Document\Element\PNMeldung;
use Lukaspotthast\DSV\Document\Element\StartPN;
use Lukaspotthast\DSV\Document\Element\StartST;
use Lukaspotthast\DSV\Document\Element\STMeldung;
use Lukaspotthast\DSV\Document\Element\STPerson;
use Lukaspotthast\DSV\Document\Element\Trainer;
use Lukaspotthast\DSV\Document\Element\Veranstaltung;
use Lukaspotthast\DSV\Document\Element\Verein;
use Lukaspotthast\DSV\Document\Element\Wettkampf;

/**
 * Class Vereinsmeldeliste
 * @package Lukaspotthast\DSV\Document\Type
 */
class Vereinsmeldeliste extends Document
{

    /** @var Erzeuger|null */
    private $erzeuger = null;

    /** @var Veranstaltung|null */
    private $veranstaltung = null;

    /** @var array [Abschnitt] */
    private $abschnitte = [];

    /** @var array [Wettkampf] */
    private $wettkaempfe = [];

    /** @var Verein|null */
    private $verein = null;

    /** @var Ansprechpartner|null */
    private $ansprechpartner = null;

    /** @var array [Karimeldung] */
    private $karimeldungen = [];

    /** @var array [Kariabschnitt] */
    private $kariabschnitte = [];

    /** @var array [Trainer] */
    private $trainer = [];

    /** @var array [PNMeldung] */
    private $pnmeldungen = [];

    /** @var array [StartPN] */
    private $startspn = [];

    /** @var array [STMeldung] */
    private $stmeldungen = [];

    /** @var array [StartST] */
    private $startsst = [];

    /** @var array [Staffelperson] */
    private $staffelpersonen = [];

    /**
     * @return array
     */
    public function create_occurrence_constraints(): array
    {
        return [
            Erzeuger::get_element_name()        => [1],
            Veranstaltung::get_element_name()   => [1],
            Abschnitt::get_element_name()       => [1, '*'],
            Wettkampf::get_element_name()       => [1, '*'],
            Verein::get_element_name()          => [1],
            Ansprechpartner::get_element_name() => [1],
            Karimeldung::get_element_name()     => [0, '*'],
            Kariabschnitt::get_element_name()   => [0, '*'],
            Trainer::get_element_name()         => [0, '*'],
            PNMeldung::get_element_name()       => [0, '*'],
            StartPN::get_element_name()         => [0, '*'],
            STMeldung::get_element_name()       => [0, '*'],
            StartST::get_element_name()         => [0, '*'],
            STPerson::get_element_name()        => [0, '*'],
        ];
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function get_listart(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @return array
     */
    public function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Erzeuger|null $erzeuger
     */
    public function set_erzeuger(?Erzeuger $erzeuger): void
    {
        $this->erzeuger = $erzeuger;
    }

    /**
     * @return Erzeuger|null
     */
    public function get_erzeuger(): ?Erzeuger
    {
        return $this->erzeuger;
    }

    /**
     * @param Veranstaltung|null $veranstaltung
     */
    public function set_veranstaltung(?Veranstaltung $veranstaltung): void
    {
        $this->veranstaltung = $veranstaltung;
    }

    /**
     * @return Veranstaltung|null
     */
    public function get_veranstaltung(): ?Veranstaltung
    {
        return $this->veranstaltung;
    }

    /**
     * @param array $abschnitte
     */
    public function set_abschnitte(array $abschnitte): void
    {
        $this->abschnitte = $abschnitte;
    }

    /**
     * @return array
     */
    public function get_abschnitte(): array
    {
        return $this->abschnitte;
    }

    /**
     * @param Abschnitt $abschnitt
     */
    public function add_abschnitt(Abschnitt $abschnitt): void
    {
        array_push($this->abschnitte, $abschnitt);
    }

    public function clear_abschnitte(): void
    {
        $this->abschnitte = [];
    }

    /**
     * @param array $wettkaempfe
     */
    public function set_wettkaempfe(array $wettkaempfe): void
    {
        $this->wettkaempfe = $wettkaempfe;
    }

    /**
     * @return array
     */
    public function get_wettkaempfe(): array
    {
        return $this->wettkaempfe;
    }

    /**
     * @param Wettkampf $wettkampf
     */
    public function add_wettkampf(Wettkampf $wettkampf): void
    {
        array_push($this->wettkaempfe, $wettkampf);
    }

    public function clear_wettkaempfe(): void
    {
        $this->wettkaempfe = [];
    }

    /**
     * @param Verein|null $verein
     */
    public function set_verein(?Verein $verein): void
    {
        $this->verein = $verein;
    }

    /**
     * @return Verein|null
     */
    public function get_verein(): ?Verein
    {
        return $this->verein;
    }

    /**
     * @param Ansprechpartner|null $ansprechpartner
     */
    public function set_ansprechpartner(?Ansprechpartner $ansprechpartner): void
    {
        $this->ansprechpartner = $ansprechpartner;
    }

    /**
     * @return Ansprechpartner|null
     */
    public function get_ansprechpartner(): ?Ansprechpartner
    {
        return $this->ansprechpartner;
    }

    /**
     * @param array $karimeldungen
     */
    public function set_karimeldungen(array $karimeldungen): void
    {
        $this->karimeldungen = $karimeldungen;
    }

    /**
     * @return array
     */
    public function get_karimeldungen(): array
    {
        return $this->karimeldungen;
    }

    /**
     * @param Karimeldung $karimeldung
     */
    public function add_karimeldung(Karimeldung $karimeldung): void
    {
        array_push($this->karimeldungen, $karimeldung);
    }

    public function clear_karimeldungen(): void
    {
        $this->karimeldungen = [];
    }

    /**
     * @param array $kariabschnitte
     */
    public function set_kariabschnitte(array $kariabschnitte): void
    {
        $this->kariabschnitte = $kariabschnitte;
    }

    /**
     * @return array
     */
    public function get_kariabschnitte(): array
    {
        return $this->kariabschnitte;
    }

    /**
     * @param Kariabschnitt $kariabschnitt
     */
    public function add_kariabschnitt(Kariabschnitt $kariabschnitt): void
    {
        array_push($this->kariabschnitte, $kariabschnitt);
    }

    public function clear_kariabschnitte(): void
    {
        $this->kariabschnitte = [];
    }

    /**
     * @param array $trainer
     */
    public function set_trainer(array $trainer): void
    {
        $this->trainer = $trainer;
    }

    /**
     * @return array
     */
    public function get_trainer(): array
    {
        return $this->trainer;
    }

    /**
     * @param Trainer $trainer
     */
    public function add_trainer(Trainer $trainer): void
    {
        array_push($this->trainer, $trainer);
    }

    public function clear_trainer(): void
    {
        $this->trainer = [];
    }

    /**
     * @param array $pnmeldungen
     */
    public function set_pnmeldungen(array $pnmeldungen): void
    {
        $this->pnmeldungen = $pnmeldungen;
    }

    /**
     * @return array
     */
    public function get_pnmeldungen(): array
    {
        return $this->pnmeldungen;
    }

    /**
     * @param PNMeldung $pnmeldung
     */
    public function add_pnmeldung(PNMeldung $pnmeldung): void
    {
        array_push($this->pnmeldungen, $pnmeldung);
    }

    public function clear_pnmeldungen(): void
    {
        $this->pnmeldungen = [];
    }

    /**
     * @param array $startspn
     */
    public function set_startspn(array $startspn): void
    {
        $this->startspn = $startspn;
    }

    /**
     * @return array
     */
    public function get_startspn(): array
    {
        return $this->startspn;
    }

    /**
     * @param StartPN $startpn
     */
    public function add_startpn(StartPN $startpn): void
    {
        array_push($this->startspn, $startpn);
    }

    public function clear_startspn(): void
    {
        $this->startspn = [];
    }

    /**
     * @param array $stmeldungen
     */
    public function set_stmeldungen(array $stmeldungen): void
    {
        $this->stmeldungen = $stmeldungen;
    }

    /**
     * @return array
     */
    public function get_stmeldungen(): array
    {
        return $this->stmeldungen;
    }

    /**
     * @param STMeldung $stmeldung
     */
    public function add_stmeldung(STMeldung $stmeldung): void
    {
        array_push($this->stmeldungen, $stmeldung);
    }

    public function clear_stmeldungen(): void
    {
        $this->stmeldungen = [];
    }

    /**
     * @param array $startsst
     */
    public function set_startsst(array $startsst): void
    {
        $this->startsst = $startsst;
    }

    /**
     * @return array
     */
    public function get_startsst(): array
    {
        return $this->startsst;
    }

    /**
     * @param StartST $startst
     */
    public function add_startst(StartST $startst): void
    {
        array_push($this->startsst, $startst);
    }

    public function clear_startsst(): void
    {
        $this->startsst = [];
    }

    /**
     * @param array $staffelpersonen
     */
    public function set_staffelpersonen(array $staffelpersonen): void
    {
        $this->staffelpersonen = $staffelpersonen;
    }

    /**
     * @return array
     */
    public function get_staffelpersonen(): array
    {
        return $this->staffelpersonen;
    }

    /**
     * @param STPerson $staffelperson
     */
    public function add_staffelperson(STPerson $staffelperson): void
    {
        array_push($this->staffelpersonen, $staffelperson);
    }

    public function clear_staffelpersonen(): void
    {
        $this->staffelpersonen = [];
    }

}