<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Type;

use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Element\Abschnitt;
use Lukaspotthast\DSV\Document\Element\Ausrichter;
use Lukaspotthast\DSV\Document\Element\Erzeuger;
use Lukaspotthast\DSV\Document\Element\Kampfgericht;
use Lukaspotthast\DSV\Document\Element\Person;
use Lukaspotthast\DSV\Document\Element\Personenergebnis;
use Lukaspotthast\DSV\Document\Element\PNReaktion;
use Lukaspotthast\DSV\Document\Element\PNZwischenzeit;
use Lukaspotthast\DSV\Document\Element\STAbloese;
use Lukaspotthast\DSV\Document\Element\Staffel;
use Lukaspotthast\DSV\Document\Element\Staffelergebnis;
use Lukaspotthast\DSV\Document\Element\Staffelperson;
use Lukaspotthast\DSV\Document\Element\STZwischenzeit;
use Lukaspotthast\DSV\Document\Element\Veranstalter;
use Lukaspotthast\DSV\Document\Element\Veranstaltung;
use Lukaspotthast\DSV\Document\Element\Verein;
use Lukaspotthast\DSV\Document\Element\Wertung;
use Lukaspotthast\DSV\Document\Element\Wettkampf_Definition;
use Lukaspotthast\Support\Arg;
use Lukaspotthast\Support\Error\Exceptions\Typecheck_Exception;

/**
 * Class Vereinsergebnisliste
 * @package Lukaspotthast\DSV\Document\Type
 */
class Vereinsergebnisliste extends Document
{

    /** @var Erzeuger|null */
    private $erzeuger = null;

    /** @var Veranstaltung|null */
    private $veranstaltung = null;

    /** @var Veranstalter|null */
    private $veranstalter = null;

    /** @var Ausrichter|null */
    private $ausrichter = null;

    /** @var array [Abschnitt] */
    private $abschnitte = [];

    /** @var array [Kampfgericht] */
    private $kampfgericht = [];

    /** @var array [Wettkampf_Definition] */
    private $wettkaempfe = [];

    /** @var array [Wertung] */
    private $wertungen = [];

    /** @var Verein|null */
    private $verein = null;

    /** @var array [Person] */
    private $personen = [];

    /** @var array [Personenergebnis] */
    private $personenergebnisse = [];

    /** @var array [PNZwischenzeit] */
    private $pnzwischenzeiten = [];

    /** @var array [PNReaktion] */
    private $pnreaktionen = [];

    /** @var array [Staffel] */
    private $staffeln = [];

    /** @var array [Staffelperson] */
    private $staffelpersonen = [];

    /** @var array [Staffelergebnis] */
    private $staffelergebnisse = [];

    /** @var array [STZwischenzeit] */
    private $stzwischenzeiten = [];

    /** @var array [Stabloese] */
    private $stabloesen = [];

    /**
     * @return array
     */
    public function create_occurrence_constraints(): array
    {
        return [
            Erzeuger::get_element_name()             => [1],
            Veranstaltung::get_element_name()        => [1],
            Veranstalter::get_element_name()         => [1],
            Ausrichter::get_element_name()           => [1],
            Abschnitt::get_element_name()            => [1, '*'],
            Kampfgericht::get_element_name()         => [0, '*'],
            Wettkampf_Definition::get_element_name() => [1, '*'],
            Wertung::get_element_name()              => [1, '*'],
            Verein::get_element_name()               => [1],
            Person::get_element_name()               => [0, '*'],
            Personenergebnis::get_element_name()     => [0, '*'],
            PNZwischenzeit::get_element_name()       => [0, '*'],
            PNReaktion::get_element_name()           => [0, '*'],
            Staffel::get_element_name()              => [0, '*'],
            Staffelperson::get_element_name()        => [0, '*'],
            Staffelergebnis::get_element_name()      => [0, '*'],
            STZwischenzeit::get_element_name()       => [0, '*'],
            STAbloese::get_element_name()            => [0, '*'],
        ];
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function get_listart(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @return array
     */
    public function get_attributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param Erzeuger|null $erzeuger
     */
    public function set_erzeuger(?Erzeuger $erzeuger): void
    {
        $this->erzeuger = $erzeuger;
    }

    /**
     * @return Erzeuger|null
     */
    public function get_erzeuger(): ?Erzeuger
    {
        return $this->erzeuger;
    }

    /**
     * @param Veranstaltung|null $veranstaltung
     */
    public function set_veranstaltung(?Veranstaltung $veranstaltung): void
    {
        $this->veranstaltung = $veranstaltung;
    }

    /**
     * @return Veranstaltung|null
     */
    public function get_veranstaltung(): ?Veranstaltung
    {
        return $this->veranstaltung;
    }

    /**
     * @param Veranstalter|null $veranstalter
     */
    public function set_veranstalter(?Veranstalter $veranstalter): void
    {
        $this->veranstalter = $veranstalter;
    }

    /**
     * @return Veranstalter|null
     */
    public function get_veranstalter(): ?Veranstalter
    {
        return $this->veranstalter;
    }

    /**
     * @param Ausrichter|null $ausrichter
     */
    public function set_ausrichter(?Ausrichter $ausrichter): void
    {
        $this->ausrichter = $ausrichter;
    }

    /**
     * @return Ausrichter|null
     */
    public function get_ausrichter(): ?Ausrichter
    {
        return $this->ausrichter;
    }

    /**
     * @param array $abschnitte
     */
    public function set_abschnitte(array $abschnitte): void
    {
        $this->abschnitte = $abschnitte;
    }

    /**
     * @return array
     */
    public function get_abschnitte(): array
    {
        return $this->abschnitte;
    }

    /**
     * @param Abschnitt $abschnitt
     */
    public function add_abschnitt(Abschnitt $abschnitt): void
    {
        array_push($this->abschnitte, $abschnitt);
    }

    public function clear_abschnitte(): void
    {
        $this->abschnitte = [];
    }

    /**
     * @param array $kampfgericht
     */
    public function set_kampfgericht(array $kampfgericht): void
    {
        Arg::typecheck($kampfgericht, '[' . Kampfgericht::class . ']');
        $this->kampfgericht = $kampfgericht;
    }

    /**
     * @return array
     */
    public function get_kampfgericht(): array
    {
        return $this->kampfgericht;
    }

    /**
     * @param Kampfgericht $kampfgericht
     */
    public function add_kampfgericht(Kampfgericht $kampfgericht): void
    {
        array_push($this->kampfgericht, $kampfgericht);
    }

    public function clear_kampfgericht(): void
    {
        $this->kampfgericht = [];
    }

    /**
     * @param array $wettkaempfe
     */
    public function set_wettkaempfe(array $wettkaempfe): void
    {
        Arg::typecheck($wettkaempfe, '[' . Wettkampf_Definition::class . ']');
        $this->wettkaempfe = $wettkaempfe;
    }

    /**
     * @return array
     */
    public function get_wettkaempfe(): array
    {
        return $this->wettkaempfe;
    }

    /**
     * @param Wettkampf_Definition $wettkampf
     */
    public function add_wettkampf(Wettkampf_Definition $wettkampf): void
    {
        array_push($this->wettkaempfe, $wettkampf);
    }

    public function clear_wettkaempfe(): void
    {
        $this->wettkaempfe = [];
    }

    /**
     * @param array $wertungen
     * @throws Typecheck_Exception
     */
    public function set_wertungen(array $wertungen): void
    {
        Arg::typecheck($wertungen, '[' . Wertung::class . ']');
        $this->wertungen = $wertungen;
    }

    /**
     * @return array
     */
    public function get_wertungen(): array
    {
        return $this->wertungen;
    }

    /**
     * @param Wertung $wertung
     */
    public function add_wertung(Wertung $wertung): void
    {
        array_push($this->wertungen, $wertung);
    }

    public function clear_wertungen(): void
    {
        $this->wertungen = [];
    }

    /**
     * @param Verein|null $verein
     */
    public function set_verein(?Verein $verein): void
    {
        $this->verein = $verein;
    }

    /**
     * @return Verein|null
     */
    public function get_verein(): ?Verein
    {
        return $this->verein;
    }

    /**
     * @param array $personen
     */
    public function set_personen(array $personen): void
    {
        Arg::typecheck($personen, '[' . Person::class . ']');
        $this->personen = $personen;
    }

    /**
     * @return array
     */
    public function get_personen(): array
    {
        return $this->personen;
    }

    /**
     * @param Person $person
     */
    public function add_person(Person $person): void
    {
        array_push($this->personen, $person);
    }

    public function clear_personen(): void
    {
        $this->personen = [];
    }

    /**
     * @param array $personenergebnisse
     */
    public function set_personenergebnisse(array $personenergebnisse): void
    {
        Arg::typecheck($personenergebnisse, '[' . Personenergebnis::class . ']');
        $this->personenergebnisse = $personenergebnisse;
    }

    /**
     * @return array
     */
    public function get_personenergebnisse(): array
    {
        return $this->personenergebnisse;
    }

    /**
     * @param Personenergebnis $personenergebnis
     */
    public function add_personenergebnis(Personenergebnis $personenergebnis): void
    {
        array_push($this->personenergebnisse, $personenergebnis);
    }

    public function clear_personenergebnisse(): void
    {
        $this->personenergebnisse = [];
    }

    /**
     * @param array $pnzwischenzeiten
     */
    public function set_pnzwischenzeiten(array $pnzwischenzeiten): void
    {
        Arg::typecheck($pnzwischenzeiten, '[' . PNZwischenzeit::class . ']');
        $this->pnzwischenzeiten = $pnzwischenzeiten;
    }

    /**
     * @return array
     */
    public function get_pnzwischenzeiten(): array
    {
        return $this->pnzwischenzeiten;
    }

    /**
     * @param PNZwischenzeit $pnzwischenzeit
     */
    public function add_pnzwischenzeit(PNZwischenzeit $pnzwischenzeit): void
    {
        array_push($this->pnzwischenzeiten, $pnzwischenzeit);
    }

    public function clear_pnzwischenzeiten(): void
    {
        $this->pnzwischenzeiten = [];
    }

    /**
     * @param array $pnreaktionen
     */
    public function set_pnreaktionen(array $pnreaktionen): void
    {
        Arg::typecheck($pnreaktionen, '[' . PNReaktion::class . ']');
        $this->pnreaktionen = $pnreaktionen;
    }

    /**
     * @return array
     */
    public function get_pnreaktionen(): array
    {
        return $this->pnreaktionen;
    }

    /**
     * @param PNReaktion $pnreaktion
     */
    public function add_pnreaktion(PNReaktion $pnreaktion): void
    {
        array_push($this->pnreaktionen, $pnreaktion);
    }

    public function clear_pnreaktionen(): void
    {
        $this->pnreaktionen = [];
    }

    /**
     * @return array
     */
    public function get_staffeln(): array
    {
        return $this->staffeln;
    }

    /**
     * @param array $staffeln
     */
    public function set_staffeln(array $staffeln): void
    {
        $this->staffeln = $staffeln;
    }

    /**
     * @param Staffel $staffel
     */
    public function add_staffel(Staffel $staffel): void
    {
        array_push($this->staffeln, $staffel);
    }

    public function clear_staffeln(): void
    {
        $this->staffeln = [];
    }

    /**
     * @param array $staffelpersonen
     */
    public function set_staffelpersonen(array $staffelpersonen): void
    {
        Arg::typecheck($staffelpersonen, '[' . Staffelperson::class . ']');
        $this->staffelpersonen = $staffelpersonen;
    }

    /**
     * @return array
     */
    public function get_staffelpersonen(): array
    {
        return $this->staffelpersonen;
    }

    /**
     * @param Staffelperson $staffelperson
     */
    public function add_staffelperson(Staffelperson $staffelperson): void
    {
        array_push($this->staffelpersonen, $staffelperson);
    }

    public function clear_staffelpersonen(): void
    {
        $this->staffelpersonen = [];
    }

    /**
     * @param array $staffelergebnisse
     */
    public function set_staffelergebnisse(array $staffelergebnisse): void
    {
        Arg::typecheck($staffelergebnisse, '[' . Staffelergebnis::class . ']');
        $this->staffelergebnisse = $staffelergebnisse;
    }

    /**
     * @return array
     */
    public function get_staffelergebnisse(): array
    {
        return $this->staffelergebnisse;
    }

    /**
     * @param Staffelergebnis $staffelergebnis
     */
    public function add_staffelergebnis(Staffelergebnis $staffelergebnis): void
    {
        array_push($this->staffelergebnisse, $staffelergebnis);
    }

    public function clear_staffelergebnisse(): void
    {
        $this->staffelergebnisse = [];
    }

    /**
     * @param array $stzwischenzeiten
     */
    public function set_stzwischenzeiten(array $stzwischenzeiten): void
    {
        Arg::typecheck($stzwischenzeiten, '[' . STZwischenzeit::class . ']');
        $this->stzwischenzeiten = $stzwischenzeiten;
    }

    /**
     * @return array
     */
    public function get_stzwischenzeiten(): array
    {
        return $this->stzwischenzeiten;
    }

    /**
     * @param STZwischenzeit $stzwischenzeit
     */
    public function add_stzwischenzeit(STZwischenzeit $stzwischenzeit): void
    {
        array_push($this->stzwischenzeiten, $stzwischenzeit);
    }

    public function clear_stzwischenzeiten(): void
    {
        $this->stzwischenzeiten = [];
    }

    /**
     * @param array $stabloesen
     */
    public function set_stabloesen(array $stabloesen): void
    {
        Arg::typecheck($stabloesen, '[' . STAbloese::class . ']');
        $this->stabloesen = $stabloesen;
    }

    /**
     * @return array
     */
    public function get_stabloesen(): array
    {
        return $this->stabloesen;
    }

    /**
     * @param STAbloese $stabloese
     */
    public function add_stabloese(STAbloese $stabloese): void
    {
        array_push($this->stabloesen, $stabloese);
    }

    public function clear_stabloesen(): void
    {
        $this->stabloesen = [];
    }

}