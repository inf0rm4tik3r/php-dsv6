<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Writer;

use Lukaspotthast\DSV\Data\Datum;
use Lukaspotthast\DSV\Data\Geldbetrag;
use Lukaspotthast\DSV\Data\JGAK;
use Lukaspotthast\DSV\Data\Uhrzeit;
use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichen;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Data\Zeit;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Comment;
use Lukaspotthast\DSV\DSV;

/**
 * Class Text_Writer
 * @package Lukaspotthast\DSV\Document\Writer
 */
class Text_Writer extends Writer
{

    /**
     * @return string
     */
    public function write_blank_line(): string
    {
        return PHP_EOL;
    }

    /**
     * @param Document $document
     * @return string
     */
    public function start_document(Document $document): string
    {
        return '';
    }

    /**
     * @param Document $document
     * @return string
     */
    public function end_document(Document $document): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function start_comment(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function end_comment(): string
    {
        return $this->write_blank_line();
    }

    /**
     * @return string
     */
    public function start_stmt(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function end_stmt(): string
    {
        return $this->write_blank_line();
    }

    /**
     * @return string
     */
    public function start_attrib_list(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function end_attrib_list(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function write_attrib_separator(): string
    {
        return DSV::STMT_ATTRIB_DELIMITER;
    }

    /**
     * @param string $s
     * @param bool   $include_completion_sign
     * @return string
     */
    public function write_element_name(string $s, bool $include_completion_sign = true): string
    {
        if ( $include_completion_sign )
        {
            return $s . DSV::STMT_ELEMENT_NAME_COMPLETION_SIGN;
        }
        else
        {
            return $s;
        }
    }

    /**
     * @param Zeichenkette $zk
     * @param string       $name
     * @return string
     */
    public function write_string(Zeichenkette $zk, string $name = ''): string
    {
        // Remove all CR and LF occurrences.
        $s = preg_replace('/[\r\n]+/', '', $zk->get_formatted());

        // Remove all ';' occurrences.
        return str_replace(';', ':', $s);
    }

    /**
     * @param Zeichen $z
     * @param string  $name
     * @return string
     */
    public function write_char(Zeichen $z, string $name = ''): string
    {
        return $z->get_formatted();
    }

    /**
     * @param Zahl   $n
     * @param string $name
     * @return string
     */
    public function write_number(Zahl $n, string $name = ''): string
    {
        return $n->get_formatted();
    }

    /**
     * @param Datum  $d
     * @param string $name
     * @return string
     */
    public function write_date(Datum $d, string $name = ''): string
    {
        return $d->get_formatted();
    }

    /**
     * @param Uhrzeit $u
     * @param string  $name
     * @return string
     */
    public function write_time(Uhrzeit $u, string $name = ''): string
    {
        return $u->get_formatted();
    }

    /**
     * @param Zeit   $z
     * @param string $name
     * @return string
     */
    public function write_duration(Zeit $z, string $name = ''): string
    {
        return $z->get_formatted();
    }

    /**
     * @param Geldbetrag $b
     * @param string     $name
     * @return string
     */
    public function write_money(Geldbetrag $b, string $name = ''): string
    {
        return $b->get_formatted();
    }

    /**
     * @param JGAK   $jgak
     * @param string $name
     * @return string
     */
    public function write_jgak(JGAK $jgak, string $name = ''): string
    {
        return $jgak->get_formatted();
    }

    /**
     * @param Comment $c
     * @return string
     */
    public function write_comment(Comment $c): string
    {
        return DSV::COMMENT_START . ' ' . $c->get_comment() . ' ' . DSV::COMMENT_END;
    }

}