<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Writer;

use Lukaspotthast\DSV\Data\Datum;
use Lukaspotthast\DSV\Data\Geldbetrag;
use Lukaspotthast\DSV\Data\JGAK;
use Lukaspotthast\DSV\Data\Uhrzeit;
use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichen;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Data\Zeit;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Comment;

/**
 * Class JSON_Writer
 * @package Lukaspotthast\DSV\Document\Writer
 */
class JSON_Writer extends Text_Writer
{

    /** @var array */
    private $data;

    /** @var string */
    private $current;

    /** @var array */
    private $attrib_list;

    /**
     * @return string
     */
    public function write_blank_line(): string
    {
        return '';
    }

    /**
     * @param Document $document
     * @return string
     */
    public function start_document(Document $document): string
    {
        $this->data = [];
        return '';
    }

    /**
     * @param Document $document
     * @return string
     */
    public function end_document(Document $document): string
    {
        return json_encode($this->data);
    }

    /**
     * @return string
     */
    public function start_comment(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function end_comment(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function start_stmt(): string
    {
        $this->current = [];
        return '';
    }

    /**
     * @return string
     */
    public function end_stmt(): string
    {
        array_push($this->data, [$this->current => $this->attrib_list]);
        return '';
    }

    /**
     * @return string
     */
    public function start_attrib_list(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function end_attrib_list(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function write_attrib_separator(): string
    {
        return '';
    }

    /**
     * @param string $s
     * @param bool   $include_completion_sign
     * @return string
     */
    public function write_element_name(string $s, bool $include_completion_sign = true): string
    {
        $this->current     = $s;
        $this->attrib_list = [];
        return '';
    }

    /**
     * @param Zeichenkette $zk
     * @param string       $name
     * @return string
     */
    public function write_string(Zeichenkette $zk, string $name = ''): string
    {
        array_push($this->attrib_list, parent::write_string($zk));
        return '';
    }

    /**
     * @param Zeichen $z
     * @param string  $name
     * @return string
     */
    public function write_char(Zeichen $z, string $name = ''): string
    {
        array_push($this->attrib_list, parent::write_char($z));
        return '';
    }

    /**
     * @param Zahl   $n
     * @param string $name
     * @return string
     */
    public function write_number(Zahl $n, string $name = ''): string
    {
        array_push($this->attrib_list, parent::write_number($n));
        return '';
    }

    /**
     * @param Datum  $d
     * @param string $name
     * @return string
     */
    public function write_date(Datum $d, string $name = ''): string
    {
        array_push($this->attrib_list, parent::write_date($d));
        return '';
    }

    /**
     * @param Uhrzeit $u
     * @param string  $name
     * @return string
     */
    public function write_time(Uhrzeit $u, string $name = ''): string
    {
        array_push($this->attrib_list, parent::write_time($u));
        return '';
    }

    /**
     * @param Zeit   $z
     * @param string $name
     * @return string
     */
    public function write_duration(Zeit $z, string $name = ''): string
    {
        array_push($this->attrib_list, parent::write_duration($z));
        return '';
    }

    /**
     * @param Geldbetrag $b
     * @param string     $name
     * @return string
     */
    public function write_money(Geldbetrag $b, string $name = ''): string
    {
        array_push($this->attrib_list, parent::write_money($b));
        return '';
    }

    /**
     * @param JGAK   $jgak
     * @param string $name
     * @return string
     */
    public function write_jgak(JGAK $jgak, string $name = ''): string
    {
        array_push($this->attrib_list, parent::write_jgak($jgak));
        return '';
    }

    /**
     * @param Comment $c
     * @return string
     */
    public function write_comment(Comment $c): string
    {
        return '';
    }

}