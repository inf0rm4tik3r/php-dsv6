<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Writer;

use Lukaspotthast\DSV\Data\Datum;
use Lukaspotthast\DSV\Data\Geldbetrag;
use Lukaspotthast\DSV\Data\JGAK;
use Lukaspotthast\DSV\Data\Uhrzeit;
use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichen;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Data\Zeit;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Comment;
use Lukaspotthast\Support\Timing\Timer;

/**
 * Class Writer
 * @package Lukaspotthast\DSV\Document\Writer
 */
abstract class Writer
{

    /** @var Timer */
    private $timer;

    /**
     * Writer constructor.
     */
    public function __construct()
    {
        $this->timer = new Timer();
    }

    /**
     * @return string
     */
    public abstract function write_blank_line(): string;

    /**
     * @param Document $document
     * @return string
     */
    public abstract function start_document(Document $document): string;

    /**
     * @param Document $document
     * @return string
     */
    public abstract function end_document(Document $document): string;

    /**
     * @return string
     */
    public abstract function start_comment(): string;

    /**
     * @return string
     */
    public abstract function end_comment(): string;

    /**
     * @return string
     */
    public abstract function start_stmt(): string;

    /**
     * @return string
     */
    public abstract function end_stmt(): string;

    /**
     * @return string
     */
    public abstract function start_attrib_list(): string;

    /**
     * @return string
     */
    public abstract function end_attrib_list(): string;

    /**
     * @return string
     */
    public abstract function write_attrib_separator(): string;

    /**
     * @param string $s
     * @param bool   $include_completion_sign
     * @return string
     */
    public abstract function write_element_name(string $s, bool $include_completion_sign = true): string;

    /**
     * @param Zeichenkette $zk
     * @param string       $name
     * @return string
     */
    public abstract function write_string(Zeichenkette $zk, string $name = ''): string;

    /**
     * @param Zeichen $z
     * @param string  $name
     * @return string
     */
    public abstract function write_char(Zeichen $z, string $name = ''): string;

    /**
     * @param Zahl   $n
     * @param string $name
     * @return string
     */
    public abstract function write_number(Zahl $n, string $name = ''): string;

    /**
     * @param Datum  $d
     * @param string $name
     * @return string
     */
    public abstract function write_date(Datum $d, string $name = ''): string;

    /**
     * @param Uhrzeit $u
     * @param string  $name
     * @return string
     */
    public abstract function write_time(Uhrzeit $u, string $name = ''): string;

    /**
     * @param Zeit   $z
     * @param string $name
     * @return string
     */
    public abstract function write_duration(Zeit $z, string $name = ''): string;

    /**
     * @param Geldbetrag $b
     * @param string     $name
     * @return string
     */
    public abstract function write_money(Geldbetrag $b, string $name = ''): string;

    /**
     * @param JGAK   $jgak
     * @param string $name
     * @return string
     */
    public abstract function write_jgak(JGAK $jgak, string $name = ''): string;

    /**
     * @param Comment $c
     * @return string
     */
    public abstract function write_comment(Comment $c): string;

    /**
     * @return Timer
     */
    public function get_timer(): Timer
    {
        return $this->timer;
    }

}