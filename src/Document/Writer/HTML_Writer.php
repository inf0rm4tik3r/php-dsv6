<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Writer;

use Lukaspotthast\DSV\Data\Datum;
use Lukaspotthast\DSV\Data\Geldbetrag;
use Lukaspotthast\DSV\Data\JGAK;
use Lukaspotthast\DSV\Data\Uhrzeit;
use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichen;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Data\Zeit;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Comment;
use Lukaspotthast\DSV\DSV;

/**
 * Class HTML_Writer
 * @package Lukaspotthast\DSV\Document\Writer
 */
class HTML_Writer extends Text_Writer
{

    /** @var float */
    private $write_time;

    /**
     * @return string
     */
    public function write_blank_line(): string
    {
        return '<div class="dsv-blank-line"></div>';
    }

    /**
     * @param Document $document
     * @return string
     */
    public function start_document(Document $document): string
    {
        $this->get_timer()->start();

        return
            '<div class="dsv-document">' .
            $this->write_header($document) .
            '<div class="dsv-document-body">';
    }

    /**
     * @param float $val
     * @param int   $precision
     * @return string
     */
    private function str_round(float $val, int $precision = 2): string
    {
        return strval(round($val, $precision));
    }

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * @param Document $document
     * @return string
     */
    public function end_document(Document $document): string
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->write_time = $this->get_timer()->stop();

        return
            '</div>' . // closes document-body
            $this->write_footer($document) .
            '</div>';  // closes document
    }

    /**
     * @param Document $document
     * @return string
     */
    public function write_header(Document $document): string
    {
        return
            '<div class="dsv-document-header">' .
            DSV::ERZEUGER_SOFTWARE . ' ' . DSV::ERZEUGER_VERSION . ' by ' . DSV::ERZEUGER_AUTOR .
            '</div>';
    }

    /**
     * @param Document $document
     * @return string
     */
    public function write_footer(Document $document): string
    {
        $s = '<div class="dsv-document-footer">';
        $s .= '<div class="dsv-performance-info">';

        $s .= '<div> Read time: ' . $this->str_round($document->get_performance_info()->get_read_time(), 2) . ' ms</div>';
        $s .= '<div> Parse time: ' . $this->str_round($document->get_performance_info()->get_parse_time(), 2) . ' ms</div>';
        $s .= '<div> Create time: ' . $this->str_round($document->get_performance_info()->get_create_time(), 2) . ' ms</div>';
        $s .= '<div> Write time: ' . $this->str_round($this->write_time, 2) . ' ms</div>';
        $s .= '<div> Document memory footprint: ' . $this->str_round($document->get_performance_info()->get_memory_footprint() / 1024, 2) . ' kb</div>';

        $s .= '</div>';
        $s .= '</div>';

        return $s;
    }

    /**
     * @return string
     */
    public function start_comment(): string
    {
        return '<div class="dsv-comment">';
    }

    /**
     * @return string
     */
    public function end_comment(): string
    {
        return '</div>';
    }

    /**
     * @return string
     */
    public function start_stmt(): string
    {
        return '<div class="dsv-statement">';
    }

    /**
     * @return string
     */
    public function end_stmt(): string
    {
        return '</div>';
    }

    /**
     * @return string
     */
    public function start_attrib_list(): string
    {
        return '<span class="dsv-attrib-list">';
    }

    /**
     * @return string
     */
    public function end_attrib_list(): string
    {
        return '</span>';
    }

    /**
     * @return string
     */
    public function write_attrib_separator(): string
    {
        return '<span class="dsv-syntax-elem dsv-attrib-separator">' . parent::write_attrib_separator() . '</span>';
    }

    /**
     * @param string $s
     * @param bool   $include_completion_sign
     * @return string
     */
    public function write_element_name(string $s, bool $include_completion_sign = true): string
    {
        $s = '<span class="dsv-element-name">' . $s . '</span>';

        if ( $include_completion_sign )
        {
            $s .= '<span class="dsv-syntax-elem dsv-element-completion">' .
                  DSV::STMT_ELEMENT_NAME_COMPLETION_SIGN .
                  '</span>';
        }

        return $s;
    }

    /**
     * @param Zeichenkette $z
     * @param string       $name
     * @return string
     */
    public function write_string(Zeichenkette $z, string $name = ''): string
    {
        return
            '<span class="dsv-attrib-name">' . $name . '</span>' .
            '<span class="dsv-val dsv-string-val">' . parent::write_string($z) . '</span>';
    }

    /**
     * @param Zeichen $z
     * @param string  $name
     * @return string
     */
    public function write_char(Zeichen $z, string $name = ''): string
    {
        return
            '<span class="dsv-attrib-name">' . $name . '</span>' .
            '<span class="dsv-val dsv-char-val">' . parent::write_char($z) . '</span>';
    }

    /**
     * @param Zahl   $n
     * @param string $name
     * @return string
     */
    public function write_number(Zahl $n, string $name = ''): string
    {
        return
            '<span class="dsv-attrib-name">' . $name . '</span>' .
            '<span class="dsv-val dsv-number-val">' . parent::write_number($n) . '</span>';
    }

    /**
     * @param Datum  $d
     * @param string $name
     * @return string
     */
    public function write_date(Datum $d, string $name = ''): string
    {
        return
            '<span class="dsv-attrib-name">' . $name . '</span>' .
            '<span class="dsv-val dsv-date-val">' . parent::write_date($d) . '</span>';
    }

    /**
     * @param Uhrzeit $u
     * @param string  $name
     * @return string
     */
    public function write_time(Uhrzeit $u, string $name = ''): string
    {
        return
            '<span class="dsv-attrib-name">' . $name . '</span>' .
            '<span class="dsv-val dsv-time-val">' . parent::write_time($u) . '</span>';
    }

    /**
     * @param Zeit   $z
     * @param string $name
     * @return string
     */
    public function write_duration(Zeit $z, string $name = ''): string
    {
        return
            '<span class="dsv-attrib-name">' . $name . '</span>' .
            '<span class="dsv-val dsv-duration-val">' . parent::write_duration($z) . '</span>';
    }

    /**
     * @param Geldbetrag $b
     * @param string     $name
     * @return string
     */
    public function write_money(Geldbetrag $b, string $name = ''): string
    {
        return
            '<span class="dsv-attrib-name">' . $name . '</span>' .
            '<span class="dsv-val dsv-money-val">' . parent::write_money($b) . '</span>';
    }

    /**
     * @param JGAK   $jgak
     * @param string $name
     * @return string
     */
    public function write_jgak(JGAK $jgak, string $name = ''): string
    {
        return
            '<span class="dsv-attrib-name">' . $name . '</span>' .
            '<span class="dsv-val dsv-jgak-val">' . parent::write_jgak($jgak) . '</span>';
    }

    /**
     * @param Comment $c
     * @return string
     */
    public function write_comment(Comment $c): string
    {
        return
            '<span class="dsv-syntax-elem dsv-comment-start">' . DSV::COMMENT_START . '</span>' .
            '<span class="dsv-val dsv-comment-val">' . '&nbsp;' . $c->get_comment() . '&nbsp;' . '</span>' .
            '<span class="dsv-syntax-elem dsv-comment-end">' . DSV::COMMENT_END . '</span>';
    }

}