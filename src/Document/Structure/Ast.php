<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Structure;

use Iterator;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Comment;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\DSV;
use Lukaspotthast\DSV\Exception\Ast_Parse_Exception;
use Lukaspotthast\Support\Str;

/**
 * Class Ast
 * @package Lukaspotthast\DSV
 */
class Ast implements Iterator
{

    /**
     * @var string
     */
    private $filename;

    /**
     * @var array
     */
    private $nodes;

    /**
     * Ast constructor.
     */
    public function __construct()
    {
        $this->nodes = [];
    }

    /**
     * Calculates the representation of this object in JSON.
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->nodes);
    }

    /**
     * @param bool $reverse
     * @return Statement|null
     */
    private function _get_first_stmt(bool $reverse = false): ?Statement
    {
        $arr = $reverse ? array_reverse($this->nodes) : $this->nodes;
        foreach ( $arr as &$node )
        {
            // Return the first node seen which represents a "Statement".
            if ( $node instanceof Statement )
            {
                return $node;
            }
        }

        return null;
    }

    /**
     * @return Statement|null
     */
    public function get_first_stmt(): ?Statement
    {
        return $this->_get_first_stmt(false);
    }

    /**
     * @return Statement|null
     */
    public function get_last_stmt(): ?Statement
    {
        return $this->_get_first_stmt(true);
    }

    /**
     * @param bool $reverse
     * @return array
     */
    private function _retrieve_leading_comments(bool $reverse = false): array
    {
        $comments = [];
        $arr      = $reverse ? array_reverse($this->nodes) : $this->nodes;
        foreach ( $arr as $key => &$node )
        {
            if ( $node instanceof Comment )
            {
                array_push($comments, $node);
                unset($this->nodes[$key]);
            }

            // Break on the first statement.
            if ( $node instanceof Statement )
            {
                break;
            }
        }
        return $comments;
    }

    /**
     * @return array
     */
    public function retrieve_leading_comments(): array
    {
        return $this->_retrieve_leading_comments(false);
    }


    /**
     * @param string $class_name
     * @param array  ...$values
     * @return null|object
     */
    public function search(string $class_name, ...$values)
    {
        foreach ( $this->nodes as &$node )
        {
            if ( $node instanceof $class_name )
            {
                if ( $node instanceof Comment )
                {
                    return $node;
                }

                if ( $node instanceof Statement )
                {
                    if ( $node->get_name() === $values[0] )
                    {
                        return $node;
                    }
                }
            }
        }
        return null;
    }

    /**
     * @param mixed
     * @return bool
     */
    public function delete($obj): bool
    {
        $unset = false;
        foreach ( $this->nodes as $key => &$node )
        {
            if ( $node === $obj )
            {
                unset($this->nodes[$key]);
                $unset = true;
            }
        }
        return $unset;
    }

    /**
     * @param array $lines
     * @throws Ast_Parse_Exception
     */
    public function parse(array $lines): void
    {
        foreach ( $lines as &$line )
        {
            // Ignore empty lines.
            if ( strlen($line) === 0 )
            {
                continue;
            }
            else
            {
                $ast_elements = $this->process_line($line);
                if ( $ast_elements === null )
                {
                    throw new Ast_Parse_Exception(
                        'Line could not be parsed! It might not be syntactically correct.', $line
                    );
                }
                else
                {
                    $this->nodes = array_merge($this->nodes, $ast_elements);
                }
            }
        }
    }

    /**
     * @param string $line
     * @return null|array [Ast_Element]
     */
    private function process_line(string $line): ?array
    {
        $ast_elements = [];

        // Just a comment.
        if ( Str::starts_with($line, DSV::COMMENT_START) and
             Str::ends_with($line, DSV::COMMENT_END) )
        {
            $comment = trim(substr($line, strlen(DSV::COMMENT_START), -strlen(DSV::COMMENT_END)));
            array_push($ast_elements, new Comment($comment));
            return $ast_elements;
        }

        // Has a comment.
        if ( Str::ends_with($line, DSV::COMMENT_END) )
        {
            $first_comment_opener_from_end = strrpos($line, DSV::COMMENT_START);
            if ( $first_comment_opener_from_end !== false )
            {
                $comment = trim(substr($line, $first_comment_opener_from_end + strlen(DSV::COMMENT_START), -strlen(DSV::COMMENT_END)));
                array_push($ast_elements, new Comment($comment));

                $line = trim(substr($line, 0, $first_comment_opener_from_end));
            }
            else
            {
                return null;
            }
        }

        // Search for an element name completion sign.
        $colon_pos = strpos($line, DSV::STMT_ELEMENT_NAME_COMPLETION_SIGN);
        if ( $colon_pos === false )
        {
            // We are unable to separate the statements name from its corresponding attribute list.
            // The statement may not have any attributes and therefore does not need the completion sign!
            array_push($ast_elements, new Statement($line, []));
            return $ast_elements;
        }

        $name           = substr($line, 0, $colon_pos);
        $attribute_list = substr($line, $colon_pos + 1);

        if ( strlen($attribute_list) > 0 and !Str::ends_with($attribute_list, ';') )
        {
            return null;
        }

        $attributes = explode(DSV::STMT_ATTRIB_DELIMITER, $attribute_list);
        // Every attribute is followed by a ";". explode() will generate an empty array entry for the string after the
        // last ";".
        unset($attributes[count($attributes) - 1]);

        // UPDATE: "Regardless of the data type, leading and trailing spaces within an attribute are allowed!"
        foreach ( $attributes as &$attribute )
        {
            $attribute = trim($attribute);
        }

        array_push($ast_elements, new Statement($name, $attributes));
        return $ast_elements;
    }

    /**
     * Rewind the Iterator to the first element
     * @link  http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        reset($this->nodes);
    }

    /**
     * Checks if current position is valid
     * @link  http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid(): bool
    {
        return key($this->nodes) !== null;
    }

    /**
     * Return the key of the current element
     * @link  http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return key($this->nodes);
    }

    /**
     * Return the current element
     * @link  http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        return current($this->nodes);
    }

    /**
     * Move forward to next element
     * @link  http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        next($this->nodes);
    }

    /**
     * @param string $filename
     */
    public function set_filename(string $filename): void
    {
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function get_filename(): string
    {
        return $this->filename;
    }

}