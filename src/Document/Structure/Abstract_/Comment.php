<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Structure\Abstract_;

use Lukaspotthast\DSV\Document\Structure\Ast_Element;

/**
 * Class Comment
 * @package Lukaspotthast\DSV\Document\Structure\Abstract_
 */
class Comment extends Ast_Element
{

    /**
     * @var string
     */
    private $comment;

    /**
     * Comment constructor.
     * @param string $comment
     */
    public function __construct(string $comment)
    {
        $this->set_comment($comment);
    }

    /**
     * @param string $comment
     */
    public function set_comment(string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function get_comment(): string
    {
        return $this->comment;
    }

}