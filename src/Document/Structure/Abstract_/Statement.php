<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document\Structure\Abstract_;

use Lukaspotthast\DSV\Document\Structure\Ast_Element;

/**
 * Class Statement
 * @package Lukaspotthast\DSV\Document\Structure\Abstract_
 */
class Statement extends Ast_Element
{

    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $attributes;

    /**
     * Statement constructor.
     * @param string $name
     * @param array  $attributes
     */
    public function __construct(string $name, array $attributes)
    {
        $this->set_name($name);
        $this->set_attributes($attributes);
    }

    /**
     * @param string $name
     */
    public function set_name(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function get_name(): string
    {
        return $this->name;
    }

    /**
     * @param array $attributes
     */
    public function set_attributes(array $attributes): void
    {
        $this->attributes = $attributes;
    }

    /**
     * @return array
     */
    public function get_attributes(): array
    {
        return $this->attributes;
    }

}