<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document;

use Lukaspotthast\DSV\Data\Datum;
use Lukaspotthast\DSV\Data\Geldbetrag;
use Lukaspotthast\DSV\Data\JGAK;
use Lukaspotthast\DSV\Data\Uhrzeit;
use Lukaspotthast\DSV\Data\Zahl;
use Lukaspotthast\DSV\Data\Zeichen;
use Lukaspotthast\DSV\Data\Zeichenkette;
use Lukaspotthast\DSV\Data\Zeit;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Comment;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Lukaspotthast\DSV\Document\Writer\Writer;
use Lukaspotthast\DSV\Exception\Element_Creation_Exception;
use Lukaspotthast\DSV\Exception\Element_Integrity_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;
use ReflectionClass;

/**
 * Class Document_Element
 * @package Lukaspotthast\DSV\Documents
 */
abstract class Document_Element
{

    const ATTRIBUTE_AMOUNT_ERROR           = 'The "%s" statement did not specify the correct number of arguments (%d).';
    const REQUIRED_ATTRIBUTE_MISSING_ERROR = 'The "%s" statement contained en empty value for a necessary attribute (%d)!';

    /** @var array */
    private $comments = [];

    /** @var Document|null */
    private $parent = null;

    /**
     * Document_Element constructor.
     * @param Document       $parent
     * @param Statement|null $stmt
     * @throws Element_Creation_Exception
     */
    public function __construct(?Document $parent, Statement $stmt = null)
    {
        $this->parent = $parent;

        if ( $stmt !== null )
        {
            try
            {
                $this->create_from_statement($stmt);
            }
            catch ( Runtime_Exception $e )
            {
                throw new Element_Creation_Exception(
                    'Unable to create an instance of "' . get_class($this) . '".',
                    0, $e, $stmt
                );
            }
        }
    }

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * @return string
     */
    public final static function get_element_name(): string
    {
        $overridden_name = static::override_get_element_name();
        if ( strlen($overridden_name) !== 0 )
        {
            return $overridden_name;
        }
        else
        {
            // Note: "static::" is aware of ongoing inheritance!
            // This function will return "FOO" if class Foo extends Document_Element and get_element_name() gets called
            // on an object of Foo.
            /** @noinspection PhpUnhandledExceptionInspection */
            // ^- static::class is always an existing class ^^
            return strtoupper((new ReflectionClass(static::class))->getShortName());
        }
    }

    /**
     * @return string
     */
    public static function override_get_element_name(): string
    {
        return '';
    }

    /**
     * @return int
     */
    public abstract static function get_attribute_amount(): int;

    /**
     * @return array [int]
     */
    public abstract static function get_required_attribute_indices(): array;

    /**
     * @return array
     */
    protected abstract function get_attributes(): array;

    /**
     * @param Statement $stmt
     * @throws Runtime_Exception
     */
    public function create_from_statement(Statement $stmt): void
    {
        if ( static::get_element_name() !== $stmt->get_name() )
        {
            throw new Runtime_Exception(
                'The given statement object does not seem to fit. Name should have been "' .
                static::get_element_name() . '", but was "' . $stmt->get_name() . '".'
            );
        }

        if ( count($stmt->get_attributes()) !== $this->get_attribute_amount() )
        {
            throw new Runtime_Exception(
                sprintf(self::ATTRIBUTE_AMOUNT_ERROR, $this->get_element_name(), $this->get_attribute_amount())
            );
        }

        foreach ( $this->get_required_attribute_indices() as &$index )
        {
            if ( strlen($stmt->get_attributes()[$index - 1]) === 0 )
            {
                throw new Runtime_Exception(
                    sprintf(self::REQUIRED_ATTRIBUTE_MISSING_ERROR, $this->get_element_name(), $index)
                );
            }
        }

        $this->_create_from_statement($stmt);

        $this->check_integrity();
    }

    /**
     * @param Statement $stmt
     * @throws Runtime_Exception
     */
    protected function _create_from_statement(Statement $stmt): void
    {
        $stmt_attributes = $stmt->get_attributes();
        $key             = -1;
        foreach ( $this->get_attributes() as $name => &$var )
        {
            $key++;
            if ( method_exists($var, 'set_from_string') )
            {
                if ( array_key_exists($key, $stmt_attributes) )
                {
                    $var->set_from_string($stmt_attributes[$key]);
                }
                else
                {
                    throw new Runtime_Exception(
                        get_class($this) . ': ' . __FUNCTION__ . '() ' . $key . ' missing in statement attributes.'
                    );
                }
            }
            else
            {
                throw new Runtime_Exception(
                    get_class($this) . ': ' . __FUNCTION__ .
                    '() Encountered attribute which is not implementing set_from_string()'
                );
            }
        }
    }

    /**
     * Performs an integrity check over this object.
     * @throws Element_Integrity_Exception
     */
    public function check_integrity(): void
    {
        foreach ( array_keys($this->get_attributes()) as &$name )
        {
            $check_var = 'check_' . $name;
            $get_var   = 'get_' . $name;

            if ( method_exists($this, $check_var) and
                 method_exists($this, $get_var) )
            {
                try
                {
                    $this->$check_var($this->$get_var());
                }
                    /** @noinspection PhpRedundantCatchClauseInspection */
                catch ( Runtime_Exception $e )
                {
                    throw new Element_Integrity_Exception(
                        'Element integrity of ' . get_class($this) . ' is violated.',
                        $this, 0, $e
                    );
                }
                //dump('checked '. $name . ' in ' . get_class($this));
            }
            //else
            //{
            //dump('Did not find expected methods..'. serialize([$check_var,$get_var]).' in ' . get_class($this));
            //}
        }

        try
        {
            $this->perform_additional_integrity_checks();
        }
        catch ( Runtime_Exception $e )
        {
            throw new Element_Integrity_Exception(
                'Element integrity of ' . get_class($this) . ' is violated.',
                $this, 0, $e
            );
        }
    }

    /** @noinspection PhpDocRedundantThrowsInspection */
    /**
     * @throws Runtime_Exception
     */
    public function perform_additional_integrity_checks(): void
    {
        // to be overridden...
    }

    /**
     * @param Writer $writer
     * @param array  $options
     * @return string
     */
    public function write(Writer $writer, array $options = []): string
    {
        $include_element_name_completion = array_key_exists('include_element_name_completion', $options) ?
            $options['include_element_name_completion'] : true;
        $include_argument_list           = array_key_exists('include_argument_list', $options) ?
            $options['include_argument_list'] : true;

        $s = '';
        foreach ( $this->comments as &$comment )
        {
            $s .= $writer->start_comment();
            $s .= $writer->write_comment($comment);
            $s .= $writer->end_comment();
        }

        $s .= $writer->start_stmt();
        $include_element_name_completion ?
            $s .= $writer->write_element_name($this->get_element_name()) :
            $s .= $writer->write_element_name($this->get_element_name(), false);
        if ( $include_argument_list )
        {
            $s .= $writer->start_attrib_list();
            $s .= $this->write_attributes($writer, $this->get_attributes());
            $s .= $writer->end_attrib_list();
        }
        $s .= $writer->end_stmt();
        return $s;
    }

    /**
     * @param Writer $writer
     * @param array  $attributes
     * @return string
     */
    protected function write_attributes(Writer $writer, array $attributes): string
    {
        $s = '';
        foreach ( $attributes as $name => &$var )
        {
            if ( $var instanceof Zeichenkette )
            {
                $s .= $writer->write_string($var, $name) . $writer->write_attrib_separator();
            }
            else if ( $var instanceof Zeichen )
            {
                $s .= $writer->write_char($var, $name) . $writer->write_attrib_separator();
            }
            else if ( $var instanceof Zahl )
            {
                $s .= $writer->write_number($var, $name) . $writer->write_attrib_separator();
            }
            else if ( $var instanceof Datum )
            {
                $s .= $writer->write_date($var, $name) . $writer->write_attrib_separator();
            }
            else if ( $var instanceof Uhrzeit )
            {
                $s .= $writer->write_time($var, $name) . $writer->write_attrib_separator();
            }
            else if ( $var instanceof Zeit )
            {
                $s .= $writer->write_duration($var, $name) . $writer->write_attrib_separator();
            }
            else if ( $var instanceof JGAK )
            {
                $s .= $writer->write_jgak($var, $name) . $writer->write_attrib_separator();
            }
            else if ( $var instanceof Geldbetrag )
            {
                $s .= $writer->write_money($var, $name) . $writer->write_attrib_separator();
            }
        }
        return $s;
    }

    /**
     * @param array $comments
     * @throws Runtime_Exception
     */
    public function set_comments(array $comments): void
    {
        foreach ( $comments as &$comment )
        {
            if ( !($comment instanceof Comment) )
            {
                throw new Runtime_Exception(
                    'Array element was not of type "' . Comment::class . '": ' . serialize($comment)
                );
            }
        }

        $this->comments = $comments;
    }

    /**
     * @return array
     */
    public function get_comments(): array
    {
        return $this->comments;
    }

    /**
     * @param Comment $comment
     */
    public function add_comment(Comment $comment): void
    {
        array_push($this->comments, $comment);
    }

    public function clear_comments(): void
    {
        $this->comments = [];
    }

    /**
     * @param Document|null $parent
     */
    public function set_parent(?Document $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return Document|null
     */
    public function get_parent(): ?Document
    {
        return $this->parent;
    }

}