<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Document;

use Lukaspotthast\DSV\Document\Element\Dateiende;
use Lukaspotthast\DSV\Document\Element\Format;
use Lukaspotthast\DSV\Document\Structure\Ast;
use Lukaspotthast\DSV\Exception\Document_Creation_Exception;
use Lukaspotthast\DSV\Exception\Document_Integrity_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Creator
 * @package Lukaspotthast\DSV\Document
 */
class Creator
{

    /**
     * @param Ast $ast
     * @return Document
     * @throws Document_Creation_Exception
     * @throws Document_Integrity_Exception
     */
    public function create(Ast $ast): Document
    {
        /*
         * INFO:
         * - the general ordering of statements is not fixed
         * - the FORMAT statement must be the first one
         * - the DATEIENDE statement must be the last one
         */

        $comments = $ast->retrieve_leading_comments();

        // We will first retrieve (search and delete) the "FORMAT" and "DATEIENDE" statements,
        // which are always necessary.
        $format    = $this->retrieve_format($ast);
        $dateiende = $this->retrieve_dateiende($ast);

        $class = $format->get_document_class($ast);

        /** @var Document $document */
        $document = new $class();
        $format->set_parent($document);

        $document->set_comments($comments);
        $document->set_format($format);
        $document->set_dateiende($dateiende);
        $document->create($ast);

        return $document;
    }

    /**
     * @param Ast $ast
     * @return Format
     * @throws Document_Creation_Exception
     */
    public function retrieve_format(Ast $ast): Format
    {
        $first = $ast->get_first_stmt();

        if ( $first === null )
        {
            $msg = 'The specified file is missing a "' . Format::get_element_name() . '" statement.';
            throw new Document_Creation_Exception($msg, $ast);
        }

        if ( $first->get_name() !== Format::get_element_name() )
        {
            $msg = 'The specified file is missing a "' . Format::get_element_name() . '" statement as its FIRST! statement.';
            throw new Document_Creation_Exception($msg, $ast);
        }

        $ast->delete($first);

        try
        {
            $format = new Format(null);
            $format->create_from_statement($first);
        }
        catch ( Runtime_Exception $e )
        {
            throw new Document_Creation_Exception(
                'Unable to create a ' . Format::class . ' object.',
                $ast, 0, $e
            );
        }

        return $format;
    }

    /**
     * @param Ast $ast
     * @return Dateiende
     * @throws Document_Creation_Exception
     */
    public function retrieve_dateiende(Ast $ast): Dateiende
    {
        $stmt = $ast->get_last_stmt();

        if ( $stmt === null )
        {
            $msg = 'The specified file is missing a "' . Dateiende::get_element_name() . '" statement.';
            throw new Document_Creation_Exception($msg, $ast, 0, null);
        }

        if ( $stmt->get_name() !== Dateiende::get_element_name() )
        {
            $msg = 'The specified file is missing a "' . Dateiende::get_element_name() . '" statement as its LAST! statement.';
            throw new Document_Creation_Exception($msg, $ast, 0, null);
        }

        $ast->delete($stmt);

        try
        {
            $dateiende = new Dateiende(null);
            $dateiende->create_from_statement($stmt);
        }
        catch ( Runtime_Exception $e )
        {
            throw new Document_Creation_Exception(
                'Unable to create a ' . Dateiende::class . ' object.',
                $ast, 0, $e
            );
        }

        return $dateiende;
    }

}