<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Exception;

use Throwable;

/**
 * Class Ast_Parse_Exception
 * @package Lukaspotthast\DSV\Exception
 */
class Ast_Parse_Exception extends Runtime_Exception
{

    /** @var string */
    private $current_line;

    /**
     * Ast_Parse_Exception constructor.
     * @param string         $message
     * @param string         $current_line
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", string $current_line, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->current_line = $current_line;
    }

    /**
     * @param string $current_line
     */
    public function set_line(string $current_line): void
    {
        $this->current_line = $current_line;
    }

    /**
     * @return string
     */
    public function get_current_line(): string
    {
        return $this->current_line;
    }

}