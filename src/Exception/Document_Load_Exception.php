<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Exception;

use Throwable;

/**
 * Class Document_Load_Exception
 * @package Lukaspotthast\DSV\Exception
 */
class Document_Load_Exception extends Runtime_Exception
{

    /**
     * Document_Load_Exception constructor.
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}