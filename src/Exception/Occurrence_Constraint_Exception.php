<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Exception;

/**
 * Class Occurrence_Constraint_Exception
 * @package Lukaspotthast\DSV\Exception
 */
class Occurrence_Constraint_Exception extends Runtime_Exception
{

}