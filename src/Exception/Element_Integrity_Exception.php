<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Exception;

use Lukaspotthast\DSV\Document\Document_Element;
use Throwable;

/**
 * Class Element_Integrity_Exception
 * @package Lukaspotthast\DSV\Exception
 */
class Element_Integrity_Exception extends Runtime_Exception
{

    /** @var Document_Element */
    private $element;

    /**
     * Element_Integrity_Exception constructor.
     * @param string           $message
     * @param Document_Element $element
     * @param int              $code
     * @param Throwable|null   $previous
     */
    public function __construct(string $message = "", Document_Element $element, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->element = $element;
    }

    /**
     * @param Document_Element $element
     */
    public function set_element(Document_Element $element): void
    {
        $this->element = $element;
    }

    /**
     * @return Document_Element
     */
    public function get_element(): Document_Element
    {
        return $this->element;
    }

}