<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Exception;

use Throwable;

/**
 * Class File_Read_Exception
 * @package Lukaspotthast\DSV\Exception
 */
class File_Read_Exception extends Runtime_Exception
{

    /** @var string */
    private $filename;

    /**
     * File_Read_Exception constructor.
     * @param string         $message
     * @param string         $filename
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", string $filename, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->filename = $filename;
    }

    /**
     * @param string $filename
     */
    public function set_filename(string $filename): void
    {
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function get_filename(): string
    {
        return $this->filename;
    }

}