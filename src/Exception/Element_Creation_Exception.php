<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Exception;

use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use Throwable;

/**
 * Class Element_Creation_Exception
 * @package Lukaspotthast\DSV\Exception
 */
class Element_Creation_Exception extends Runtime_Exception
{

    /**
     * @var Statement
     */
    private $stmt;

    /**
     * Element_Creation_Exception constructor.
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     * @param Statement      $stmt
     */
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null, Statement $stmt)
    {
        parent::__construct($message, $code, $previous);

        $this->stmt = $stmt;
    }

    /**
     * @return Statement
     */
    public function get_statement(): Statement
    {
        return $this->stmt;
    }

}