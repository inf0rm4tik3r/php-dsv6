<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Exception;

use Exception;

/**
 * Class Runtime_Exception
 * @package Lukaspotthast\DSV\Exception
 */
class Runtime_Exception extends Exception
{

}