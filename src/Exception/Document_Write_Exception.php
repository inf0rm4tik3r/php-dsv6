<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Exception;

use Lukaspotthast\DSV\Document\Document;
use Throwable;

/**
 * Class Document_Write_Exception
 * @package Lukaspotthast\DSV\Exception
 */
class Document_Write_Exception extends Runtime_Exception
{

    /** @var Document $document */
    private $document;

    /**
     * Document_Write_Exception constructor.
     * @param string         $message
     * @param Document       $document
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", Document $document, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->document = $document;
    }

    /**
     * @param Document $document
     */
    public function set_document(Document $document): void
    {
        $this->document = $document;
    }

    /**
     * @return Document
     */
    public function get_document(): Document
    {
        return $this->document;
    }

}