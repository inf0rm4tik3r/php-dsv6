<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Exception;

use Lukaspotthast\DSV\Document\Structure\Ast;
use Throwable;

/**
 * Class Document_Creation_Exception
 * @package Lukaspotthast\DSV\Exception
 */
class Document_Creation_Exception extends Runtime_Exception
{

    /**
     * @var Ast
     */
    private $ast;

    /**
     * Document_Creation_Exception constructor.
     * @param string         $message
     * @param Ast            $ast
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", Ast $ast, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->ast = $ast;
    }

    /**
     * @return Ast
     */
    public function get_ast(): Ast
    {
        return $this->ast;
    }

}