<?php

declare(strict_types = 1);

use Lukaspotthast\DSV\Tests\System\Read_From_File_Test;

require '../vendor/autoload.php';

(new Read_From_File_Test())->run();
//(new Manually_Create_Wettkampfdefinitionsliste_Document_Test())->run();

//$unit_tests_result = PHPUnit_Test_Runner::run_get_result(__DIR__.'/Tests/Unit');
?>


<HTML>
<head>
    <title>Unit Tests</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <style>
        .phpunit-result {

            margin-top: 1em;
            padding: 1em;
            border: 1px solid gray;
            border-radius: 0;
            background-color: #f3f4f3
        }

        .phpunit-test-results-table-wrapper {
            width: 100%;
            max-height: 15em;
            overflow-x: auto;
            font-size: 0.8em;
        }

        .phpunit-test-results-table {
            margin: 0;
            width: 100%;
            font-size: 1em;
            border-collapse: collapse;
        }

        .phpunit-test-results-table td {
            padding: 0.3em;
            text-align: left;
            text-align: start;
            vertical-align: top;
        }

        .phpunit-test-results-table td, th {
            border: .05em solid gray;
        }

        .phpunit-test-results-table th {
            padding: 1em;
            border-bottom: .15em solid gray;
        }

        .phpunit-test-results-table .column-header {
            vertical-align: top;
        }

        .phpunit-test-result {
            background-color: beige;
        }

        .phpunit-test-result .phpunit-test-func {
        }

        .phpunit-test-result .phpunit-test-func-result {
        }

        .phpunit-header {
            background-color: #7cbff4;
        }

        .phpunit-skipped {
            margin: 0 1em;
        }

        .phpunit-defect {
            background-color: #f4594b;
        }

        .phpunit-footer {
            background-color: #f4d97a;
        }
    </style>
    <style>
        .dsv-document {
            background-color: #232525;
            color: #BABABA;
        }

        .dsv-document span {
            display: inline-block;
            vertical-align: middle;
        }

        .dsv-document-header,
        .dsv-document-body,
        .dsv-document-footer {
            padding: .5em;
        }

        .dsv-document-body {
            white-space: nowrap;
            overflow: auto;
        }

        .dsv-syntax-elem {
            color: #CC7832;
        }

        .dsv-val {
            color: #9876AA;
        }

        .dsv-comment-val {
            color: #92C14F
        }

        .dsv-string-val,
        .dsv-char-val {
            color: #9876AA;
        }

        .dsv-number-val,
        .dsv-money-val {
            color: #f4594b;
        }

        .dsv-date-val,
        .dsv-time-val,
        .dsv-duration-val,
        .dsv-jgak-val {
            color: #6897BB;
        }

        .dsv-element-name {
            color: #E8BF6A;
        }

        .dsv-element-completion {
        }

        .dsv-attrib-name {
            margin-left: 0.5em;
            color: #787878;
            background-color: #333333;
            border: .1em solid #2B2B2B;
            font-size: 0.7em;
            user-select: none;
        }

        .dsv-attrib-name:after {
            content: ":";
        }

        .dsv-attrib-separator {
        }

        .dsv-document-header {
            border-bottom: 1px solid #BABABA;
        }

        .dsv-document-footer {
            border-top: 1px solid #BABABA;
        }

        .dsv-performance-info {
        }
    </style>

</head>
<body style="font-family: 'Open Sans', sans-serif;">
<div class="phpunit-result">
    <?= isset($test_results) ? $test_results : 'Test were not executed...' ?>
</div>
</body>
</HTML>
