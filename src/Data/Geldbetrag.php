<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Data;

use Lukaspotthast\DSV\Exception\Runtime_Exception;
use Lukaspotthast\Support\Str;

/**
 * Class Geldbetrag
 * @package Lukaspotthast\DSV\Data
 */
class Geldbetrag implements Data_Object
{

    /**
     * Amount.
     * @var null|int
     */
    private $a = null;

    /**
     * Fraction.
     * @var null|int
     */
    private $f = null;

    /**
     * Money constructor.
     * @param string|null $money
     * @throws Runtime_Exception
     */
    public function __construct(string $money = null)
    {
        if ( $money !== null )
        {
            try
            {
                $this->set_from_string($money);
            }
            catch ( Runtime_Exception $e )
            {
                throw new Runtime_Exception('Unable to create ' . __CLASS__ . ' instance.', 0, $e);
            }
        }
    }

    /**
     * Resets the object to an uninitialized state.
     */
    public function reset(): void
    {
        $this->a = null;
        $this->f = null;
    }

    /**
     * @param string $data
     * @throws Runtime_Exception
     */
    public function set_from_string(string $data): void
    {
        // Only one ','.
        if ( substr_count(',', $data) > 1 )
        {
            throw new Runtime_Exception(
                '"' . $data . '" was not a properly formatted amount of money! ' .
                'Only one "," is allowed as the decimal separator.'
            );
        }

        $parts = explode(',', $data);
        $a     = $parts[0];
        $f     = $parts[1];

        // Both parts must be numerical.
        if ( !(is_numeric($a) and is_numeric($f)) )
        {
            throw new Runtime_Exception(
                'Both amount and fraction must be numerical! Given data: ' . $data
            );
        }

        $this->set_amount(intval($a));
        $this->set_fraction(intval($f));
    }

    /**
     * @return string
     */
    public function get_formatted(): string
    {
        return $this->get_amount_formatted() . ',' . $this->get_fraction_formatted();
    }

    /**
     * @param int $amount
     */
    public function set_amount(int $amount): void
    {
        $this->a = $amount;
    }

    /**
     * @return int
     */
    public function get_amount(): int
    {
        return $this->a;
    }

    /**
     * @return string
     */
    public function get_amount_formatted(): string
    {
        $a        = $this->a;
        $negative = $a < 0;
        if ( $negative )
        {
            $a *= -1;
        }
        return $negative ? '- ' . strval($a) : strval($a);
    }

    /**
     * @param int $fraction
     */
    public function set_fraction(int $fraction): void
    {
        $this->f = $fraction;
    }

    /**
     * @return int
     */
    public function get_fraction(): int
    {
        return $this->f;
    }

    /**
     * @return string
     */
    public function get_fraction_formatted(): string
    {
        $f = strval($this->f);

        if ( strlen($f) < 2 )
        {
            return Str::zero_fill_right($f, 2);
        }
        else
        {
            return substr($f, 0, 2);
        }
    }

    /**
     * @return bool
     */
    public function is_set(): bool
    {
        return
            $this->a !== null and
            $this->f !== null;
    }

}