<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Data;

use Lukaspotthast\DSV\Exception\Runtime_Exception;
use Lukaspotthast\Support\Str;

/**
 * Class JGAK
 * @package Lukaspotthast\DSV\Data
 *
 * Vierstellige Zahl, wenn Jahrgang. Bei Einzelwettkämpfen sind für Altersklassen
 * folgende Angaben zulässig: A,B,C,D,E,J sowie für die Masters 20,25,30,40 usw. für die
 * entsprechenden Einzel-Altersklassen der Masters. Bei Staffelwettkämpfen sind für die
 * Altersklassen folgende Angaben zulässig: A,B,C,D,E,J, sowie bei Masters das minimale
 * Gesamtalter der Mannschaft gefolgt von einem Pluszeichen (80+,100+,120+ usw.).
 */
class JGAK implements Data_Object
{

    /**
     * @var string|null
     */
    private $jgak = null;

    /**
     * Time constructor.
     * @param string|null $jgak
     * @throws Runtime_Exception
     */
    public function __construct(string $jgak = null)
    {
        if ( $jgak !== null )
        {
            try
            {
                $this->set_from_string($jgak);
            }
            catch ( Runtime_Exception $e )
            {
                throw new Runtime_Exception('Unable to create ' . __CLASS__ . ' instance.', 0, $e);
            }
        }
    }

    /**
     * Resets the object to an uninitialized state.
     */
    public function reset(): void
    {
        $this->jgak = null;
    }

    /**
     * @param string $jgak
     * @return bool
     */
    public function check_string(string $jgak): bool
    {
        // Das Objekt darf leer sein.
        if ( $jgak === '' )
        {
            return true;
        }

        // Die "0" kann z.B. als "offene Klasse" oder "und jünger" definiert sein (abhängig vom aktuellen Kontext)
        // und ist damit erlaubt.
        if ( $jgak === '0' )
        {
            return true;
        }

        if ( strlen($jgak) === 4 and is_numeric($jgak) )
        {
            return true;
        }

        // AK Einzelwettkampf
        if ( in_array($jgak, ['A', 'B', 'C', 'D', 'E', 'J']) )
        {
            return true;
        }

        // Masters Einzelwettkampf
        if ( is_numeric($jgak) )
        {
            $intval = intval($jgak);
            if ( $intval === 25 )
            {
                return true;
            }
            if ( $intval > 10 and ($intval % 10 === 0) )
            {
                return true;
            }
        }

        // AK Staffel: Wie AK Einzelwettkampf

        // Masters Staffel:
        if ( Str::ends_with($jgak, '+') )
        {
            $numerical = substr($jgak, 0, -1);
            if ( is_numeric($numerical) )
            {
                $intval = intval($jgak);
                if ( $intval >= 80 and ($intval % 20 === 0) )
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param string $jgak
     * @throws Runtime_Exception
     */
    public function set_from_string(string $jgak): void
    {
        if ( $this->check_string($jgak) === false )
        {
            throw new Runtime_Exception('"' . $jgak . '" was not a valid JGAK value!');
        }

        $this->jgak = $jgak;
    }

    /**
     * @return string|null
     */
    public function get_jgak(): ?string
    {
        return $this->jgak;
    }

    /**
     * @return string
     */
    public function get_formatted(): string
    {
        return $this->get_jgak();
    }

    /**
     * @return bool
     */
    public function is_set(): bool
    {
        return $this->jgak !== null;
    }

}