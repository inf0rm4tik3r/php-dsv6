<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Data;

/**
 * Interface Data_Object
 * @package Lukaspotthast\DSV\Data
 */
interface Data_Object
{

    /**
     * Resets the object to an uninitialized state.
     */
    public function reset(): void;

    /**
     * @param string $data
     */
    public function set_from_string(string $data): void;

    /**
     * @return string
     */
    public function get_formatted(): string;

    /**
     * @return bool
     */
    public function is_set(): bool;

}