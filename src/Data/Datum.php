<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Data;

use DateTime;
use Lukaspotthast\DSV\Exception\Runtime_Exception;
use Lukaspotthast\Support\Str;

/**
 * Class Datum
 * @package Lukaspotthast\DSV\Data
 */
class Datum implements Data_Object
{

    const FORMAT = 'd.m.Y';

    /** @var string|null */
    private $day = null;

    /** @var string|null */
    private $month = null;

    /** @var string|null */
    private $year = null;

    /**
     * Date constructor.
     * @param string|null $date
     * @throws Runtime_Exception
     */
    public function __construct(string $date = null)
    {
        if ( $date !== null )
        {
            try
            {
                $this->set_from_string($date);
            }
            catch ( Runtime_Exception $e )
            {
                throw new Runtime_Exception('Unable to create ' . __CLASS__ . ' instance.', 0, $e);
            }
        }
    }

    /**
     * Resets the object to an uninitialized state.
     */
    public function reset(): void
    {
        $this->day   = null;
        $this->month = null;
        $this->year  = null;
    }

    /**
     * This function may return a reformatted value!
     * @param string $date
     * @return array|bool
     */
    public function check_string(string $date)
    {
        return $this->check_date_parts($this->get_date_parts($date));
    }

    /**
     * @param string $date
     * @throws Runtime_Exception
     */
    public function set_from_string(string $date): void
    {
        if ( strlen($date) === 0 )
        {
            return;
        }
        $check_result = $this->check_string($date);
        if ( $check_result === false )
        {
            throw new Runtime_Exception('"' . $date . '" was not of the form "DD.MM.YYYY"!');
        }

        $this->day   = $check_result['day'];
        $this->month = $check_result['month'];
        $this->year  = $check_result['year'];
    }

    /**
     * @param string $date
     * @return array
     */
    private function get_date_parts(string $date): array
    {
        // There must be three date parts (day, month and year)!
        return explode('.', $date);
    }

    /**
     * @param array $parts
     * @return array|bool
     */
    private function check_date_parts(array $parts)
    {
        if ( count($parts) !== 3 )
        {
            return false;
        }

        $day   = intval($parts[0]);
        $month = intval($parts[1]);
        $year  = intval($parts[2]);

        // The specified data must represent a valid date.
        if ( checkdate($month, $day, $year) === false )
        {
            return false;
        }

        return [
            'day'   => $day,
            'month' => $month,
            'year'  => $year,
        ];
    }

    /**
     * @return string
     */
    public function get_formatted(): string
    {
        if ( $this->is_set() )
        {
            return
                Str::zero_fill_left(strval($this->day), 2) . '.' .
                Str::zero_fill_left(strval($this->month), 2) . '.' .
                strval($this->year);
        }
        else
        {
            return '';
        }
    }

    /**
     * @return DateTime
     * @throws Runtime_Exception
     */
    public function get_datetime_object(): DateTime
    {
        if ( !$this->is_set() )
        {
            throw new Runtime_Exception('This ' . __CLASS__ . ' object is not set.');
        }
        return new DateTime($this->get_formatted());
    }

    /**
     * @return bool
     */
    public function is_set(): bool
    {
        return
            $this->day !== null and
            $this->month !== null and
            $this->year !== null;
    }

}