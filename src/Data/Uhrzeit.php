<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Data;

use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Uhrzeit
 * @package Lukaspotthast\DSV\Data
 */
class Uhrzeit implements Data_Object
{

    const FORMAT = 'H:i';

    /** @var int|null */
    private $timestamp = null;

    /**
     * Uhrzeit constructor.
     * @param string|null $time
     * @throws Runtime_Exception
     */
    public function __construct(string $time = null)
    {
        if ( $time !== null )
        {
            try
            {
                $this->set_from_string($time);
            }
            catch ( Runtime_Exception $e )
            {
                throw new Runtime_Exception('Unable to create ' . __CLASS__ . ' instance.', 0, $e);
            }
        }
    }

    /**
     * Resets the object to an uninitialized state.
     */
    public function reset(): void
    {
        $this->timestamp = null;
    }

    /**
     * This function may return a reformatted value!
     * @param string $time
     * @return bool
     */
    public function check_string(string $time): bool
    {
        $timestamp = strtotime($time);
        if ( $timestamp === false )
        {
            return false;
        }

        return date(self::FORMAT, $timestamp) === $time;
    }

    /**
     * @param string $time
     * @throws Runtime_Exception
     */
    public function set_from_string(string $time): void
    {
        if ( strlen($time) === 0 )
        {
            return;
        }
        if ( $this->check_string($time) === false )
        {
            throw new Runtime_Exception('"' . $time . '" was not of the form "HH:MM"!');
        }
        $this->timestamp = strtotime($time);
    }

    /**
     * @return int|null
     */
    public function get_timestamp(): ?int
    {
        return $this->timestamp;
    }

    /**
     * @return string
     */
    public function get_formatted(): string
    {
        if ( $this->is_set() )
        {
            return date(self::FORMAT, $this->timestamp);
        }
        else
        {
            return '';
        }
    }

    /**
     * @return bool
     */
    public function is_set(): bool
    {
        return $this->timestamp !== null;
    }

}