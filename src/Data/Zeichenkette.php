<?php

declare (strict_types = 1);

namespace Lukaspotthast\DSV\Data;

/**
 * Class Zeichenkette
 * @package Lukaspotthast\DSV\Data
 */
class Zeichenkette implements Data_Object
{

    /** @var string|null */
    private $zeichenkette = null;
    private $allow_empty  = true;

    /**
     * Zeichenkette constructor.
     * @param string|null $zeichenkette
     * @param bool        $allow_empty
     */
    public function __construct(string $zeichenkette = null, bool $allow_empty = true)
    {
        $this->allow_empty = $allow_empty;
        if ( $zeichenkette !== null )
        {
            $this->set_from_string($zeichenkette);
        }
    }

    /**
     * Resets the object to an uninitialized state.
     */
    public function reset(): void
    {
        $this->zeichenkette = null;
    }

    /**
     * @param string $data
     */
    public function set_from_string(string $data): void
    {
        if ( $this->allow_empty === false && strlen($data) === 0 )
        {
            return;
        }
        $this->zeichenkette = $data;
    }

    /**
     * @return string|null
     */
    public function get_zeichenkette(): ?string
    {
        return $this->zeichenkette;
    }

    /**
     * @return string
     */
    public function get_formatted(): string
    {
        if ( $this->is_set() )
        {
            return strval($this->zeichenkette);
        }
        return '';
    }

    /**
     * @return bool
     */
    public function is_set(): bool
    {
        return $this->zeichenkette !== null;
    }

}