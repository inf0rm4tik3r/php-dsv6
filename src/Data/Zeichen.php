<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Data;

use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Zeichen
 * @package Lukaspotthast\DSV\Data
 */
class Zeichen implements Data_Object
{

    /** @var string|null */
    private $zeichen = null;

    /**
     * Char constructor.
     * @param string|null $zeichen
     * @throws Runtime_Exception
     */
    public function __construct(string $zeichen = null)
    {
        if ( $zeichen !== null )
        {
            try
            {
                $this->set_from_string($zeichen);
            }
            catch ( Runtime_Exception $e )
            {
                throw new Runtime_Exception('Unable to create ' . __CLASS__ . ' instance.', 0, $e);
            }
        }
    }

    /**
     * Resets the object to an uninitialized state.
     */
    public function reset(): void
    {
        $this->zeichen = null;
    }

    /**
     * @param string $data
     * @return bool
     */
    public function check_string(string $data): bool
    {
        return strlen($data) <= 1;
    }

    /**
     * @param string $data
     * @throws Runtime_Exception
     */
    public function set_from_string(string $data): void
    {
        if ( $this->check_string($data) )
        {
            $this->zeichen = $data;
        }
        else
        {
            throw new Runtime_Exception('"' . $data . '" is not a single character.');
        }
    }

    /**
     * @return null|string
     */
    public function get_zeichen(): ?string
    {
        return $this->zeichen;
    }

    /**
     * @return string
     */
    public function get_formatted(): string
    {
        if ( $this->is_set() )
        {
            return $this->zeichen;
        }
        return '';
    }

    /**
     * @return bool
     */
    public function is_set(): bool
    {
        return $this->zeichen !== null;
    }

}