<?php

declare (strict_types = 1);

namespace Lukaspotthast\DSV\Data;

use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Nummer
 * @package Lukaspotthast\DSV\Data
 */
class Zahl implements Data_Object
{

    /** @var int|null */
    private $zahl = null;

    /**
     * Zahl constructor.
     * @param string|null $zahl
     * @throws Runtime_Exception
     */
    public function __construct(string $zahl = null)
    {
        if ( $zahl !== null )
        {
            try
            {
                $this->set_from_string($zahl);
            }
            catch ( Runtime_Exception $e )
            {
                throw new Runtime_Exception('Unable to create ' . __CLASS__ . ' instance.', 0, $e);
            }
        }
    }

    /**
     * Resets the object to an uninitialized state.
     */
    public function reset(): void
    {
        $this->zahl = null;
    }

    /**
     * @param string $data
     * @throws Runtime_Exception
     */
    public function set_from_string(string $data): void
    {
        // An empty string does not represent a number!
        if ( strlen($data) === 0 )
        {
            return;
        }

        $intval = intval($data);

        if ( $intval < 0 )
        {
            throw new Runtime_Exception('Negative numbers are not allowed!');
        }

        $this->zahl = $intval;
    }

    /**
     * @return int|null
     */
    public function get_zahl(): ?int
    {
        return $this->zahl;
    }

    /**
     * @return string
     */
    public function get_formatted(): string
    {
        if ( $this->is_set() )
        {
            return strval($this->zahl);
        }
        return '';
    }

    /**
     * @return bool
     */
    public function is_set(): bool
    {
        return $this->zahl !== null;
    }

}