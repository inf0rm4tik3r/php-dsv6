<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Data;

use Lukaspotthast\DSV\Exception\Runtime_Exception;
use Lukaspotthast\Support\Str;

/**
 * Class Zeit
 * @package Lukaspotthast\DSV\Data
 */
class Zeit implements Data_Object
{

    /**
     * @var int|null
     */
    private $h = null;

    /**
     * @var int|null
     */
    private $m = null;

    /**
     * @var int|null
     */
    private $s = null;

    /**
     * @var int|null
     */
    private $f = null;

    /**
     * Duration constructor.
     * @param string|null $duration
     * @throws Runtime_Exception
     */
    public function __construct(string $duration = null)
    {
        if ( $duration !== null )
        {
            try
            {
                $this->set_from_string($duration);
            }
            catch ( Runtime_Exception $e )
            {
                throw new Runtime_Exception('Unable to create ' . __CLASS__ . ' instance.', 0, $e);
            }
        }
    }

    /**
     * Resets the object to an uninitialized state.
     */
    public function reset(): void
    {
        $this->h = null;
        $this->m = null;
        $this->s = null;
        $this->f = null;
    }

    /**
     * @param string $data
     * @throws Runtime_Exception
     */
    public function set_from_string(string $data): void
    {
        $general_error = '"%s" was not of the form "HH:MM:SS,hh"!';

        // Split into h, m and sf
        $hms_parts = explode(':', $data);
        if ( count($hms_parts) !== 3 )
        {
            throw new Runtime_Exception(sprintf($general_error, $data));
        }

        $h  = $hms_parts[0];
        $m  = $hms_parts[1];
        $sf = $hms_parts[2];

        // Split sf into s and f
        $sf_parts = explode(',', $sf);
        if ( count($sf_parts) !== 2 )
        {
            throw new Runtime_Exception(sprintf($general_error, $data));
        }

        $s = $sf_parts[0];
        $f = $sf_parts[1];

        // Every part must be exactly two characters long.
        if ( !strlen($h . $m . $s . $f) === 8 )
        {
            throw new Runtime_Exception(sprintf($general_error, $data));
        }

        // Each part must be numerical.
        if ( !(is_numeric($h) and is_numeric($m) and is_numeric($s) and is_numeric($f)) )
        {
            throw new Runtime_Exception(sprintf($general_error, $data));
        }

        $this->set_hours(intval($h));
        $this->set_minutes(intval($m));
        $this->set_seconds(intval($s));
        $this->set_fraction(intval($f));
    }

    /**
     * @return string
     *      HH:MM:SS,hh
     */
    public function get_formatted(): string
    {
        if ( $this->is_set() )
        {
            $h = $this->get_hours_formatted();
            $m = $this->get_minutes_formatted();
            $s = $this->get_seconds_formatted();
            $f = $this->get_fraction_formatted();

            return "$h:$m:$s,$f";
        }
        else
        {
            return '';
        }
    }

    /**
     * @param int $hours
     * @throws Runtime_Exception
     */
    public function set_hours(int $hours): void
    {
        $this->check_hours($hours);
        $this->h = $hours;
    }

    /**
     * @param int $hours
     * @throws Runtime_Exception
     */
    public function check_hours(int $hours): void
    {
        if ( $hours < 0 or $hours > 24 )
        {
            throw new Runtime_Exception('The $hours parameter did not represent a value between 0 and 24!');
        }
    }

    /**
     * @return int
     */
    public function get_hours(): int
    {
        return $this->h;
    }

    /**
     * @return string
     */
    public function get_hours_formatted(): string
    {
        return $this->h !== null ? Str::zero_fill_left(strval($this->h), 2) : '';
    }

    /**
     * @param int $minutes
     * @throws Runtime_Exception
     */
    public function set_minutes(int $minutes): void
    {
        $this->check_minutes($minutes);
        $this->m = $minutes;
    }

    /**
     * @param int $minutes
     * @throws Runtime_Exception
     */
    public function check_minutes(int $minutes): void
    {
        if ( $minutes < 0 or $minutes > 60 )
        {
            throw new Runtime_Exception('The $minutes parameter did not represent a value between 0 and 60!');
        }
    }

    /**
     * @return int
     */
    public function get_minutes(): int
    {
        return $this->m;
    }

    /**
     * @return string
     */
    public function get_minutes_formatted(): string
    {
        return $this->m !== null ? Str::zero_fill_left(strval($this->m), 2) : '';
    }

    /**
     * @param int $seconds
     * @throws Runtime_Exception
     */
    public function set_seconds(int $seconds): void
    {
        $this->check_seconds($seconds);
        $this->s = $seconds;
    }

    /**
     * @param int $seconds
     * @throws Runtime_Exception
     */
    public function check_seconds(int $seconds): void
    {
        if ( $seconds < 0 or $seconds > 60 )
        {
            throw new Runtime_Exception('The $seconds parameter did not represent a value between 0 and 60!');
        }
    }

    /**
     * @return int
     */
    public function get_seconds(): int
    {
        return $this->m;
    }

    /**
     * @return string
     */
    public function get_seconds_formatted(): string
    {
        return $this->s !== null ? Str::zero_fill_left(strval($this->s), 2) : '';
    }

    /**
     * @param int $fraction
     * @throws Runtime_Exception
     */
    public function set_fraction(int $fraction): void
    {
        $this->check_fraction($fraction);

        $f = strval($this->f);
        if ( strlen($f) > 2 )
        {
            $this->f = substr($f, 0, 2);
        }
        else
        {
            $this->f = $fraction;
        }
    }

    /**
     * @param int $fraction
     * @throws Runtime_Exception
     */
    public function check_fraction(int $fraction): void
    {
        if ( $fraction < 0 )
        {
            throw new Runtime_Exception('The $fraction parameter was not positive!');
        }
    }

    /**
     * @return int
     */
    public function get_fraction(): int
    {
        return $this->f;
    }

    /**
     * @return string
     */
    public function get_fraction_formatted(): string
    {
        return $this->f !== null ? Str::zero_fill_right(strval($this->f), 2) : '';
    }

    /**
     * @return bool
     */
    public function is_set(): bool
    {
        return
            $this->h !== null and
            $this->m !== null and
            $this->s !== null and
            $this->f !== null;
    }

}