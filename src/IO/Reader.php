<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\IO;

use Lukaspotthast\DSV\Exception\File_Read_Exception;
use Lukaspotthast\DSV\Exception\Runtime_Exception;
use Lukaspotthast\Support\Encoding;

/**
 * Class Reader
 * @package Lukaspotthast\DSV\IO
 */
class Reader
{

    /**
     * @param string $filename
     * @return array
     * @throws File_Read_Exception
     */
    public function read(string $filename): array
    {
        try
        {
            $content = $this->read_file($filename);
        }
        catch ( Runtime_Exception $e )
        {
            throw new File_Read_Exception('Could not read file.', $filename, 0, $e);
        }

        $lines = $this->get_lines($content);
        return $lines;
    }

    /**
     * @param string $filename
     * @param bool   $fix_encoding
     *      If set, non UTF-8 files will be read and converted to UTF-8 before further processing.
     * @return string
     * @throws Runtime_Exception
     */
    private function read_file(string $filename, bool $fix_encoding = true): string
    {
        $content = file_get_contents($filename);
        if ( $content === false )
        {
            throw new Runtime_Exception('The file "' . $filename . '" is not accessible. It may not exist!');
        }

        return $this->check_encoding($content, $fix_encoding);
    }

    /**
     * @param string $content
     * @param bool   $fix_encoding
     * @return string
     * @throws Runtime_Exception
     */
    private function check_encoding(string $content, bool $fix_encoding): string
    {
        // Ensure that the read string is encoded in UTF-8.
        $encoding = mb_detect_encoding($content, 'UTF-8', true);
        if ( $fix_encoding )
        {
            if ( $encoding === false or $encoding !== 'UTF-8' )
            {
                $content = Encoding::toUTF8($content);
            }
        }
        else
        {
            if ( $encoding === false )
            {
                throw new Runtime_Exception('The encoding of the specified file could not be detected.');
            }
            if ( $encoding !== 'UTF-8' )
            {
                throw new Runtime_Exception('The specified file was not UTF-8 encoded! Encountered: ' . $encoding);
            }
        }
        return $content;
    }

    /**
     * @param string $content
     * @return array
     */
    private function get_lines(string $content): array
    {
        /*
         * In PHP preg_split(), preg_match, and preg_replace the \R matches all line breaks of any sort.
         * http://www.pcre.org/pcre.txt
         *
         * By default, the sequence \R in a pattern matches any Unicode newline sequence, whatever has been selected as
         * the line ending sequence. If you specify
         *   --enable-bsr-anycrlf
         * The default is changed so that \R matches only CR, LF, or CRLF. Whatever is selected when PCRE is built
         * can be overridden when the library functions are called.
         */
        $lines = preg_split('/\R/', $content);

        // We do not want special white-space characters at both ends of each line.
        foreach ( $lines as &$line )
        {
            $line = trim($line);
        }

        return $lines;
    }

}