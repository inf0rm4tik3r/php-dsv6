<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Tests\System;

use Lukaspotthast\DSV\Document\Writer\HTML_Writer;
use Lukaspotthast\DSV\Document\Writer\JSON_Writer;
use Lukaspotthast\DSV\Document\Writer\Text_Writer;
use Lukaspotthast\DSV\Exception\Document_Load_Exception;
use Lukaspotthast\DSV\Exception\Document_Write_Exception;
use Lukaspotthast\DSV\Loader;

/**
 * Class Read_From_File_Test
 * @package Lukaspotthast\DSV\Tests\System
 */
class Read_From_File_Test
{

    /**
     * @var Loader
     */
    private $loader;

    public function __construct()
    {
        $this->loader = new Loader();
    }

    public function run(): void
    {
        $base  = __DIR__ . '/../Documents/';
        $files = [
            '2018-03-04-Schwimmo-Wk.dsv6',
            'test.dsv6',
            'vereinsergebnisliste_0.dsv6',
            'vereinsmeldeliste_0.dsv6',
            'wettkampfdefinitionsliste_0.dsv6',
            'wettkampfergebnisliste_0.dsv6',
        ];

        try
        {
            $document = $this->loader->load($base . $files[3]);
            $document->set_writer(new HTML_Writer());
            echo($document->write());

            /*
            $document->set_writer(new JSON_Writer());
            $json = $document->write();
            dump( $json );
            //dump( json_encode(json_decode($json), JSON_PRETTY_PRINT) );
            dump( json_decode($json) );

            $document->set_writer(new Text_Writer());
            dump( $document->write() );
            */
        }
        catch ( Document_Load_Exception $e )
        {
            dump($e);
        }
        catch ( Document_Write_Exception $e )
        {
            dump($e);
        }
    }

}