<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Tests\System;

use Lukaspotthast\DSV\Data\JGAK;
use Lukaspotthast\DSV\Document\Element\Abschnitt;
use Lukaspotthast\DSV\Document\Element\Ausrichter;
use Lukaspotthast\DSV\Document\Element\Ausschreibungimnetz;
use Lukaspotthast\DSV\Document\Element\Erzeuger;
use Lukaspotthast\DSV\Document\Element\Meldeadresse;
use Lukaspotthast\DSV\Document\Element\Meldegeld;
use Lukaspotthast\DSV\Document\Element\Meldeschluss;
use Lukaspotthast\DSV\Document\Element\Veranstalter;
use Lukaspotthast\DSV\Document\Element\Veranstaltung;
use Lukaspotthast\DSV\Document\Element\Veranstaltungsort;
use Lukaspotthast\DSV\Document\Element\Wertung;
use Lukaspotthast\DSV\Document\Element\Wettkampf;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Comment;
use Lukaspotthast\DSV\Document\Type\Wettkampfdefinitionsliste;
use Lukaspotthast\DSV\Document\Writer\HTML_Writer;
use Lukaspotthast\DSV\Exception\Runtime_Exception;

/**
 * Class Manually_Create_Wettkampfdefinitionsliste_Document_Test
 * @package Lukaspotthast\DSV\Tests\System
 */
class Manually_Create_Wettkampfdefinitionsliste_Document_Test
{

    public function run(): void
    {
        try
        {
            $document = new Wettkampfdefinitionsliste();

            $erzeuger = new Erzeuger($document);
            $erzeuger->set_software('foo');
            $erzeuger->set_version('v1');
            $erzeuger->set_kontakt('asdasdasd');
            $document->set_erzeuger($erzeuger);
            $document->add_occurrence(Erzeuger::get_element_name());

            $veranstaltung = new Veranstaltung($document);
            $veranstaltung->set_veranstaltungsbezeichnung('s');
            $veranstaltung->set_veranstaltungsort('s');
            $veranstaltung->set_bahnlaenge('50');
            $veranstaltung->set_zeitmessung('HANDZEIT');
            $document->set_veranstaltung($veranstaltung);
            $document->add_occurrence(Veranstaltung::get_element_name());

            $veranstaltungsort = new Veranstaltungsort($document);
            $veranstaltungsort->set_name_schwimmhalle('sslz');
            $veranstaltungsort->set_land('s');
            $veranstaltungsort->set_ort('s');
            $veranstaltungsort->set_plz('s');
            $veranstaltungsort->set_strasse('s');
            $veranstaltungsort->set_fax('s');
            $veranstaltungsort->set_fax('s');
            $veranstaltungsort->set_email('s');
            $document->set_veranstaltungsort($veranstaltungsort);
            $document->add_occurrence(Veranstaltungsort::get_element_name());

            $ausschreibungimnetz = new Ausschreibungimnetz($document);
            $ausschreibungimnetz->set_internetadresse('www.foo.com');
            $document->set_ausschreibungimnetz($ausschreibungimnetz);
            $document->add_occurrence(Ausschreibungimnetz::get_element_name());

            $veranstalter = new Veranstalter($document);
            $veranstalter->set_name('blababalb');
            $veranstalter->add_comment(new Comment('a comment'));
            $document->set_veranstalter($veranstalter);
            $document->add_occurrence(Veranstalter::get_element_name());

            $ausrichter = new Ausrichter($document);
            $ausrichter->set_name_des_ausrichters('asv wuppertal');
            $ausrichter->set_name('L');
            $ausrichter->set_email('m@a.il');
            $document->set_ausrichter($ausrichter);
            $document->add_occurrence(Ausrichter::get_element_name());

            $meldeadresse = new Meldeadresse($document);
            $meldeadresse->set_name('det asd');
            $meldeadresse->set_email('e@ma.il');
            $document->set_meldeadresse($meldeadresse);
            $document->add_occurrence(Meldeadresse::get_element_name());

            $meldeschluss = new Meldeschluss($document);
            $meldeschluss->set_datum(new Date('01.01.1999'));
            $meldeschluss->set_uhrzeit(new Time('18:00'));
            $document->set_meldeschluss($meldeschluss);
            $document->add_occurrence(Meldeschluss::get_element_name());

            $abschnitt = new Abschnitt($document);
            $abschnitt->set_abschnittsnummer(1);
            $document->add_abschnitt($abschnitt);
            $document->add_occurrence(Abschnitt::get_element_name());

            $wettkampf = new Wettkampf($document);
            $wettkampf->set_abschnittsnummer(1);
            $wettkampf->set_wettkampfnummer(1);
            $wettkampf->set_wettkampfart('V');
            $wettkampf->set_einzelstrecke(200);
            $wettkampf->set_technik('F');
            $wettkampf->set_geschlecht('M');
            $wettkampf->set_ausuebung('GL');
            $wettkampf->set_zuordnung_bestenliste('SW');
            $document->add_wettkampf($wettkampf);
            $document->add_occurrence(Wettkampf::get_element_name());

            $wertung = new Wertung($document);
            $wertung->set_wertungsnummer(10);
            $wertung->set_wettkampfnummer(1);
            $wertung->set_wettkampfart('V');
            $wertung->set_wertungsklasse('JG');
            $wertung->set_minimaler_jgak(new JGAK('1995'));
            $wertung->set_maximaler_jgak(new JGAK('1995'));
            $wertung->set_wertungsname('WK 1 männlich');
            $wertung->set_geschlecht('M');
            $document->add_wertung($wertung);
            $document->add_occurrence(Wertung::get_element_name());

            $meldegeld = new Meldegeld($document);
            $meldegeld->set_meldegeld_typ('Wkmeldegeld');
            $meldegeld->set_wettkampfnummer(1);
            $meldegeld->set_betrag(new Money('4,50'));
            $document->add_meldegeld($meldegeld);
            $document->add_occurrence(Meldegeld::get_element_name());


            $document->set_writer(new HTML_Writer());
            dump($document);
            echo($document->write());
        }
        catch ( Runtime_Exception $e )
        {
            dump($e);
        }
    }

}