<?php

namespace Lukaspotthast\DSV\Tests\Unit;

use Lukaspotthast\DSV\Document\Structure\Ast;
use PHPUnit\Framework\TestCase;
use RuntimeException;

/**
 * Class Ast_Test
 * @package Lukaspotthast\DSV\Tests\Unit
 */
final class Ast_Test extends TestCase
{

    /**
     * @return Ast
     */
    public function test_creation(): Ast
    {
        $ast = new Ast();
        $this->assertInstanceOf(Ast::class, $ast);
        $this->assertObjectHasAttribute('filename', $ast);
        return $ast;
    }

    /**
     * @depends test_creation
     * @param Ast $ast
     */
    public function test_set_get_filename_valid_value(Ast $ast): void
    {
        $test_value = 'test/filename/foo.bar';
        $ast->set_filename($test_value);
        $this->assertEquals($test_value, $ast->get_filename());
    }

    /**
     * @return array
     */
    public function provides_good_lines(): array
    {
        return [
            [['']],
            [['(* *)']],
            [['ASD:v1;']],
            [['ASD:v1;v2;']],
            [['foo']],
            [['BAR']],
            [['FOOBAR:']],
        ];
    }

    /**
     * @return array
     */
    public function provides_bad_lines(): array
    {
        return [
            [['ASD:v1;v2']],
            [['ASD:v1;v2']],
        ];
    }

    /**
     * @dataProvider provides_good_lines
     * @depends      test_creation
     * @param array $lines
     * @param Ast   $ast
     */
    public function test_parse_good_lines(array $lines, Ast $ast)
    {
        /** @noinspection PhpVoidFunctionResultUsedInspection */
        $result = $ast->parse($lines);
        $this->assertEquals(null, $result);
    }

    /**
     * @dataProvider provides_bad_lines
     * @depends      test_creation
     * @param array $lines
     * @param Ast   $ast
     */
    public function test_parse_bad_lines(array $lines, Ast $ast)
    {
        $this->expectException(RuntimeException::class);

        /** @noinspection PhpVoidFunctionResultUsedInspection */
        $result = $ast->parse($lines);
        $this->assertEquals(null, $result);
    }

}