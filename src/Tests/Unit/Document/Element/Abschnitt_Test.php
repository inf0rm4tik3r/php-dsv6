<?php

namespace Lukaspotthast\DSV\Tests\Unit;

use Lukaspotthast\DSV\Document\Element\Abschnitt;
use Lukaspotthast\DSV\Document\Structure\Abstract_\Statement;
use PHPUnit\Framework\TestCase;
use RuntimeException;

/**
 * Class Abschnitt_Test
 * @package Lukaspotthast\DSV\Tests\Unit
 */
final class Abschnitt_Test extends TestCase
{

    /**
     * @return array
     */
    public function provide_statement()
    {
        return [
            [new Statement('ABSCHNITT', ['asd'])],
        ];
    }

    /**
     * @return Abschnitt
     */
    public function test_instance()
    {
        $abschnitt = new Abschnitt();
        $this->assertInstanceOf(Abschnitt::class, $abschnitt);

        $this->assertObjectHasAttribute('abschnittsnummer', $abschnitt);
        $this->assertObjectHasAttribute('abschnittsdatum', $abschnitt);
        $this->assertObjectHasAttribute('einlass', $abschnitt);
        $this->assertObjectHasAttribute('kampfrichtersitzung', $abschnitt);
        $this->assertObjectHasAttribute('anfangszeit', $abschnitt);
        $this->assertObjectHasAttribute('relative_angabe', $abschnitt);

        return $abschnitt;
    }

    /**
     * @dataProvider provide_statement
     * @depends      test_instance
     * @param Abschnitt $abschnitt
     * @param Statement $stmt
     */
    public function test_create_from_statement(Statement $stmt, Abschnitt $abschnitt)
    {
        $this->expectException(RuntimeException::class);
        $abschnitt->create_from_statement($stmt);
    }

}