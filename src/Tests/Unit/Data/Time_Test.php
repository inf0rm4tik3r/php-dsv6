<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Tests\Unit;

use Lukaspotthast\DSV\Data\Time;
use PHPUnit\Framework\TestCase;
use RuntimeException;

/**
 * Class Time_Test
 * @package Lukaspotthast\DSV\Tests\Unit
 */
final class Time_Test extends TestCase
{

    /**
     * @return Time
     */
    public function test_creation(): Time
    {
        $time = new Time();
        $this->assertInstanceOf(Time::class, $time);
        $this->assertTrue(is_string(Time::FORMAT));
        return $time;
    }

    /**
     * @depends test_creation
     * @param Time $time
     */
    public function test_initial_state(Time $time): void
    {
        $this->assertEquals(date(Time::FORMAT, time()), $time->get_formatted());
    }

    /**
     * @return array
     */
    public function provides_good_times(): array
    {
        return [
            ['00:00'],
            ['23:59'],
            ['12:00'],
        ];
    }

    /**
     * @return array
     */
    public function provides_bad_times(): array
    {
        return [
            ['24:00'],
            ['01:00:00'],
            ['2359'],
            ['foo'],
        ];
    }

    /**
     * @dataProvider provides_good_times
     * @depends      test_creation
     * @param string $good
     * @param Time   $time
     */
    public function test_set_from_string_good(string $good, Time $time): void
    {
        // Should not throw an exception:
        $time->set_from_string($good);
        $this->assertEquals($good, $time->get_formatted());
    }

    /**
     * @dataProvider provides_bad_times
     * @depends      test_creation
     * @param string $bad
     * @param Time   $time
     */
    public function test_set_from_string_bad(string $bad, Time $time): void
    {
        // Should throw an exception:
        $this->expectException(RuntimeException::class);
        $time->set_from_string($bad);
    }

}