<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV\Tests\Unit;

use Lukaspotthast\DSV\Data\Date;
use PHPUnit\Framework\TestCase;
use RuntimeException;

/**
 * Class Date_Test
 * @package Lukaspotthast\DSV\Tests\Unit
 */
final class Date_Test extends TestCase
{

    /**
     * @return Date
     */
    public function test_creation(): Date
    {
        $date = new Date();
        $this->assertInstanceOf(Date::class, $date);
        $this->assertTrue(is_string(Date::FORMAT));
        return $date;
    }

    /**
     * @depends test_creation
     * @param Date $date
     */
    public function test_initial_state(Date $date): void
    {
        $this->assertEquals(date(Date::FORMAT, time()), $date->get_formatted());
    }

    /**
     * @return array
     */
    public function provides_good_dates(): array
    {
        return [
            ['01.01.1970'],
            ['21.01.2018'],
            ['05.11.3845'],
        ];
    }

    /**
     * @return array
     */
    public function provides_bad_dates(): array
    {
        return [
            ['32.01.2018'],
            ['2018.01.01'],
            ['01-01-2018'],
            ['2018-01-01'],
            ['foo'],
        ];
    }

    /**
     * @dataProvider provides_good_dates
     * @depends      test_creation
     * @param string $good
     * @param Date   $date
     */
    public function test_set_from_string_good(string $good, Date $date): void
    {
        // Should not throw an exception:
        $date->set_from_string($good);
        $this->assertEquals($good, $date->get_formatted());
    }

    /**
     * @dataProvider provides_bad_dates
     * @depends      test_creation
     * @param string $bad
     * @param Date   $date
     */
    public function test_set_from_string_bad(string $bad, Date $date): void
    {
        // Should throw an exception:
        $this->expectException(RuntimeException::class);
        $date->set_from_string($bad);
    }

}