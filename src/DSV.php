<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV;

use Lukaspotthast\DSV\Document\Document;

/**
 * Class DSV
 * @package Lukaspotthast\DSV
 */
class DSV
{

    const ERZEUGER_SOFTWARE = 'lukaspotthast\dsv';
    const ERZEUGER_VERSION  = '1.0';
    const ERZEUGER_KONTAKT  = 'info@lukas-potthast.de';
    const ERZEUGER_AUTOR    = 'Lukas Potthast';

    const COMMENT_START                     = '(*';
    const COMMENT_END                       = '*)';
    const STMT_ELEMENT_NAME_COMPLETION_SIGN = ':';
    const STMT_ATTRIB_DELIMITER             = ';';

    /**
     * @param string $in
     * @return Document
     */
    public static function load_dsv(string $in): Document
    {
        return null;
    }

    /**
     * @param string $in
     * @param string $out
     */
    public static function convert_dsv_to_json(string $in, string $out): void
    {

    }

}