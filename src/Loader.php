<?php

declare(strict_types = 1);

namespace Lukaspotthast\DSV;

use Lukaspotthast\DSV\Document\Creator;
use Lukaspotthast\DSV\Document\Document;
use Lukaspotthast\DSV\Document\Structure\Ast;
use Lukaspotthast\DSV\Exception\Ast_Parse_Exception;
use Lukaspotthast\DSV\Exception\Document_Creation_Exception;
use Lukaspotthast\DSV\Exception\Document_Integrity_Exception;
use Lukaspotthast\DSV\Exception\Document_Load_Exception;
use Lukaspotthast\DSV\Exception\File_Read_Exception;
use Lukaspotthast\DSV\IO\Reader;
use Lukaspotthast\Support\Filesystem\Filesystem;
use Lukaspotthast\Support\Timing\Timer;

/**
 * Class Loader
 * @package Lukaspotthast\DSV
 */
class Loader
{

    const DOCUMENT_LOAD_ERROR = 'Document could not be loaded.';

    /**
     * @param string $filename
     * @return Document
     * @throws Document_Load_Exception
     */
    public function load(string $filename): Document
    {
        $timer = new Timer();

        // READ
        $timer->start();
        $lines     = $this->perform_read($filename);
        $read_time = $timer->stop();

        // PARSE
        $timer->start();
        $ast = $this->perform_parse($lines);
        $ast->set_filename(Filesystem::file_name($filename));
        $parse_time = $timer->stop();

        // CREATE
        $timer->start();
        $document    = $this->perform_create($ast);
        $create_time = $timer->stop();

        $document->get_performance_info()->set_read_time($read_time);
        $document->get_performance_info()->set_parse_time($parse_time);
        $document->get_performance_info()->set_create_time($create_time);

        return $document;
    }

    /**
     * @param string $filename
     * @return array
     * @throws Document_Load_Exception
     */
    private function perform_read(string $filename): array
    {
        $reader = new Reader();
        try
        {
            $lines = $reader->read($filename);
        }
        catch ( File_Read_Exception $e )
        {
            throw new Document_Load_Exception(self::DOCUMENT_LOAD_ERROR, 0, $e);
        }
        return $lines;
    }

    /**
     * @param array $lines
     * @return Ast
     * @throws Document_Load_Exception
     */
    private function perform_parse(array $lines): Ast
    {
        $ast = new Ast();
        try
        {
            $ast->parse($lines);
        }
        catch ( Ast_Parse_Exception $e )
        {
            throw new Document_Load_Exception(self::DOCUMENT_LOAD_ERROR, 0, $e);
        }
        return $ast;
    }

    /**
     * @param Ast $ast
     * @return Document
     * @throws Document_Load_Exception
     */
    private function perform_create(Ast $ast): Document
    {
        $creator = new Creator();
        try
        {
            $document = $creator->create($ast);
        }
        catch ( Document_Creation_Exception $e )
        {
            throw new Document_Load_Exception(self::DOCUMENT_LOAD_ERROR, 0, $e);
        }
        catch ( Document_Integrity_Exception $e )
        {
            throw new Document_Load_Exception(self::DOCUMENT_LOAD_ERROR, 0, $e);
        }
        return $document;
    }

}