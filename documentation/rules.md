# Rules



### Document elements:


- `null` must always be considered as an invalid value for an attribute.

- All attributes must be initialized in the constructor before the parent::__constructor() call is mode.

- Every **required** attribute must be initialized with null.

- Every **required** attribute must be included in the integrity check.

- Every **non required** attribute must be initialized with an appropriate default value.

- `check_` functions must be declared public.

- `check_` functions must tell if the data given to the is valid by returning a boolean value.

- `set_` functions calling the `check_` functions must throw a RuntimeException if the checker function returned false.